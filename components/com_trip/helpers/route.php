<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_weblinks
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

// Component Helper
jimport('joomla.application.component.helper');
jimport('joomla.application.categories');

/**
 * Weblinks Component Route Helper
 *
 * @static
 * @package		Joomla.Site
 * @subpackage	com_weblinks
 * @since 1.5
 */
abstract class TripHelperRoute
{
	protected static $lookup;

	/**
	 * @param	int	The route of the weblink
	 */
	public static function getItemIdByTriptype($triptype_id)
	{

		$menu =& JSite::getMenu();
		//$menu = JFactory::getApplication()->getMenu();
		//$menuitems = $menu ->getMenu();
		
		/*if($menuitems->getActive()==$menu->getDefault()){
			echo'This is the front page';
		}*/
		$menuitems = $menu->getMenu('_items');
		if($menuitems){
			foreach($menuitems as $item){
				$menuparams = $menu->getParams( $item->id );
				if($menuparams->get('triptype_id') == $triptype_id){
					return $item->id;
				}
			}
		}
		
		return false;
	}
	
	function getTripAlias($title){
		$alias =  strtolower(preg_replace("/[^a-zA-Z0-9\s]/", "", $title));
		$alias = str_replace("  "," ",$alias);
		$alias = str_replace(" ","-",$alias);
		
		return $alias;
	}
	
	function getTripTypeAlias($title){
		$alias =  strtolower(preg_replace("/[^a-zA-Z0-9\s]/", "", $title));
		$alias = str_replace("  "," ",$alias);
		$alias = str_replace(" ","-",$alias);
		
		return $alias;
	}
}
