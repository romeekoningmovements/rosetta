<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_weblinks
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

// Component Helper
jimport('joomla.application.component.helper');
jimport('joomla.application.categories');

/**
 * Weblinks Component Route Helper
 *
 * @static
 * @package		Joomla.Site
 * @subpackage	com_weblinks
 * @since 1.5
 */
abstract class TripHelperForm
{
	protected static $lookup;
	
	public function getTripTypes(){
		$db =& JFactory::getDBO();
		$query = "SELECT * FROM #__xtrip_triptypes ORDER BY id ASC";
		$db->setQuery($query);
		if(!$triptypes = $db->loadObjectList()){
			return false;
		}
		
		$options[] = JHTML::_('select.option',0,JText::_('KIES EEN REIS'));
		foreach($triptypes as $key=>$value){
	    	$options[] = JHTML::_('select.option',$value->id,$value->triptype);
	    }

	    return JHTML::_('select.genericlist',$options,'triptype_id',array('class'=>'triptypes', 'onchange'=>"getTrips(this.value);"),'value','text',JRequest::getInt('triptype_id'));
	}
	
	public function getTrips($triptype){
		if($triptype){
			$db =& JFactory::getDBO();
			$query = "SELECT * FROM #__xtrip_trips WHERE triptype_id=".$db->Quote($triptype)." AND `state`=1 ORDER BY title ASC";
			$db->setQuery($query);
			if(!$trips = $db->loadObjectList()){
				return false;
			}
			
			$options[] = JHTML::_('select.option',0,JText::_('Kies aub een reis'));
			foreach($trips as $key=>$value){
		    	$options[] = JHTML::_('select.option',$value->trip_id,$value->title);
		    }
			return JHTML::_('select.genericlist',$options,'trip_id',array('class'=>'trips', 'onchange'=>"getDepartures('".JRequest::getInt('triptype_id')."', this.value);"),'value','text',JRequest::getInt('trip_id'));
		}
		return false;
	}
	
	public function getDepartures($triptype, $trip){
		//JHTML::_('behavior.calendar');
		if($triptype && $trip){
			$db =& JFactory::getDBO();
			$query = "SELECT * FROM #__xtrip_trips WHERE trip_id=".$db->Quote($trip)." AND `state`=1 ";
			$db->setQuery($query);
			if($result = $db->loadObject()){
				if($result->willekeurige_vertrek_datum == 1){
					$value = "";
					if(JRequest::getVar('departure_id') == 'willekeurig'){
						$value = "dd-mm-jjjj";
					}
					else{
						$value = JRequest::getVar('departure_id');
					}
					return "<input type='text' class='departures' id='willekeurig' name='willekeurig' value='".$value."' onchange='getPersons(".JRequest::getInt('triptype_id').", ".JRequest::getInt('trip_id').", this.value);' />";
				}
				else{
					$query  = " SELECT * FROM #__xtrip_departures d ";
					$query .= " WHERE d.tripid=".$db->Quote($trip)." AND vertrek > ".$db->Quote(date('Y-m-d'))." ORDER BY d.id ASC";
					//print $query;
					$db->setQuery($query);
					if(!$departures = $db->loadObjectList()){
						return "Geen vertrekdata beschikbaar, neem aub contact met ons op.";
					}
	
					$options[] = JHTML::_('select.option',0,JText::_('Kies aub een vertrekdatum'));
					foreach($departures as $key=>$value){
					    $options[] = JHTML::_('select.option',$value->id,ucfirst(strftime("%A %d %B %Y",strtotime($value->vertrek))));
					   }
					return JHTML::_('select.genericlist',$options,'departure_id',array('class'=>'departures', 'onchange'=>"getPersons('".JRequest::getInt('triptype_id')."', '".JRequest::getInt('trip_id')."', this.value);"),'value','text',JRequest::getVar('departure_id'));
				}
			}
		}
		return false;
	}
	
	public function getPersons($triptype, $trip, $depature){
		if($triptype && $trip && $depature){
			if(is_numeric($depature)){
				$db =& JFactory::getDBO();
				$query = "SELECT max(beschikbaarheid) as maximum FROM #__xtrip_departures WHERE tripid=".$db->Quote($trip)." AND id=".$db->Quote($depature);
				$db->setQuery($query);
				if(!$persons = $db->loadObject()){
					return false;
				}
				
				$options[] = JHTML::_('select.option',0,JText::_('Kies het aantal personen'));
				for($i = 1; $i < $persons->maximum +1; $i++){
			    	$options[] = JHTML::_('select.option',$i, $i);
			    }
			   
			    return JHTML::_('select.genericlist',$options,'person', array('class'=>'persons', 'onchange'=>"getBookingsForm('".JRequest::getInt('triptype_id')."', '".JRequest::getInt('trip_id')."', '".JRequest::getInt('departure_id')."',this.value);"),'value','text',JRequest::getInt('departure_id'));
			}
			else{
				$options[] = JHTML::_('select.option',0,JText::_('Kies het aantal personen'));
				for($i = 1; $i < 8; $i++){
			    	$options[] = JHTML::_('select.option',$i, $i);
			    }
			   
			    return JHTML::_('select.genericlist',$options,'person', array('class'=>'persons', 'onchange'=>"getBookingsForm('".JRequest::getInt('triptype_id')."', '".JRequest::getInt('trip_id')."', '".JRequest::getVar('departure_id')."',this.value);"),'value','text',JRequest::getVar('departure_id'));
			}
			//return JHTML::_('select.integerlist',1,$persons->maximum,1, 'person', array('class'=>'persons', 'onchange'=>"getBookingsForm();"),JRequest::getInt('person'));
		}
		return false;
	}
}
