<?php
// No direct access
defined('_JEXEC') or die;
jimport('joomla.application.component.model');

class TripModelSearch extends JModelLegacy
{
	public $_db;
	public $_session;
	
	var $_pagination = null;
	var $_data = null;
	var $_total = null;
	var $_destination_id = null;
	var $_triptype_id = null;
	var $_search_date_start = null;
	var $_search_date_end = null;
	var $_searchfield = null;
	
	function __construct()
	{
		parent::__construct();
		$menu =& JSite::getMenu();
		$menuitemid = JRequest::getInt('Itemid');
		$menuparams = $menu->getParams( $menuitemid );
		$this->_destination_id = JRequest::getInt('destination_id');
		$this->_triptype_id = JRequest::getInt('triptype_id');
		$this->_search_date_start = JRequest::getVar('search_date_start');
		$this->_search_date_end = JRequest::getVar('search_date_end');
		$this->_searchfield = JRequest::getVar('searchfield');


		// Get pagination request variables
		$this->_session =& JFactory::getSession();
		$mainframe =& JFactory::getApplication();
		$limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart = JRequest::getVar('limitstart', 0, '', 'int');
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
	 	
	 	JRequest::setVar('limit', $limit);
	 	JRequest::setVar('limitstart', $limitstart);
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
	}
	
	function getDestination(){
		$db=$this->_db;		
		$query = "SELECT * FROM #__xtrip_destinations WHERE destination_id=".$db->Quote($this->_destination_id);
		$db->setQuery($query);
		if($catid = $db->loadObject()){
			return $catid;
		}
	}
		
	function getDestinationTrips($triptype_id, $destination_id, $search_date_start, $search_date_end){

		//print "triptype_id: ".$this->_triptype_id."<br/>";
		//print "destination_id: ".$this->_destination_id."<br/>";
		$search_date_start = '';
		$search_date_end = '';
		if($this->_search_date_start){
			$search_date_start = explode('-', str_replace(':','-',$this->_search_date_start));
		}
		if($this->_search_date_end){
			$search_date_end = explode('-', str_replace(':','-',$this->_search_date_end));
		}
		
		$db=$this->_db;		
		$query  = "select SQL_CALC_FOUND_ROWS  *,tr.reissom as tripreissom, tr.title as triptitle ,d.title as bestemming, c.color as tripcolor, ";
		$query .= "SUBSTRING_INDEX(GROUP_CONCAT(de.vertrek ORDER BY vertrek ASC SEPARATOR ','), ',',2) as eerstvolgende ";
		$query .= "FROM #__xtrip_trips tr ";	
		$query .= "INNER JOIN #__xtrip_destinations d ON d.destination_id=tr.destination_id ";
		$query .= "INNER JOIN #__xtrip_triptypes tt ON tt.id=tr.triptype_id ";
		$query .= "INNER JOIN #__xtrip_colors c ON c.id=tt.color ";
		$query .= "LEFT JOIN #__xtrip_departures de ON de.tripid=tr.trip_id ";
		//$query .= "LEFT JOIN #__xtrip_content c ON c.trip_id=tr.id ";
		if($this->_destination_id){
			$query .= "WHERE tr.destination_id=".$db->Quote($this->_destination_id);
		}
		if($this->_triptype_id){
			if(strpos($query, 'WHERE') === FALSE){
				$query .= " WHERE tr.triptype_id=".$db->Quote($this->_triptype_id);
			}
			else{
				$query .= " AND tr.triptype_id=".$db->Quote($this->_triptype_id);
			}
		}
		
		if($this->_searchfield){
			if(strpos($query, 'WHERE') === FALSE){
				$query .= " WHERE tr.title LIKE '%".$this->_searchfield."%'";
			}
			else{
				$query .= " AND tr.title LIKE '%".$this->_searchfield."%'";
			}
		}
		
		if($search_date_start){
			if(strpos($query, 'WHERE') === FALSE){
				$query .= " WHERE (tr.willekeurige_vertrek_datum=1 || (de.vertrek >= ".$db->Quote($search_date_start[2]."-".$search_date_start[1]."-".$search_date_start[0])." && de.terugkomst <= ".$db->Quote($search_date_end[2]."-".$search_date_end[1]."-".$search_date_end[0])." ))";
			}
			else{
				$query .= " AND (tr.willekeurige_vertrek_datum=1 || (de.vertrek >= ".$db->Quote($search_date_start[2]."-".$search_date_start[1]."-".$search_date_start[0])." && de.terugkomst <= ".$db->Quote($search_date_end[2]."-".$search_date_end[1]."-".$search_date_end[0])." ))";
			}
		}
		
		if(strpos($query, 'WHERE') === FALSE){
			$query .= " WHERE tr.state=1 ";
		}
		else{
			$query .= " AND tr.state=1 ";
		}
		$query .= " GROUP BY tr.trip_id ";
		$query .= " ORDER BY d.title ASC ";
		//print $query;
		return $query;
	}
	
	function getData() 
    {
	 	// if data hasn't already been obtained, load it
	 	
	 	if (empty($this->_data)) {
	 	    $query = $this->getDestinationTrips(JRequest::getInt('triptype_id'), JRequest::getInt('destination_id'), JRequest::getVar('search_date_start'), JRequest::getVar('search_date_end'));
	 	    $this->_data = $this->_getList($query, $this->getState('limitstart'),  $this->getState('limit'));		
	 	    if(empty($this->_data)){
	 	    	$this->_session->set('hasresults', 0);
	 	    }
	 	    else{
	 	    	$this->_session->set('hasresults', 1);
	 	    }
	 	}
	 
	 	return $this->_data;
    }
	
	function getTotal()
    {
	 	// Load the content if it doesn't already exist
	 	 
	 	if (empty($this->_total)) {
	 	    $query =$this->getDestinationTrips(JRequest::getInt('triptype_id'), JRequest::getInt('destination_id'), JRequest::getVar('search_date_start'), JRequest::getVar('search_date_end'));
	 	    $this->_total = $this->_getListCount($query);	
	 	    //print "total: ".$this->_total;
	 	}
	 	return $this->_total;
    }
    
	function getPagination()
    {
    	$db =& JFactory::getDBO();
		$db->setQuery('SELECT FOUND_ROWS();'); 
	 	if (empty($this->_pagination)) {
	 	    jimport('joomla.html.pagination');
	 	    // meer dan 30 pagina's is niet nodig.
	 	    $total = $db->loadResult();
	 	    $this->_pagination = new JPagination($total, $this->getState('limitstart'), $this->getState('limit') );

	 	}
	 	return $this->_pagination;
    }
}