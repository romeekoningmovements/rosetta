<?php
// No direct access
defined('_JEXEC') or die;
jimport('joomla.application.component.model');

class TripModelBook extends JModelLegacy
{
	public $_db;
	
	function __construct()
	{
		parent::__construct();
		
		$this->_db =& JFactory::getDBO();
	}
	
	public function getExtraOptions($trip_id){
		$db =& $this->_db;
		$query = "SELECT * FROM jos_xtrip_options WHERE trip_id=".$db->Quote($trip_id)." ORDER BY ordering";
		$db->setQuery($query);
		if(!$options = $db->loadObjectList()){
			return false;
		}
		return $options;
	}
	
	public function getTriptypeId($triptype){
		$db =& $this->_db;
		$query = "SELECT * FROM jos_xtrip_triptypes WHERE alias=".$db->Quote(str_replace(':','-',$triptype));
		$db->setQuery($query);
		if(!$triptype = $db->loadObject()){
			return false;
		}
		return $triptype->id;
	}
}
