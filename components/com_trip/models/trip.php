<?php
// No direct access
defined('_JEXEC') or die;
jimport('joomla.application.component.model');

class TripModelTrip extends JModelLegacy
{
	public $_db;
	
	function __construct()
	{
		parent::__construct();
		
		$this->_db =& JFactory::getDBO();
	}
	
	function getTrip($trip_id){
		$db=$this->_db;	
		$query =  " SELECT t.*, tt.triptype, tt.color, tt.quote, tt.image, tt.alias FROM #__xtrip_trips t ";
		$query .= " INNER JOIN jos_xtrip_triptypes tt ON tt.id=t.triptype_id ";
		$query .= " WHERE t.trip_id=".$db->Quote($trip_id);
		//$query .= " AND t.state = 1 ";

		$db->setQuery($query);
		if(!$obj=$db->loadObject()){
			echo "<br/>".$db->getErrorMsg();
			return false;
		}

		if ($obj->state == 0) {
			$app = JFactory::getApplication();
			$app->redirect(JRoute::_(JURI::root().'index.php'));
		}
		
		return $obj;
	}
	
	function getCategory($trip_id, $cat_id){
		$db=$this->_db;	
		$query =" SELECT * FROM #__xtrip_content c ";
		$query.=" WHERE c.trip_id=".$db->Quote($trip_id)." AND cat_id=".$db->Quote($cat_id);
		$db->setQuery($query);
		if(!$obj=$db->loadObject()){
			echo "<br/>".$db->getErrorMsg();
			return false;
		}

		return $obj;
	}
	
	function getCategories($trip_id){
		$db=$this->_db;	
		$query  =" SELECT * FROM #__xtrip_contentcategories cc ";
		$query .= "INNER JOIN #__xtrip_content c ON c.cat_id=cc.id";
		$query .=" WHERE c.trip_id=".$db->Quote($trip_id);
		$db->setQuery($query);
		if(!$obj=$db->loadObjectList()){
			echo "<br/>".$db->getErrorMsg();
			return false;
		}

		return $obj;
	}
}
