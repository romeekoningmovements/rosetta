<?php
// No direct access
defined('_JEXEC') or die;
jimport('joomla.application.component.model');

class TripModelDestinations extends JModelLegacy
{
	public $_db;
	public $_session;
	
	var $_pagination = null;
	var $_data = null;
	var $_total = null;
	var $_destination_id = null;
	
	function __construct()
	{
		parent::__construct();
		$menu =& JSite::getMenu();
		$menuitemid = JRequest::getInt('Itemid');
		$menuparams = $menu->getParams( $menuitemid );
		$this->_destination_id = $menuparams->get('destination_id');

		// Get pagination request variables
		$this->_session =& JFactory::getSession();
		$limit = JRequest::getInt('limit');
		$limitstart = JRequest::getVar('limitstart', 0, '', 'int');
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
	 	
	 	JRequest::setVar('limit', $limit);
	 	JRequest::setVar('limitstart', $limitstart);
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
	}
	
	function getDestination(){
		$db=$this->_db;		
		$query = "SELECT * FROM #__xtrip_destinations WHERE destination_id=".$db->Quote($this->_destination_id);
		$db->setQuery($query);
		if($catid = $db->loadObject()){
			return $catid;
		}
	}
	
	function getDepartures($trips){
		$db=$this->_db;		
		if(!empty($trips)){
			$data = array();
			foreach($trips as $trip){
				$query = "SELECT * FROM #__xtrip_departures WHERE id IN ('.$trip->departure.') AND vertrek > CURDATE() ORDER BY vertrek ASC LIMIT 2";
				$db->setQuery($query);
				if($departures = $db->loadObjectList()){
					$data[$trip->trip_id] = $departures;
				}
			}
			return $data;
		}
		return false;
	}
		
	function getDestinationTrips(){
		$db=$this->_db;		
		$sql = " SELECT title FROM #__xtrip_destinations WHERE destination_id=".$db->Quote($this->_destination_id);
		$db->setQuery($sql);
		$departure_title = $db->loadObject()->title;
		
		$query  ="select SQL_CALC_FOUND_ROWS  *, tr.reissom as tripreissom, tr.title as triptitle ,d.title as bestemming, c.color as tripcolor FROM #__xtrip_trips tr ";	
		$query .= "INNER JOIN #__xtrip_destinations d ON d.destination_id=tr.destination_id ";
		$query .= "INNER JOIN #__xtrip_triptypes tt ON tt.id=tr.triptype_id ";
		$query .= "INNER JOIN #__xtrip_colors c ON c.id=tt.color ";
		//$query .= "LEFT JOIN #__xtrip_content c ON c.trip_id=tr.id ";
		$query .= " WHERE tr.state = 1 ";
		if($this->_destination_id){
			if($departure_title){
				$query .= " AND (tr.destination_id=".$db->Quote($destination_id)." || tr.title LIKE '%".$departure_title."%') ";
			}
			else{
				$query .= "AND tr.destination_id=".$db->Quote($this->_destination_id);
			}
		}
		
		$query .= " ORDER BY d.title ASC ";
		
		return $query;
	}
	
	function getData() 
    {
	 	// if data hasn't already been obtained, load it
	 	
	 	if (empty($this->_data)) {
	 	    $query = $this->getDestinationTrips();
	 	    $this->_data = $this->_getList($query, $this->getState('limitstart'),  $this->getState('limit'));	
	 	    $this->getTotal();	
	 	    if(empty($this->_data)){
	 	    	$this->_session->set('hasresults', 0);
	 	    }
	 	    else{
	 	    	$this->_session->set('hasresults', 1);
	 	    }
	 	}
	 
	 	return $this->_data;
    }
	
	function getTotal()
    {
	 	// Load the content if it doesn't already exist
	 	 
	 	if (empty($this->_total)) {
	 	    $query =$this->getDestinationTrips();
	 	    $this->_total = $this->_getListCount($query);	
	 	    //print "total: ".$this->_total;
	 	}
	 	return $this->_total;
    }
    
	function getPagination()
    {
    	$db =& JFactory::getDBO();
		$db->setQuery('SELECT FOUND_ROWS();'); 
	 	if (empty($this->_pagination)) {
	 	    jimport('joomla.html.pagination');
	 	    // meer dan 30 pagina's is niet nodig.
	 	    //$total = $db->loadResult();
	 	    $total = $this->_total;
	 	    $this->_pagination = new JPagination($total, $this->getState('limitstart'), $this->getState('limit') );

	 	}
	 	return $this->_pagination;
    }
}