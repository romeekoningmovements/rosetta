<?php
// No direct access
defined('_JEXEC') or die;
jimport('joomla.application.component.model');

class TripModelTrips extends JModelLegacy
{
	public $_db;
	var $_session = null;
	
	var $_pagination = null;
	var $_data = null;
	var $_total = null;
	var $_triptype_id = null;
	
	function __construct()
	{
		parent::__construct();
		$menu =& JSite::getMenu();
		$menuitemid = JRequest::getInt('Itemid');
		$menuparams = $menu->getParams( $menuitemid );
		$this->_triptype_id = $menuparams->get('triptype_id');

		// Get pagination request variables
		$this->_session =& JFactory::getSession();
		$limit = JRequest::getInt('limit');
		$limitstart = JRequest::getVar('limitstart', 0, '', 'int');
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
	 	
	 	JRequest::setVar('limit', $limit);
	 	JRequest::setVar('limitstart', $limitstart);
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
	}
	
	function getCategory(){
		$db=$this->_db;		
		$query = "SELECT * FROM #__xtrip_triptypes WHERE id=".$db->Quote($this->_triptype_id);
		$db->setQuery($query);
		if($catid = $db->loadObject()){
			return $catid;
		}
	}
	
	function getDepartures($trips){
		$db=$this->_db;		
		if(!empty($trips)){
			$data = array();
			foreach($trips as $trip){				
				$query = "SELECT * FROM #__xtrip_departures WHERE id IN ('.$trip->departure.') AND vertrek > CURDATE() ORDER BY vertrek ASC LIMIT 2";
				$db->setQuery($query);
				if($departures = $db->loadObjectList()){
					$data[$trip->trip_id] = $departures;
				}
			}
			return $data;
		}
		return false;
	}
		
	function getTrips(){
		$db=$this->_db;		
		$query  ="select SQL_CALC_FOUND_ROWS  *,tr.title as triptitle,t.triptype as category FROM #__xtrip_trips tr ";	
		$query .= "INNER JOIN #__xtrip_triptypes t ON t.id=tr.triptype_id ";
		//$query .= "LEFT JOIN #__xtrip_content c ON c.trip_id=tr.id ";
		$query .= " WHERE tr.state = 1 ";
		
		if($this->_triptype_id){
			$query .= "AND tr.triptype_id=".$db->Quote($this->_triptype_id);
		}
		$query .= " ORDER BY tr.title ASC";
		//print $query;
		return $query;
	}
	
	function getData() 
    {
	 	// if data hasn't already been obtained, load it
	 	
	 	if (empty($this->_data)) {
	 	    $query = $this->getTrips();
	 	    $this->_data = $this->_getList($query, $this->getState('limitstart'),  $this->getState('limit'));	
	 	    $this->getTotal();	
	 	    if(empty($this->_data)){
	 	    	$this->_session->set('hasresults', 0);
	 	    }
	 	    else{
	 	    	$this->_session->set('hasresults', 1);
	 	    }
	 	}
	 
	 	return $this->_data;
    }
	
	function getTotal()
    {
	 	// Load the content if it doesn't already exist
	 	 
	 	if (empty($this->_total)) {
	 	    $query =$this->getTrips();
	 	    $this->_total = $this->_getListCount($query);	
	 	    //print "total: ".$this->_total;
	 	}
	 	return $this->_total;
    }
    
	function getPagination()
    {
    	$db =& JFactory::getDBO();
		$db->setQuery('SELECT FOUND_ROWS();'); 
	 	if (empty($this->_pagination)) {
	 	    jimport('joomla.html.pagination');
	 	    // meer dan 30 pagina's is niet nodig.
	 	    //$total = $db->loadResult();
	 	    $total = $this->_total;
	 	    $this->_pagination = new JPagination($total, $this->getState('limitstart'), $this->getState('limit') );

	 	}
	 	return $this->_pagination;
    }
}