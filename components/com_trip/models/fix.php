<?php
// No direct access
defined('_JEXEC') or die;
jimport('joomla.application.component.model');

class TripModelFix extends JModel
{
	public $_db;
	
	function __construct()
	{
		parent::__construct();
		
		$this->_db =& JFactory::getDBO();
	}
	
	function mergek2(){
		$db=$this->_db;	
		$query =" SELECT k.*, t.reisid FROM jos_k2_items k INNER JOIN jos_xtrip_trips t ON t.reisid=k.id ";
		$db->setQuery($query);
		if($results = $db->loadObjectList()){
			foreach($results as $trip){
				$query  = " UPDATE #__xtrip_trips SET title=".$db->Quote($trip->title).", triptype_id=".$db->Quote($trip->catid).", ";
				$query .= " introtext=".$db->Quote($trip->introtext).", `fulltext`=".$db->Quote($trip->fulltext).", state=".$db->Quote($trip->published)." WHERE reisid=".$trip->reisid;
				$db->setQuery($query);
				$db->Query();
			}
		}
	}
	
	function getXtripcontent(){
		$db=$this->_db;	
		$query = "SELECT t.reisid,i.* FROM jos_k2_items i INNER JOIN jos_xtrip_trips t ON t.extra_field_search=i.extra_fields_search WHERE i.catid NOT IN (11,12,13,14,15) ORDER BY i.extra_fields_search";
		$db->setQuery($query);
		if($results = $db->loadObjectList()){
			foreach($results as $result){
				$query  = " INSERT INTO #__xtrip_content (trip_id, cat_id, metadescription, metakeywords, introtext, `fulltext`, created, created_by, state, publish_up, publish_down) ";
				$query .= " VALUES (".$db->Quote($result->reisid).", ".$db->Quote($result->catid).", ".$db->Quote($result->metadesc).", ".$db->Quote($result->metakey).", ";
				$query .= $db->Quote($result->introtext).", ".$db->Quote($result->fulltext).",".$db->Quote($result->created).",".$db->Quote($result->created_by).",". $db->Quote($result->published).", ";
				$query .= $db->Quote($result->publish_up).", ".$db->Quote($result->publish_down).")";
				$db->setQuery($query);
				$db->Query();
			}
			exit;
		}
	}
	
	function fillExtraFieldsSearch(){
		$db=$this->_db;	
		$query = "SELECT reisid FROM #__xtrip_trips ";
		$db->setQuery($query);
		if($results = $db->loadObjectList()){
			foreach($results as $trip){
				$query = "SELECT * FROM jos_k2_items WHERE id=".$db->Quote($trip->reisid);
				$db->setQuery($query);
				if($item = $db->loadObject()){
					$query = "UPDATE jos_xtrip_trips SET extra_field_search=".$db->Quote($item->extra_fields_search)." WHERE reisid=".$db->Quote($trip->reisid);
					$db->setQuery($query);
					$db->Query();				
				}
			}
			exit;
		}
	}
	
	function setMetaDataTrips(){
		$db=$this->_db;	
		$query = "SELECT * FROM #__xtrip_trips ";
		$db->setQuery($query);
		if($results = $db->loadObjectList()){
			foreach($results as $result){
				$query = "SELECT * FROM #__k2_items WHERE id=".$db->Quote($result->reisid);
				$db->setQuery($query);
				if($item = $db->loadObject()){
					$query = "UPDATE #__xtrip_trips SET metadescription=".$db->Quote($item->metadesc).", metakeywords=".$db->Quote($item->metakey)." WHERE reisid=".$db->Quote($item->id);
					$db->setQuery($query);
					$db->Query();
				}
			}
		}
	}
	
	function fixAttachments(){
		$db = $this->_db;
		$query = "
		
		select a.filename, t.trip_id from jos_k2_attachments a
		inner join jos_k2_items i ON i.id=a.itemID
		inner join jos_xtrip_trips t ON t.extra_field_search=i.extra_fields_search
		WHERE i.catid IN (11,12,13,14,15) AND a.title = 'catimage'";

		$db->setQuery($query);
		if($items = $db->loadObjectList()){
			foreach($items as $item){
				$query = "UPDATE jos_xtrip_trips SET imagethumb=".$db->Quote($item->filename)." WHERE trip_id=".$db->Quote($item->trip_id);
				$db->setQuery($query);
				$db->Query();
			}
		}
	}
	
	function fixRouteImages(){
		$db = $this->_db;
		$query = "
		
		select a.filename, t.trip_id from jos_k2_attachments a inner join jos_k2_items i ON i.id=a.itemID inner join jos_xtrip_trips t ON t.extra_field_search=i.extra_fields_search WHERE i.catid IN (11,12,13,14,15) AND a.title = 'route'";

		$db->setQuery($query);
		if($items = $db->loadObjectList()){
			foreach($items as $item){
				$filename = explode('.',$item->filename);
				$query = "UPDATE jos_xtrip_trips SET route=".$db->Quote($filename[0].".jpg")." WHERE trip_id=".$db->Quote($item->trip_id);
				$db->setQuery($query);
				$db->Query();
			}
		}
		
	}
}
