<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class TripModelProcess extends JModel {
    /**
	 * Constructor
	 */
    var $_trip_id,$_triptype_id,$_departure_id,$_persons,$_option1,$_option2,$_option3,$_db,$_debug=false, $_defaultoptions,$_persondata,$_booking, $_bookings_id,$_font,$_size,$_pdf, $_form_value,$_mailto, $_concerning;
    
	function __construct() {
        $session =& JFactory::getSession();
	    define('EURO', chr(128));
	    $this->_db=JFactory::getDBO();
	    $this->_persondata=JRequest::getVar('persondata');
	   
	    $data=&JRequest::get('POST');
	    $ip = $_SERVER['REMOTE_ADDR'];
	    /*
	    if(JRequest::getVar('task') == 'articlespdf'){
	    	$this->generateTripOverview();
	    }
	    $this->_form_value = $data;
	    $this->getBooking($data);
	    $this->createPdf();
	    */
		parent::__construct();
    } 
    
    function createSitemapTrips(){

    	require_once(JPATH_COMPONENT.DS.'helpers'.DS.'route.php');
		$sitemappath=JPATH_SITE;
		$db=&JFactory::getDBO();
		$query =" SELECT * FROM jos_xtrip_trips WHERE state=1";
		$db->setQuery($query);
		if(!$obj=$db->loadObjectList()){
			return false;
		}
		$sitemapheader ='<?xml version="1.0" encoding="utf-8"?>'."\n";
		$sitemapheader.='<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
		$sitemapfooter='</urlset>';
		
		$i=1;
		$sitemapfile="sitemap".$i.".xml";
		$k=0;
		$sitemapcontent="";
	
		$file=fopen($sitemappath.DS.$sitemapfile,"w+");
		fwrite($file,$sitemapheader);

		foreach($obj as $trip){
			$alias = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($trip->title, ENT_QUOTES, 'UTF-8'));
			$alias = strtolower($alias);
			$alias = str_replace("  "," ",$alias);
			$alias = str_replace(" - ","-",$alias);
			$alias = str_replace(" ","-",$alias);
			// JRoute::_('index.php?option=com_trip&view=trip&trip_id='.$trip->trip_id.":".$alias.'&Itemid='.TripHelperRoute::getItemIdByTriptype($trip->triptype_id))
			$url=JURI::base().JRoute::_('index.php?option=com_trip&view=trip&trip_id='.$trip->trip_id.":".$alias.'&Itemid='.TripHelperRoute::getItemIdByTriptype($trip->triptype_id));
			
			$sitemapcontent="<url>\n";
			$sitemapcontent.="<loc><![CDATA[".$url."]]></loc>"."\n";
			$sitemapcontent.="<lastmod>".date('Y-m-d')."</lastmod>"."\n";
			$sitemapcontent.="<changefreq>daily</changefreq>"."\n";
			$sitemapcontent.="<priority>0.5</priority>"."\n";
			$sitemapcontent.="</url>"."\n";
			fwrite($file,$sitemapcontent);
			if($k>45000){
				fwrite($file,$sitemapfooter);
				fclose($file);
				$i++;
				$file=fopen($sitemappath.DS."sitemap".$i.".xml","w+");
				fwrite($file,$sitemapheader);
				$k=0;
	
			}
			$k++;
		}
		
		fwrite($file,$sitemapfooter);
		fclose($file);
	}
	
	function createSitemapContent(){
		require_once(JPATH_COMPONENT.DS.'helpers'.DS.'route.php');
		$sitemappath=JPATH_SITE;
		$db=&JFactory::getDBO();
		$query =" SELECT t.trip_id, t.triptype_id, t.title, cc.id as cat_id, cc.alias FROM jos_xtrip_content c
				  INNER JOIN jos_xtrip_contentcategories cc ON cc.id=c.cat_id
				  INNER JOIN jos_xtrip_trips t ON t.trip_id=c.trip_id
				  WHERE t.state=1 AND LENGTH(c.introtext)>10
				  ORDER BY c.trip_id ASC";
		$db->setQuery($query);
		if(!$obj=$db->loadObjectList()){
			return false;
		}
		$sitemapheader ='<?xml version="1.0" encoding="utf-8"?>'."\n";
		$sitemapheader.='<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
		$sitemapfooter='</urlset>';
		
		$i=2;
		$sitemapfile="sitemap".$i.".xml";
		$k=0;
		$sitemapcontent="";
	
		$file=fopen($sitemappath.DS.$sitemapfile,"w+");
		fwrite($file,$sitemapheader);

		foreach($obj as $trip){
			$alias = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($trip->title, ENT_QUOTES, 'UTF-8'));
			$alias = strtolower($alias);
			$alias = str_replace("  "," ",$alias);
			$alias = str_replace(" - ","-",$alias);
			$alias = str_replace(" ","-",$alias);
			// JRoute::_('index.php?option=com_trip&view=trip&trip_id='.$trip->trip_id.":".$alias.'&Itemid='.TripHelperRoute::getItemIdByTriptype($trip->triptype_id))
			$url=JURI::base().JRoute::_('index.php?option=com_trip&view=trip&trip_id='.$trip->trip_id.":".$alias."&cat_id=".$trip->cat_id.":".$trip->alias."&Itemid=".TripHelperRoute::getItemIdByTriptype($trip->triptype_id));

			$sitemapcontent="<url>\n";
			$sitemapcontent.="<loc><![CDATA[".$url."]]></loc>"."\n";
			$sitemapcontent.="<lastmod>".date('Y-m-d')."</lastmod>"."\n";
			$sitemapcontent.="<changefreq>daily</changefreq>"."\n";
			$sitemapcontent.="<priority>0.8</priority>"."\n";
			$sitemapcontent.="</url>"."\n";
			fwrite($file,$sitemapcontent);
			if($k>45000){
				fwrite($file,$sitemapfooter);
				fclose($file);
				$i++;
				$file=fopen($sitemappath.DS."sitemap".$i.".xml","w+");
				fwrite($file,$sitemapheader);
				$k=0;
	
			}
			$k++;
		}
		fwrite($file,$sitemapfooter);
		fclose($file);
	}
    
     function generateTripOverview($trip_id){
     	$db = $this->_db;
     	$query = "SET NAMES Unicode";
		$db->setQuery($query);
		$db->query();
     	
     	$session =& JFactory::getSession();

    	$query  = "SELECT c.*, cc.*, t.title as triptitle, t.introtext as quote, t.`fulltext` as triptext FROM jos_xtrip_content c ";
    	$query .= "INNER JOIN jos_xtrip_contentcategories cc ON cc.id=c.cat_id ";
    	$query .= "INNER JOIN jos_xtrip_trips t ON t.trip_id=c.trip_id ";
    	$query .= "WHERE c.state=1 AND c.cat_id IN (SELECT cc2.id from jos_xtrip_contentcategories cc2 WHERE cc2.id != 32 AND cc2.id != 4 AND cc2.id != 5 AND cc2.id != 8) AND c.trip_id=".$trip_id;


    	$db->setQuery($query);
    	if($results = $db->loadObjectList()){	
    		require_once(JPATH_COMPONENT.DS.'classes'.DS.'dompdf'.DS.'dompdf_config.inc.php');
    		$dompdf = new DOMPDF();

    		$html = "
    			<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
				<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='nl-nl' lang='nl-nl' >
    				<head>
						 <meta http-equiv='content-type' content='text/html; charset=utf-8' />
    					<style>
    						body{
    							margin-bottom:110px;
    							font-family:'Helvetica Neue', Helvetica, Verdana, Arial, sans-serif;
    							font-size:14px;
    							line-height:20px;
    							color:#666;
    						}
    						.quote{
								font-size:19pt;
								line-height:20pt;
								color:#B8B125;
    						}
    						#testje {z-index:3;}
    						h2{page-break-before:always;}
    					</style>
    				</head>
    				<body>
    		";
    		
    		$html .= "<img style='position:absolute; left:0; top:0; margin:0; padding:0;' src='images/format2.jpg' width='100%' height='100%'/>";
    		if(isset($results[0])){
    			$html .= "<div style='position:absolute; left:250px; top:740px; margin:0; padding:0; font-size:18pt; font-weight:bold;'>".$results[0]->triptitle."</div>";
    			$html .= "<h2 style='position:relative; z-index:5;'><span class='quote'>".strip_tags($results[0]->quote)."</span></h2>";
    			$html .= "<img style='z-index:4; position:fixed;' src='images/format3.jpg' width='100%' height='100%'/>";
	    		$html .= "<div style='position:relative; z-index:5;'>".strip_tags($results[0]->triptext,'<p><a><strong><div><table><tr><td><th><span><br><br/><ul><li><tbody><thead>')."</div>";
    		}
    		
    		
    		foreach($results as $article){
    			if(strlen($article->introtext) > 10){
	    			$html .= "<h2 style='position:relative; z-index:5;'>".$article->title."</h2>";
	    			$html .= "<img style='z-index:4; position:fixed;' src='images/format3.jpg' width='100%' height='100%'/>";
	    			$html .= "<div style='position:relative; z-index:5;'>".strip_tags($article->introtext,'<p><a><strong><div><table><tr><td><th><span><br><br/><ul><li><tbody><thead>')."</div>";
	    		}	
    		}
    		$html .= "
    				</body>
    			</html>
    		";

    		$html = mb_convert_encoding($html, 'HTML-ENTITIES', 'utf-8');
			$dompdf->load_html($html);
			$dompdf->render();
			$dompdf->stream("reisinformatie.pdf");
    		exit;
    	}
    	return false;
    }
    
    function decode_entities_full($string, $quotes = ENT_COMPAT, $charset = 'ISO-8859-1') {
	  return html_entity_decode(preg_replace_callback('/&([a-zA-Z][a-zA-Z0-9]+);/', 'convert_entity', $string), $quotes, $charset); 
	}
	    
   
    
    function getBooking($data){
    	if(isset($data['form']['email'][0])){
    		$this->_mailto = $data['form']['email'][0];
    	}
    	
    	if(isset($data['form']['achternaam'][0])){
    		$this->_concerning = $data['form']['achternaam'][0];
    	}
	
        $session =& JFactory::getSession();
        $ip = $_SERVER['REMOTE_ADDR'];
    	

        $db=$this->_db;

        $trip_id=$session->get('trip_id');
        $departure_id=$session->get('departure_id');
        $persons=$session->get('persons');
        $defaultoptions=$session->get('defaultoptions');
        $options=$session->get('options');
        
        $bookingsnumber = '000';
        $query = "SELECT max(id) as total FROM #__xtrip2_bookings";
        $db->setQuery($query);
        if($result = $db->loadObject()){
        	$bookingsnumber .= ($result->total + 1);
        }
        else{
        	$bookingsnumber .= 1;
        }

        $query =  "INSERT INTO #__xtrip2_bookings (persons, bookingnumber, trip_id, departure_id, options, default_options, form_values) ";
        $query .= "VALUES (".$db->Quote($persons).", ".$db->Quote($bookingsnumber).", ".$db->Quote($trip_id).", ".$db->Quote($departure_id).", ".$db->Quote(serialize($options)).", ".$db->Quote(serialize($defaultoptions));
        $query .= ", ".$db->Quote(serialize($data['form'])).")";
        $db->setQuery($query);
       	if(!$db->Query()){
       		return false;
       	}
       	$this->_bookings_id = mysql_insert_id();
        $booking=new StdClass();
        // general info trip
        $query="SELECT * FROM jos_k2_items WHERE id=".$db->Quote($trip_id);
        $db->setQuery($query);
        $tripinfo=$db->loadObject();
  
        // financial trip info + default options
        $query="SELECT * FROM jos_xtrip2_trips WHERE reisid=".$db->Quote($trip_id);
        $db->setQuery($query);
        $trip=$db->loadObject();
      
        // opties ophalen
		$options = $session->get('calculatie')->options;
		$departure = '';
		if(!JRequest::getVar('vertrekdatum')){
	        $query="SELECT vertrek FROM jos_xtrip2_departures WHERE id=".$db->Quote(JRequest::getInt('departure_id'));
	        $db->setQuery($query);
	        $departure=$db->loadObject();
		}
		else{
			$exploded = explode('-', JRequest::getVar('vertrekdatum'));
			$departure = $exploded[2]."-".$exploded[1]."-".$exploded[0];
		}
		
		
        
        $query="SELECT triptype FROM jos_xtrip2_triptypes WHERE id=".$db->Quote(JRequest::getInt('triptype_id'));
        $db->setQuery($query);
        $triptype=$db->loadObject();
        
        $query="SELECT ka.filename FROM jos_k2_attachments ka INNER JOIN jos_k2_items ki ON ki.id=ka.itemID WHERE ka.title='pageimg' AND ki.id=".$db->Quote($trip_id);
        $db->setQuery($query);
        $tripimage=$db->loadObject();
        
        $booking->triptype = $triptype->triptype;
        if(!JRequest::getVar('vertrekdatum')){
        	$booking->departure = $departure->vertrek;
        }
        else{
        	$booking->departure = $departure;
        }
        $booking->triptitle=$tripinfo->title;
        $booking->description=$tripinfo->introtext;
        $booking->fulltext=$tripinfo->fulltext;
        $booking->trip=$trip;
        $booking->options=$options;
        $booking->defaultoptions = $defaultoptions;
        $booking->persons = $persons;
        $booking->image = $tripimage->filename;
      
        if(isset($data['form']['same_address'])){
        	$booking->same_address = $data['form']['same_address'];
        }
     
        // trip booking ophalen
	    $query="SELECT * FROM #__xtrip2_bookings WHERE id=".$db->Quote($this->_bookings_id);
	    $db->setQuery($query);
	    if(!$obj=$db->loadObject()){
	        echo $db->getErrorMsg();
	        return false;
	    }
	    $this->_trip=$obj;
	    
       $tripoptions = unserialize($this->_trip->options);
       $i = 0;
   	   if($ip == '95.97.95.78'){
			//print $query;
			//print "<pre>";
			//print_r($booking);
			//print_R($_REQUEST);
			//print "</pre>";
			//exit;
		}
	 
       foreach($booking->options as $option){
        	foreach($tripoptions as $key=>$tripoption){
        		if($key == $option->id){
        			$numbers = '';
        			if(!is_array($tripoption)){
        				for($j = 1; $j <= $booking->persons; $j++){
        					$numbers .= $j.",";
        				}
        				$numbers = substr($numbers, 0, -1);
        				$booking->options[$i]->for = array($numbers);
        			}
        			else{
        				$values = array();
        				foreach($tripoption as $k=>$value){
        					if($value == 1){
        						$values[0] .= $k.",";
        					}
        				}
        				$values[0] = substr($values[0], 0, -1);
        				$booking->options[$i]->for = $values;
        			}
        		}
        	}
        	$i++;
        }
    	
        $this->_booking=$booking;
	 
        return $booking; 
    }
    
   function createPersonstable($pdf, $post){
	   	error_reporting(E_ALL);
	   	ini_set('display_errors', 1);
   		$session =& JFactory::getSession();
        $values=$post;
        $persons=array();
        foreach($values as $key=>$value){
        	if($key != 'same_address'){
	            if(is_array($value)){
	                foreach($value as $nr=>$val){
	                    $persons[$nr][$key]=$val;    
	                }
	            }
	            else{
	                //$persons
	                $persons[0][$key]=$value;
	            }
        	}
        }
        // calculatie uit het model book ophalen
        $booking = json_decode($session->get('booking'));
        $hoofdaanvrager=array('initials','nickname','officialname','lastname', 'birthdate','gender', 'passport', 'street', 'zipandcity', 'phone', 'phonework', 'email', 'remarks');  
        $medereizigers=array('initials','nickname','officialname','lastname', 'birthdate','gender', 'passport', 'street','zipandcity', 'phone', 'phonework' );   
        $maxrows = 0;

        $html = '';
        $p=0;
        $stepin = false;
        
        foreach($persons as $person){
        	if($p % 2 == 0 && $p != 0){
        		$pdf->addPage();
        	}
        	if($p == 0){
        		$maxrows=count($hoofdaanvrager);
           		$pdf->Cell(110,5,'Persoon '.($p+1),1,0,'C',1);
        	}
        	else{
        		$maxrows=count($medereizigers);
        		$pdf->Cell(110,5,'Persoon '.($p+1),1,0,'C',1);
        	}
            $pdf->Ln();
            
            $multicell = false;
            for($i=0;$i<$maxrows;$i++){
            	if($p == 0){
	                if(isset($person[$hoofdaanvrager[$i]])){
	                	if($hoofdaanvrager[$i] == 'phone'){
	                		$pdf->Cell(40,5,html_entity_decode('telpriv&eacute;'),1);
	                	}
	                	else{
	                		$pdf->Cell(40,5,$hoofdaanvrager[$i],1);
	                	}
		                if($hoofdaanvrager[$i] == 'remarks'){
		                	$multicell = true;
	                   	 	$pdf->MultiCell(70,5,utf8_decode($person[$hoofdaanvrager[$i]]),1);   
	            		}
	                	//elseif($hoofdaanvrager[$i] == 'straatnaam'){
	                   	// 	$pdf->Cell(70,5,($person[$hoofdaanvrager[$i]]." ".$person['nummer']." ".$person['toevoeging']),1);   
	                	//}
	                	else{
	                    	$pdf->Cell(70,5,utf8_decode($person[$hoofdaanvrager[$i]]),1);   
	                	}               
	                }    
            	}
            	else{	
            		 if(isset($person[$medereizigers[$i]])){ 		 	
        		 		if($medereizigers[$i] == 'phone'){
	                    	$pdf->Cell(40,5,html_entity_decode('telpriv&eacute;'),1);
        		 		}
        		 		else{
        		 			$pdf->Cell(40,5,$medereizigers[$i],1);
        		 		}
	                    $pdf->Cell(70,5,utf8_decode($person[$medereizigers[$i]]),1);	
	                } 
            	}
            	if(!$multicell){
                	$pdf->Ln(); 
            	}
                $multicell = false;    
            }
            
            $pdf->Cell(40,5,'geselecteerde opties',1);
            
            // nog naar kijken
            // default options per persoon toevoegen
            //print "<pre>";
            //print_R($booking);
            //print "</pre>";
            //exit;
            /*
            $o = 0;
            foreach($booking->defaultoptions as $key=>$defaultoption){
            	if($key == 0 && $defaultoption==1){
            		$pdf->Cell(70,5,'- annuleringsverzekering',1,2);
            	}
            	if($key == 1 && $defaultoption == 1){
            		$pdf->Cell(70,5,'- gescheiden facturering',1,2);
            	}
            	if($key == 2 && $defaultoption==1){
            		$pdf->Cell(70,5,'- visumkosten',1,2);
            	}
            	$o++;
            }
          
            // extra opties per persoon toevoegen
            foreach($this->_booking->options as $option){
            	if(isset($option->for[0])){
            		if(strlen($option->for[0]) > 1){
            			$exploded = explode(',', $option->for[0]);         
            			foreach($exploded as $explode){
            				if($explode == ($p+1)){
            					$pdf->Cell(70,5,"- ".utf8_decode($option->description),1,2);
            				}
            			}
            		}
            		else{
            			$pdf->Cell(70,5,"- ".utf8_decode($option->description),1,2);
            		}
            	}
            }
            */
            $stepin = false;
            $p++;
        	$pdf->Ln();     
        	$pdf->Ln();   
        	
        }        
        return;
    }
    

    
    function createPdf(){
    		$session =& JFactory::getSession();
    		$booking = json_decode($session->get('booking'));
    		$post = JRequest::get('post');

    		// aantal personen in een string weergeven voor de pdf
    		$concerningall = '';
	        for($i = 1; $i < $booking->nrpersons+1; $i++){
	        	$concerningall .= $i.",";
	        }
	        $concerningall = substr($concerningall, 0, -1);
        		
    		require_once(JPATH_COMPONENT.DS.'classes'.DS.'dompdf'.DS.'dompdf_config.inc.php');
    		$dompdf = new DOMPDF();

    		$html = "
    			<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
				<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='nl-nl' lang='nl-nl' >
    				<head>
						 <meta http-equiv='content-type' content='text/html; charset=utf-8' />
    					<style>
    						body{
    							margin-bottom:110px;
    							font-family:'Helvetica Neue', Helvetica, Verdana, Arial, sans-serif;
    							font-size:14px;
    							line-height:20px;
    							color:#666;
    						}
    						.quote{
								font-size:19pt;
								line-height:20pt;
								color:#B8B125;
    						}
    						#testje {z-index:3;}
    						h2{page-break-before:always;}
    						.table {
							    margin-bottom: 20px;
							    width: 100%;
							}
							.table-bordered {
							    -moz-border-bottom-colors: none;
							    -moz-border-image: none;
							    -moz-border-left-colors: none;
							    -moz-border-right-colors: none;
							    -moz-border-top-colors: none;
							    border-collapse: separate;
							    border-color: #DDDDDD #DDDDDD #DDDDDD -moz-use-text-color;
							    border-radius: 4px 4px 4px 4px;
							    border-style: solid solid solid none;
							    border-width: 0px 1px 1px 0;
							}
							
							.table-bordered th, .table-bordered td {
							    border-left: 1px solid #DDDDDD;
							}
							
							.table th, .table td {
							    border-top: 1px solid #DDDDDD;
							    line-height: 20px;
							    padding: 8px;
							    text-align: left;
							    vertical-align: top;
							}
							
							.table th{
								background:#D7D039;
							}
    					</style>
    				</head>
    				<body>
    		";

    		$html .= "<img style='position:absolute; left:0; top:0; z-index:0; margin:0; padding:0;' src='images/format3.jpg' width='100%' height='100%'/>";
    		$html .= "<div style='position:absolute; z-index:1;'>";
    		$html .= "<span>Nijmegen, ".strftime('%A %e %B %Y')."</span><br/><br/><br/>";
    		if($post['gender1'] == 'm'){
	    		$html .= "Beste meneer ".$post['initials1'].". ".$post['lastname1']."<br/><br/>";
    		}
    		else{
	    		$html .= "Beste mevrouw ".$post['initials1'].". ".$post['lastname1']."<br/><br/>";
    		}
 
    		$html .= "Bedankt voor uw boeking bij Rosetta Reizen. Hieronder vindt u alle gegevens van de reis. Als er vragen zijn of er staan onjuistheden in onderstaand overzicht, neem dan s.v.p contact met ons op.<br/><br/>";   
    		$html .= "<b>Reisgegevens</b><br/><br/>";
    		$html .= "
    			<table class='table table-bordered'>
    				<tbody>
    					<tr>
    						<td>Reis</td>
    						<td>".$booking->title."</td>
    					</tr>
    					<tr>
    						<td>Reistype</td>
    						<td>".$booking->triptype."</td>
    					</tr>
    					<tr>
    						<td>Vertrekdatum</td>
    						<td>".strftime("%A %e %B %Y",strtotime($booking->vertrekdatum))."</td>
    					</tr>
    					<tr>
    						<td>Aantal personen</td>
    						<td>".$booking->nrpersons."</td>
    					</tr>
    				</tbody>
    			</table><br/><br/>
    		";		
    		$html .= "<b>Berekening reissom</b><br/><br/>";

    		$html .= "
    			<table class='table table-bordered'>
    				<thead>
    					<tr>
    						<th>Basis</th>
    						<th>Prijs</th>
    						<th>Betreft perso(o)n(en)</th>
    						<th>Subtotaal</th>
    					</tr>
    				</thead>
    				<tbody>
    					<tr>
    						<td>Reissom</td>
    						<td style='text-align:right;'>€ ".number_format(($booking->reissom / $booking->nrpersons), 2, '.',',')."</td>
    						<td style='text-align:right;'>".$concerningall."</td>
    						<td style='text-align:right;'>€ ".number_format($booking->reissom, 2, '.',',')."</td>
    					</tr>";
    		if($booking->luchthaventax > 0){
    			$html .= "
    					<tr>
    						<td>Luchthaventax</td>
    						<td style='text-align:right;'>€ ".number_format(($booking->luchthaventax / $booking->nrpersons), 2, '.',',')."</td>
    						<td style='text-align:right;'>".$concerningall."</td>
    						<td style='text-align:right;'>€ ".number_format($booking->luchthaventax, 2,'.',',')."</td>
    					</tr>";
    		}
    		$html .= "
    					<tr>
    						<td>Boekingsbijdrage</td>
    						<td style='text-align:right;'>€ ".number_format(($booking->boekingsbijdrage),2,'.',',')."</td>
    						<td style='text-align:right;'>".$concerningall."</td>
    						<td style='text-align:right;'>€ ".number_format($booking->boekingsbijdrage,2,'.',',')."</td>
    					</tr>
    					<tr>
    						<td>Calamiteitenfonds</td>
    						<td style='text-align:right;'>€ ".number_format(($booking->calamiteitenfonds),2,'.',',')."</td>
    						<td style='text-align:right;'>".$concerningall."</td>
    						<td style='text-align:right;'>€ ".number_format($booking->calamiteitenfonds,2,'.',',')."</td>
    					</tr>";
    		if($booking->visumnodig){
	    		$html .= "
	    			<tr>
	    				<td>Visumkosten</td>
						<td style='text-align:right;'>€ ".number_format(($booking->visumkosten / $booking->nrpersons),2,'.',',')."</td>
						<td style='text-align:right;'>".$concerningall."</td>
						<td style='text-align:right;'>€ ".number_format($booking->visumkosten,2,'.',',')."</td>
	    			</tr>
	    		";
    		}
    		if($booking->annuleringskosten > 0 || $booking->gescheiden == 'Ja' || isset($booking->extraoptions)){
    			$html .= "
    					<tr>
    						<td colspan='4'>Opties</td>
    					</tr>";
    			if($booking->annuleringskosten > 0){
	    			$html .= "
	    				<tr>
	    					<td>- Annuleringskosten</td>
	    					<td></td>
	    					<td style='text-align:right;'>".$concerningall."</td>
	    					<td style='text-align:right;'>€ ".number_format($booking->annuleringskosten,2,'.',',')."</td>
	    				</tr>
	    			";
    			}
    			if($booking->gescheiden == 'Ja'){
	    			$html .= "
	    				<tr>
	    					<td>- Gescheiden facturering</td>
	    					<td></td>
	    					<td style='text-align:right;'>".$concerningall."</td>
	    					<td style='text-align:right;'>Ja</td>
	    				</tr>
	    			";
    			}
    			if(isset($booking->extraoptions)){
    				foreach($booking->extraoptions as $key=>$val){
		    			$html .= "
		    				<tr>
		    					<td>- ".$val->name."</td>
		    					<td style='text-align:right;'>€ ".number_format(($val->total / $booking->nrpersons),2,'.',',')."</td>
		    					<td style='text-align:right;'>".$concerningall."</td>
		    					<td style='text-align:right;'>€ ".number_format($val->total,2,'.',',')."</td>
		    				</tr>
		    			";
		    		}
    			}
    		}
    		
    		$html .= "
    					<tr>
    						<td colspan='3'>Totale kosten</td>
    						<td style='text-align:right;'>€ ".number_format($booking->total,2,'.',',')."</td>
    					</tr>
    				</tbody>
    			</table>
    		";

    		$html .= "<h2>Reisgezelschap</h2><br/><br/>";
    		for($i = 1; $i < (int)$booking->nrpersons+1; $i++){
	    		$html .= "
	    			<table class='table table-bordered'>
	    				<thead>
	    					<tr>
	    						<th>Persoon ".$i."</th>
	    						<th></th>
	    					</tr>
	    				</thead>
	    				<tbody>
	    					<tr>
	    						<td>Voorletters</td>
	    						<td>".$post['initials'.$i]."</td>
	    					</tr>
	    					<tr>
	    						<td>Roepnaam</td>
	    						<td>".$post['nickname'.$i]."</td>
	    					</tr>
	    					<tr>
	    						<td>Voornaam</td>
	    						<td>".$post['officialname'.$i]."</td>
	    					</tr>
	    					<tr>
	    						<td>Achternaam</td>
	    						<td>".$post['lastname'.$i]."</td>
	    					</tr>
	    					<tr>
	    						<td>Geboortedatum</td>
	    						<td>".$post['birthdate'.$i]."</td>
	    					</tr>
	    					<tr>
	    						<td>Geslacht</td>
	    						<td>".$post['gender'.$i]."</td>
	    					</tr>
	    					<tr>
	    						<td>Paspoortnr</td>
	    						<td>".$post['passport'.$i]."</td>
	    					</tr>
	    					<tr>
	    						<td>Straatnaam</td>
	    						<td>".$post['street'.$i]."</td>
	    					</tr>
	    					<tr>
	    						<td>Huisnummer</td>
	    						<td>".$post['housenr'.$i]."</td>
	    					</tr>
	    					<tr>
	    						<td>Postcode en plaats</td>
	    						<td>".$post['zipandcity'.$i]."</td>
	    					</tr>
	    					<tr>
	    						<td>Telprivé</td>
	    						<td>".$post['phone'.$i]."</td>
	    					</tr>
	    					<tr>
	    						<td>Telwerk</td>
	    						<td>".$post['phonework'.$i]."</td>
	    					</tr>
	    					<tr>
	    						<td>Email</td>
	    						<td>".$post['email'.$i]."</td>
	    					</tr>";
	    		if($i == 1){
	    			$html .= "<tr>
	    						<td>Opmerkingen</td>
	    						<td>".$post['remarks']."</td>
	    					</tr>";
	    		}
	    		$html .= "	</tbody>
	    			</table>
	    		";
	    		if($i != (int)$booking->nrpersons){
		    		$html .= "<h2></h2>";
	    		}
    		}
    	
    		$html .= "
    					</div>
    				</body>
    			</html>
    		";

    		$html = mb_convert_encoding($html, 'HTML-ENTITIES', 'utf-8');
			$dompdf->load_html($html);
			$dompdf->render();
			$pdf = $dompdf->output();
			
			
			$db =& JFactory::getDBO();
			$query = "INSERT INTO #__xtrip_bookings (trip_id, departure_id, form_values, persons, `datum`) VALUES (".$booking->trip_id.", ".$db->Quote($booking->departure).", ".$db->Quote(json_encode($post)).", ".$booking->nrpersons.", ".$db->Quote(date('Y-m-d H:i:s')).")";	
			$db->setQuery($query);
			$db->Query();

			$pdfsavepath = JPATH_COMPONENT.DS.'assets'.DS.'pdf'.DS."boekingsbevestiging_".$db->insertid()."_".date('Y-m-d').".pdf";
			file_put_contents($pdfsavepath, $pdf);

			//$dompdf->stream("reisinformatie.pdf");

	        if($this->sendConfirmationMail($pdfsavepath, $post['email1'])){
	        	// redirect naar boekingsbevestigings pagina.
	        	$app =& JFactory::getApplication();
	       	 	$app->redirect(JRoute::_('index.php?Itemid=152'));  
	        }
	        print "niet kunnen mailen";
	        exit;
    }   
    
    function sendConfirmationMail($pdfsavepath, $emailto){
    	$mailer =& JFactory::getMailer();
    	$mailer->addAttachment($pdfsavepath);
    	//$recipients = array($this->_mailto, 'info@rosettareizen.nl');
    	$mailer->addRecipient($emailto);
    	$mailer->addBCC(array('info@rosettareizen.nl'));
    	$mailer->setSubject('uw boekingsbevestiging bij Rosetta Reizen.');
    	$mailer->From = 'info@rosettareizen.nl';
    	$mailer->isHtml(true);
    	$mailer->setBody("<p><img src='".JURI::base()."templates/rosettareizen/img/logoZW.jpg' width='100%'/></p><p>Bedankt voor uw boeking bij Rosetta reizen. Hieronder vindt u alle gegevens van de reis. Als er vragen zijn of er staan onjuistheden in onderstaand overzicht, neem dan s.v.p. contact met ons op.</p>");

    	if($mailer->Send()){
    		return true;
    	}
    	return false;
    }
    
    function writeBold($text, $size){
           $this->_pdf->setFont('Times','B',$size);
           $this->_pdf->Write(7,$text);
           $this->_pdf->setFont('Times','',12);
    }  
    
    function sendMail($subject,$email,$cc,$html){
        $mail=&JFactory::getMailer();
        $mail->addRecipient('marc@i-brix.nl');
        $mail->setSubject("test");
        $mail->setBody($html);
        if(!$mail->send()){
            echo "mail verzenden mislukt";
        }
    }
}