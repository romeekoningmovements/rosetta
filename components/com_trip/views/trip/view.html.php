<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die;
jimport('joomla.application.component.view');
class TripViewTrip extends JViewLegacy
{
	protected $state;
	protected $items;
	protected $category;
	protected $categories;
	protected $pagination;

	function display($tpl = null)
	{
		$app		= JFactory::getApplication();
		$user		= JFactory::getUser();
		//$params		= $app->getParams();

		//$products	= $this->get('Products');
		$trip_id=JRequest::getInt('trip_id');
		$cat_id = JRequest::getInt('cat_id');
		
		$model = $this->getModel();
		$trip=$model->getTrip($trip_id);
		$this->assignRef('trip',$trip);
		
		if($cat_id){
			$category = $model->getCategory($trip_id, $cat_id);
			$this->assignRef('category',$category);
		}
		
		$categories = $model->getCategories($trip_id);
		$this->assignRef('categories', $categories);
		
		parent::display($tpl);
	}
}