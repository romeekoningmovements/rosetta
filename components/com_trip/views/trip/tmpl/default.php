<?php
	defined('_JEXEC') or die('Restricted access');
	$doc =& JFactory::getDocument();
	$trip = $this->trip;
	
	$application =& JFactory::getApplication();
	$document =& JFactory::getDocument();

	if($application->getCfg('sitename_pagetitles') == 1){
			$document->setTitle($application->getCfg('sitename').' - '.$trip->title); 
		}elseif(2){
			$document->setTitle($trip->title.' - '.$application->getCfg('sitename'));
		}

	$categories = $this->categories;
	$categoryobj = $this->category;

	$activecategory = JRequest::getInt('cat_id');
	$triptype_id = JRequest::getInt('triptype_id');
	

	$doc->setMetaData('description', $trip->metadescription);
	$doc->setMetaData('keywords', $trip->metakeywords);

	$text = JHtml::_('content.prepare', $trip->fulltext);


	$article = new stdClass;
	$article->text = $text;
	
	// add more to parameters if needed
	$params = new JObject;
	 
	// Note JDispatcher is deprecated in favour of JEventDispatcher in Joomla 3.x however still works.
	JPluginHelper::importPlugin('content');
	$dispatcher = JDispatcher::getInstance();


	$dispatcher->trigger('onContentPrepare', array('com_trip', &$article, &$params, 0));
?>
<span class="triptypetitle" ><?php echo $trip->triptype; ?></span>
<h1><?php echo $trip->title; ?></h1>


<span class="quote">
	<?php echo strip_tags($trip->introtext); ?>
</span>

<div class="tripinfo tabbable tabs-left">
	<?php
	if($categories){
		$alias = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($trip->title, ENT_QUOTES, 'UTF-8'));
		$alias = strtolower($alias);
		$alias = str_replace("  "," ",$alias);
		$alias = str_replace(" - ","-",$alias);
		$alias = str_replace(" ","-",$alias);
		?>
		<ul class="nav nav-tabs">
			<?php
				$active = '';
				if(!$activecategory){
					$active = " class='active' ";
				}
			?>
			<li <?php echo $active; ?>><a href="<?php echo JRoute::_('index.php?option=com_trip&view=trip&trip_id='.$trip->trip_id.":".$alias); ?>" title="<?php echo JText::_('TRIP_INFO'); ?>"><?php echo JText::_('TRIP_INFO'); ?></a></li>
		<?php
		foreach($categories as $category){
			$active = "";
			
			if($category->cat_id == $activecategory){
				$doc->setMetaData('description', $category->metadescription);
				$doc->setMetaData('keywords', $category->metakeywords);
				$active = " class='active' ";
				$pagetitletrip = $category->title;
			}

			if(strlen(strip_tags($category->introtext)) > 1){			
			?>	
				<li <?php echo $active; ?>><a href="<?php echo JRoute::_('index.php?option=com_trip&view=trip&trip_id='.$trip->trip_id.":".$alias."&cat_id=".$category->cat_id.":".$category->alias); ?>" title="<?php echo $category->title; ?>"><?php echo $category->title; ?></a></li>			
			<?php
			}
		}
		?>
		</ul>
		<?php
	}
	?>
	<div class="tab-content">
		<?php if($pagetitletrip){?>
			<span class="tripcontenttitle"><?php echo $pagetitletrip ?></span>
		<?php }else{?>
			<span class="tripcontenttitle">Reis informatie</span>
		<?php }?>
		<p>
			<div class="anchorLinksContainer">
				<ul></ul>
			</div>
			<div class="clearfix"></div>
			<?php 
				JPluginHelper::importPlugin('trip');
				$dispatcher =& JDispatcher::getInstance();
				$departures = $dispatcher->trigger( 'loadDepartures', array($trip));
				if($departures){
					echo $departures[0];
				}
				
				if(!$categoryobj){
					if($trip->route){
					
					?>
						<a href="#route" data-toggle="modal"><img class="thumb" src="/images/routes/<?php echo $trip->route; ?>" /></a>						
						<?php
							if($trip->extrainfo){ 
								?>
								<h3>
									<?php 
									echo $trip->extrainfo; 
									?>
								</h3>
								<?php 
							}
						?>					
						<div id="route" class="modal hide fade">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h3><?php echo $trip->title; ?> - Route</h3>
							</div>
							<div class="modal-body">
								<center><img class="big" src="/images/routes/<?php echo $trip->route; ?>" /></center>
							</div>
						</div>
					<?php
					}
					echo $article->text; 
				}
				else{
					echo $categoryobj->introtext;
				}
			?>
		</p> 
	</div>
	<a rel="nofollow" class="btn btn-small pull-right" href="index.php?option=com_trip&view=process&task=articlepdf&trip_id=<?php echo $trip->trip_id; ?>" title="<?php echo $trip->title; ?> - PDF versie" name="<?php echo $trip->title; ?> - PDF versie" ><i class="icon-file"></i> Print pdf bestand</a>
</div>

<div id="contentImageContainer" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3>&nbsp;</h3>
  </div>
  <div class="modal-body">
    <img id="contentImageBig" src="" style="width:100%;"/>
  </div>
</div>