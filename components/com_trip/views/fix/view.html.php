<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die;
jimport('joomla.application.component.view');
class TripViewFix extends JView
{
	function display($tpl = null)
	{
		$task = JRequest::getCmd('task');
		$model =& $this->getModel();
		switch($task){
			case 'mergek2':
				$model->mergek2();
			break;
			case 'getxtripcontent':
				$model->getXtripcontent();
			break;
			case 'fillExtraFieldsSearch':
				$model->fillExtraFieldsSearch();
			break;
			case 'setMetaDataTrips':
				$model->setMetaDataTrips();
			break;
			case 'fixAttachments':
				$model->fixAttachments();
			break;
			case 'fixRouteImages':
				$model->fixRouteImages();
			break;
		}
		
		parent::display($tpl);
	}
}