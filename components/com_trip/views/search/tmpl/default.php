<?php
	defined('_JEXEC') or die('Restricted access');

	require_once(JPATH_COMPONENT.DS.'helpers'.DS.'route.php');
	
	$trips = $this->trips;
	$destination = $this->destination;

?>


<h2><?php echo JText::_('SEARCH_RESULTS'); ?></h2>
<form method="gt" name="searchform" action="<?php echo JRoute::_('index.php?option=com_trip&view=search&Itemid='.JRequest::getInt('Itemid')); ?>">
	<input type="hidden" name="triptype_id" value="<?php echo JRequest::getInt('triptype_id'); ?>"/>
	<input type="hidden" name="destination_id" value="<?php echo JRequest::getInt('destination_id'); ?>"/>
	<input type="hidden" name="search_date_start" value="<?php echo JRequest::getVar('search_date_start'); ?>"/>
	<input type="hidden" name="search_date_end" value="<?php echo JRequest::getVar('search_date_end'); ?>"/>
	<?php
		if($this->pagination->getListFooter()){
			?>	
		
				<?php echo $this->pagination->getListFooter(); ?>
		
		<?php
		}
	?>
</form>
<?php
	if($trips){

		foreach($trips as $trip){
			$alias = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($trip->triptitle, ENT_QUOTES, 'UTF-8'));
			$alias = strtolower($alias);
			$alias = str_replace("  "," ",$alias);
			$alias = str_replace(" - ","-",$alias);
			$alias = str_replace(" ","-",$alias);
			?>
				<div class="trip <?php echo $trip->tripcolor; ?>" onclick='window.location.href="<?php echo JRoute::_('index.php?option=com_trip&view=trip&trip_id='.$trip->trip_id.":".$alias.'&Itemid='.TripHelperRoute::getItemIdByTriptype($trip->triptype_id)); ?>"'>
					<span>
						<img src="/images/reisthumbnails/<?php echo $trip->imagethumb; ?>" alt="<?php echo $trip->triptitle; ?>" name="<?php echo $trip->triptitle; ?>"/>
						<h3><?php echo $trip->triptitle; ?></h3>
						<p>
							<?php echo strip_tags($trip->introtext); ?>
						</p>
						<div>
							<a href="<?php echo JRoute::_('index.php?option=com_trip&view=trip&trip_id='.$trip->trip_id.":".$alias.'&Itemid='.TripHelperRoute::getItemIdByTriptype($trip->triptype_id)); ?>" title="<?php echo $trip->title; ?>" name="<?php echo $trip->title; ?>" class="btn btn-small pull-right"><i class="icon-eye-open"></i> Bekijk</a>

							<span class="startingprice">v.a. &euro;<?php echo number_format($trip->tripreissom,2 ,',','.'); ?></span><br/>
							<span class="departuredate">
								<?php 
									if($trip->willekeurige_vertrek_datum != 1){
										if(!empty($trip->eerstvolgende)){
											$eerstvolgende = explode(',', $trip->eerstvolgende);
											$dates = '';
											foreach($eerstvolgende as $date){
												$timestamp = strtotime($date);
												$dates .= strftime('%A %d %B', $timestamp).", ";
											}
											if(strlen($dates) > 5){
												$dates = substr($dates, 0, -2);
												echo "Eerst volgende vertrekdata: ".$dates;
											}
										}
									}
									else{
										echo "Eerst volgende vertrekdata: willekeurig";
									}
								?>
							</span>
						</div>
					</span>
				</div>
			<?php
		}
	}else{
	?>
	<div><?php echo JText::_('NO_TRIPS_FOUND'); ?></div>
	<?php
	}
?>
