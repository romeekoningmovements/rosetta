<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die;
jimport('joomla.application.component.view');
class TripViewTrips extends JViewLegacy
{
	protected $state;
	protected $items;
	protected $category;
	protected $categories;
	protected $pagination;
	protected $total;

	function display($tpl = null)
	{
		$app		= JFactory::getApplication();
		$user		= JFactory::getUser();
		$model 		= $this->getModel();

		$this->state		= $this->get('State');
		$this->trips		= $this->get('Data');
		$this->departures   = $model->getDepartures($this->trips);
		$this->pagination	= $this->get('Pagination');
		$this->category		= $this->get('Category');
		
		parent::display($tpl);
	}
}