<?php
	defined('_JEXEC') or die('Restricted access');
	require_once(JPATH_COMPONENT.DS.'helpers'.DS.'route.php');
	
	$trips = $this->trips;



	$category = $this->category;
	
	$doc =& JFactory::getDocument();

	if($category->metadescription){
		$doc->setMetaData('description', $category->metadescription);
	}
	if($category->metakeywords){
		$doc->setMetaData('keywords', $category->metakeywords);
	}
	
	$departures = $this->departures;

?>

<h1><?php echo $category->triptype; ?></h1>

<?php
	if($category->quote){
		?>
			<span class="quote"><?php echo $category->quote; ?></span>
		<?php
	}
	?>
		<form method="get" name="searchform" action="<?php echo JRoute::_('index.php?option=com_trip&view=trips&Itemid='.JRequest::getInt('Itemid')); ?>">
			<?php
				if($this->pagination->getListFooter()){
					?>	
				
						<?php echo $this->pagination->getListFooter(); ?>
				
				<?php
				}
			?>
		</form>
	<?php
	if($trips){
		
		foreach($trips as $trip){
			$alias = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($trip->title, ENT_QUOTES, 'UTF-8'));
			$alias = strtolower($alias);
			$alias = str_replace("  "," ",$alias);
			$alias = str_replace(" - ","-",$alias);
			$alias = str_replace(" ","-",$alias);
			?>
				<div class="trip" onclick='window.location.href="<?php echo JRoute::_('index.php?option=com_trip&view=trip&trip_id='.$trip->trip_id.":".$alias.'&Itemid='.TripHelperRoute::getItemIdByTriptype($trip->triptype_id)); ?>"'>
					<span>
						<img src="/images/reisthumbnails/<?php echo $trip->imagethumb; ?>" alt="<?php echo $trip->triptitle; ?>" name="<?php echo $trip->triptitle; ?>"/>
						<h3><?php echo $trip->triptitle; ?></h3>
						<p>
							<?php echo strip_tags($trip->introtext); ?>
						</p>
						<div>
							<a href="<?php echo JRoute::_('index.php?option=com_trip&view=trip&trip_id='.$trip->trip_id.":".$alias.'&Itemid='.TripHelperRoute::getItemIdByTriptype($trip->triptype_id)); ?>" title="<?php echo $trip->title; ?>" name="<?php echo $trip->title; ?>" class="btn btn-small pull-right"><i class="icon-eye-open"></i> Bekijk</a>

							<span class="startingprice">v.a. &euro;<?php echo number_format($trip->reissom, 2, ',','.'); ?></span><br/>
							<span class="departuredate">
								<?php					
									if(isset($departures[$trip->trip_id])){
										echo "<strong>Eerst volgende vertrekdata: </strong>";
										foreach($departures[$trip->trip_id] as $key=>$departure){
											echo strftime("%e %B",strtotime($departure->vertrek));
											if($key != count($departures[$trip->trip_id])-1){
												echo ", ";
											}
										}
									}
								?>
							</span>
						</div>
					</span>
				</div>
			<?php
		}
	}else{
	?>
	<div><?php echo JText::_('NO_TRIPS_FOUND'); ?></div>
	<?php
	}
?>