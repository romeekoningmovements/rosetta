<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die;
jimport('joomla.application.component.view');

class TripViewBook extends JViewLegacy
{
	protected $state;
	protected $items;
	protected $category;
	protected $categories;
	protected $pagination;
	protected $total;

	function display($tpl = null)
	{
		$app		 = JFactory::getApplication();
		$user		 = JFactory::getUser();
		$model 		 = $this->getModel();
		$triptype_id = JRequest::getVar('triptype_id');

		$this->state		= $this->get('State');
		$this->extraoptions	= $model->getExtraOptions(JRequest::getInt('trip_id'));
		$this->pagination	= $this->get('Pagination');
		
		if($triptype_id){
			if(!is_numeric($triptype_id)){
				$triptype_id = $model->getTriptypeId(JRequest::getVar('triptype_id'));
				JRequest::setVar('triptype_id', $triptype_id);
			}
		}
		
		parent::display($tpl);
	}
}