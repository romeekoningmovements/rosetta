<?php
	defined('_JEXEC') or die('Restricted access');
	require_once(JPATH_COMPONENT.DS.'helpers'.DS.'form.php');
	
	JHTML::_('behavior.framework');
	$doc =& JFactory::getDocument();
	$doc->addScript(JURI::base().'components'.DS.'com_trip'.DS.'assets'.DS.'js'.DS.'script.js');


?>
<div style="position:relative;" id="bookwrapper_comp">
<table class="table table-bordered table-hover table-condesed" id="bookwrapper">
	<tbody>
		<tr>
			<td>Reistype</td>
			<td>
				<?php 
					echo TripHelperForm::getTripTypes();
				?>
			</td>
		</tr>
		<tr>
			<td>Reis</td>
			<td>
				<?php 
					echo TripHelperForm::getTrips(JRequest::getInt('triptype_id'));
				?>
			</td>
		</tr>
		<tr>
			<td>Vertrek</td>
			<td>
				<?php 
					echo TripHelperForm::getDepartures(JRequest::getInt('triptype_id'), JRequest::getInt('trip_id'));
				?>
			</td>
		</tr>
		<tr>
			<td>Aantal personen</td>
			<td>
				<?php 
					if(JRequest::getVar('departure_id') != 'willekeurig' && JRequest::getVar('departure_id') != 'dd-mm-jjjj'){
						echo TripHelperForm::getPersons(JRequest::getInt('triptype_id'), JRequest::getInt('trip_id'), JRequest::getVar('departure_id'), JRequest::getInt('person'));
					}
				?>
			</td>
		</tr>
	</tbody>
</table>
</div>
<!-- placeholder form -->
<div id="bookingformwrapper"></div>





    
    
    
    
    
    
    
    
    
    
    