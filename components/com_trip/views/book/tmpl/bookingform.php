<?php 
	defined('_JEXEC') or die('Restricted access');
	// @loek nog stijl hieronder weghalen en in less zetten.
?>

<style>
	#bookingsform input:hover,
	#bookingsform textarea:hover,
	#bookingsform input:focus,
	#bookingsform textarea:focus {
	  background-color: #ddffdd;
	}
	
	#bookingsform .validation-failed {
	  border-color: #ff0000;
	  background-color: #ffdddd;
	}
	
	#bookingsform .validation-advice {
	  color: #ff0000;
	}
	
	#bookingsform .spinner {
	  border-radius: 3px;
	  -webkit-border-radius: 3px;
	  -moz-border-radius: 3px;
	  background-color: #f0f0f0;
	}
	
	#bookingsform .overTxtLabel {
	  color: #888888;
	}
</style>
<div id="bookingformcontent">
	
	<div class="tripoptions alert alert-success" id="tripoptions">
		<?php
			$checked = array();
			if(JRequest::getVar('defaultoptions')){
				foreach(JRequest::getVar('defaultoptions') as $key=>$o){
					$checked[$key] = $o;
				}
			}
		?>
		<div id="defaultoptions">
			<div class="control-group">
			  <div class="controls">
			    <label class="checkbox inline">
				  <input type="checkbox" id="defaultoption0" <?php if(isset($checked[0])){echo " checked='checked' ";} ?> onclick="getCalculationBox(<?php echo JRequest::getInt('triptype_id'); ?>, <?php echo JRequest::getInt('trip_id'); ?>, '<?php echo JRequest::getVar('departure_id'); ?>', <?php echo JRequest::getInt('person'); ?>, getOptionsChecked(), getExtraOptionsChecked());" data-target="0" name="defaultoption[0]"/> Annuleringsverzekering (5.5% van de totale reissom) 
				</label>
			  </div>
			</div>
			<div class="control-group">
			  <div class="controls">
			    <label class="checkbox inline">
				  <input type="checkbox" id="defaultoption1" <?php if(isset($checked[1])){echo " checked='checked' ";} ?> onclick="getCalculationBox(<?php echo JRequest::getInt('triptype_id'); ?>, <?php echo JRequest::getInt('trip_id'); ?>, '<?php echo JRequest::getVar('departure_id'); ?>', <?php echo JRequest::getInt('person'); ?>, getOptionsChecked(), getExtraOptionsChecked());" data-target="1" name="defaultoption[1]"/> Gescheiden facturering 
				</label>
			  </div>
			</div>
		</div>
		<?php
			if(!empty($this->extraoptions)){
				?>
				<form id="testform">
				<div id="extraoptions">
					<?php
						foreach($this->extraoptions as $option){
							$checked = '';
							if(JRequest::getVar('extraoptions')){
								foreach(JRequest::getVar('extraoptions') as $key=>$value){
									if($key == $option->id){
										if($value == 'true'){
											$checked = " checked='checked' ";
										}
									}
								}
							}
							?>
								<div class="control-group">
								  <div class="controls">
								    <label class="checkbox inline">
									  <input type="checkbox" id="extraoption<?php echo $option->id; ?>" <?php echo $checked; ?> onclick="getCalculationBox(<?php echo JRequest::getInt('triptype_id'); ?>, <?php echo JRequest::getInt('trip_id'); ?>, '<?php echo JRequest::getVar('departure_id'); ?>', <?php echo JRequest::getInt('person'); ?>, getOptionsChecked(),getExtraOptionsChecked());" value="<?php echo $option->id; ?>" data-target="<?php echo $option->id; ?>" name="extraoptions[<?php echo $option->id; ?>]"> 
									  <?php 
									  	 echo $option->description; 
									  	 if($option->cpp){
										  	 echo "( &euro; ".$option->cpp." per persoon )";
									  	 }
									  	 elseif($option->cpt){
										  	  echo "( &euro; ".$option->cpt." per reis )";
									  	 }
									  ?>
									</label>
								  </div>
								</div>
							<?php
						}
					?>
				</div>
				</form>
				<?php
			}
		?>
	</div>
	<div class="tabbable tabs-left"> <!-- Only required for left/right tabs -->
	    <ul class="nav nav-tabs">
	    	<?php
	    		for($i = 1; $i < JRequest::getInt('person') +1; $i++){
		    		?>
		    			<li <?php if($i == 1){ ?>class="active"<?php }?>><a href="#person<?php echo $i; ?>" data-toggle="tab">Persoon <?php echo $i; ?></a></li>
		    		<?php
	    		}
	    	?>
	    </ul>
	    <form class="form-horizontal" id="bookingsform" action="<?php echo JRoute::_('index.php?option=com_trip&view=process&task=savebooking&Itemid=151'); ?>" method="post">
	    <input type="hidden" name="trip_id" value="<?php echo JRequest::getInt('trip_id'); ?>"/>
	    <input type="hidden" name="triptype_id" value="<?php echo JRequest::getInt('triptype_id'); ?>"/>
	    <input type="hidden" name="departure_id" value="<?php echo  JRequest::getVar('departure_id'); ?>"/>
	    <input type="hidden" name="person" value="<?php echo  JRequest::getInt('person'); ?>"/>

	    <div class="tab-content">
	    	
	    	<?php
	    		for($i = 1; $i < JRequest::getInt('person') +1; $i++){
		    		?>
		    			 <div class="tab-pane <?php if($i == 1){ ?>active<?php }?>" id="person<?php echo $i; ?>">
						 	
								<legend>Persoon <?php echo $i; ?> <?php if($i == 1){ ?>(Hoofdaanvrager) <?php }?></legend>
								
								<?php if($i != 1){ ?>
									<div class="control-group">
									  <div class="controls">
									    <label class="checkbox inline">
										  <input type="checkbox" id="inlineCheckbox<?php echo $i; ?>" onclick="fillinForm(<?php echo $i; ?>);" value="option1"> Adresgegevens hetzelfde als hoofdaanvrager?
										</label>
									  </div>
									</div>
								<?php }?>
								<div class="control-group">
								  <label class="control-label">Voorletters *</label>
								  <div class="controls">
								    <input type="text" id="initials<?php echo $i; ?>" title="Vul hier uw initialen in" name="initials<?php echo $i; ?>" data-validators="required" class="span4">
								  </div>
								</div>
								<div class="control-group">
								  <label class="control-label">Roepnaam *</label>
								  <div class="controls">
								    <input type="text" id="nickname<?php echo $i; ?>" title="Vul hier uw roepnaam in" name="nickname<?php echo $i; ?>" data-validators="required" class="span4">
								  </div>
								</div>
								<div class="control-group">
								  <label class="control-label">Offici&euml;le 1e voornaam *</label>
								  <div class="controls">
								    <input type="text" id="officialname<?php echo $i; ?>" title="Vul hier uw officiele 1e naam in" name="officialname<?php echo $i; ?>" data-validators="required" class="span4">
								  </div>
								</div>
								<div class="control-group">
								  <label class="control-label">Achternaam *</label>
								  <div class="controls">
								    <input type="text" id="lastname<?php echo $i; ?>" title="Vul hier uw achternaam in" name="lastname<?php echo $i; ?>" data-validators="required" class="span4">
								  </div>
								</div>
								<div class="control-group">
								  <label class="control-label">Geboortedatum *</label>
								  <div class="controls">
								    <input type="text" id="birthdate<?php echo $i; ?>" title="Vul hier uw geboortedatum in" name="birthdate<?php echo $i; ?>" data-validators="required" class="span4">
								  </div>
								</div>
								<div class="control-group">
								  <label class="control-label">Geslacht *</label>
								  <div class="controls">
								    <label class="radio inline">
									  <input type="radio" name="gender<?php echo $i; ?>" id="optionsRadios1" value="m" checked>
									  Man
									</label>
									<label class="radio inline">
									  <input type="radio" name="gender<?php echo $i; ?>" id="optionsRadios2" value="v">
									  Vrouw
									</label>
								  </div>
								</div>
								<div class="control-group">
								  <label class="control-label">Paspoortnummer</label>
								  <div class="controls">
								    <input type="text" name="passport<?php echo $i; ?>" class="span4">
								  </div>
								</div>
								
								<hr/>
								
								<div class="control-group">
								  <label class="control-label">Straatnaam *</label>
								  <div class="controls">
								    <input type="text" id="street<?php echo $i; ?>" title="Vul hier uw straatnaam in" name="street<?php echo $i; ?>" data-validators="required" class="span4">
								  </div>
								</div>
								<div class="control-group">
								  <label class="control-label">Huisnummer *</label>
								  <div class="controls">
								    <input type="text" id="housenr<?php echo $i; ?>" title="Vul hier uw huisnummer in" name="housenr<?php echo $i; ?>" data-validators="required" class="span4">
								  </div>
								</div>
								<div class="control-group">
								  <label class="control-label">Toevoeging</label>
								  <div class="controls">
								    <input type="text" id="extra<?php echo $i; ?>" name="extra<?php echo $i; ?>" class="span4">
								  </div>
								</div>
								<div class="control-group">
								  <label class="control-label">Postcode en Plaats *</label>
								  <div class="controls">
								    <input type="text" id="zipandcity<?php echo $i; ?>" title="Vul hier uw postcode en plaats in" name="zipandcity<?php echo $i; ?>" data-validators="required" class="span4">
								  </div>
								</div>
								<div class="control-group">
								  <label class="control-label">Telefoon priv&eacute; *</label>
								  <div class="controls">
								    <input type="text" id="phone<?php echo $i; ?>" title="Vul hier uw telefoonnummer in" name="phone<?php echo $i; ?>" data-validators="required" class="span4">
								  </div>
								</div>
								<div class="control-group">
								  <label class="control-label">Telefoon werk</label>
								  <div class="controls">
								    <input type="text" name="phonework<?php echo $i; ?>" class="span4">
								  </div>
								</div>
								<div class="control-group">
								  <label class="control-label">Email *</label>
								  <div class="controls">
								    <input type="text" id="email<?php echo $i; ?>" title="Vul hier uw e-mailadres in" name="email<?php echo $i; ?>" data-validators="required validate-email" class="span4">
								  </div>
								</div>
								
								<?php if($i == 1){ ?>
																	<?php }?>
								
							 	
					    </div>
		    		<?php
	    		}
	    	?>
	    	<hr/>
			<div class="control-group">
			  <label class="control-label">Opmerkingen</label>
			  <div class="controls">
			    <textarea rows="8" name="remarks" class="span4"></textarea>
			  </div>
			</div>
			<hr/>
			<div class="control-group">
			  <div class="controls">
			    <label class="checkbox inline">
				  <input type="checkbox" id="akkoord" name="akkoord" value="1" title="U dient akkoord te gaan met de voorwaarden" class="validate-required-check" data-validators="validate-required-check"> Ik ga akkoord met de <a href="<?php echo JRoute::_( 'index.php?option=com_content&view=article&id=8&Itemid=153' ); ?>" target="_blank" title="Algemene voorwaarden" name="Algemene voorwaarden">algemene voorwaarden</a>
				</label>
			  </div>
			</div>
			<div class="control-group">
			  <div class="controls">
			    <label class="checkbox inline">
				  <input type="checkbox" id="nieuwsbrief" name="nieuwsbrief" title="Klik hier om in te schrijven op onze nieuwsbrief" checked> Klik hier om in te schrijven op onze nieuwsbrief
				</label>
			  </div>
			</div>
	    	<button type="submit" id="continue" class="btn btn-success btn-large" type="button">Verzenden</button>
	    </div>
	    </div>
    </div>
</div>
