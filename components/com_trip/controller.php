<?php

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

/**
 * ShopController class.
 * 
 * @extends JController
 */
class TripController extends JControllerLegacy
{
	public function display($cachable = false, $urlparams = false)
	{
		$cachable = false;
		$application =& JFactory::getApplication();
		$document =& JFactory::getDocument();

		if($application->getCfg('sitename_pagetitles') == 1){
			$document->setTitle($application->getCfg('sitename').' - '.$document->title); 
		}elseif(2){
			$document->setTitle($document->title.' - '.$application->getCfg('sitename'));
		}

		$view 		  = JRequest::getVar('view');
		$trip_id 	  = JRequest::getInt('trip_id');
		$triptype_id  = JRequest::getInt('triptype_id');
		$departure_id = JRequest::getVar('departure_id');

		$safeurlparams = array('id'=>'INT', 'limit'=>'INT', 'limitstart'=>'INT', 'filter_order'=>'CMD', 'filter_order_Dir'=>'CMD', 'lang'=>'CMD');
		if(JRequest::getCmd('task') == 'articlepdf'){
			$model =& $this->getModel('process');
			$model->generateTripOverview(JRequest::getInt('trip_id'));

			exit;
		}
		
		if(JRequest::getCmd('task') == 'sitemap'){
			
			$model =& $this->getModel('process');
			$model->createSitemapTrips();
			$model->createSitemapContent();
		}
		if(JRequest::getCmd('task') == 'savebooking'){
			$model =& $this->getModel('process');
			$model->subscribe_newsletter();
			$model->createPdf();
		}
		parent::display($cachable, $safeurlparams);
		
		
	}
}
