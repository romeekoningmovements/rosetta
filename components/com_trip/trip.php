<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_weblinks
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

setlocale(LC_ALL, 'nl_NL');
$controller	= JControllerLegacy::getInstance('Trip');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
