window.addEvent('domready', function(){
	
});

function getTrips(triptype){
	//alert('kaas');
	var myHTMLRequest = new Request.HTML({
		url: '/index.php?option=com_trip&view=book&Itemid=151',
		useSpinner: true,
		spinnerTarget: 'bookwrapper',
		spinnerOptions:{
			message: 'Uw reis wordt opgehaald.',
		},
		onComplete: function(responseTree, responseElements, responseHTML, responseJavaScript){
			if(document.id('bookwrapper_comp')){
				document.id('bookwrapper_comp').innerHTML = responseElements.filter('#bookwrapper_comp')[0].innerHTML;
			}
			if(document.id('calculation_wrapper')){
				document.id('calculation_wrapper').innerHTML = 'Maak aub een selectie';
			}
			if(document.id('bookingformwrapper')){
				document.id('bookingformwrapper').innerHTML = '';
			}
		}	
	}).send('triptype_id='+triptype);
}

function getDepartures(triptype, trip){

	var myHTMLRequest = new Request.HTML({
		url: '/index.php?option=com_trip&view=book&Itemid=151',
		useSpinner: true,
		spinnerTarget: 'bookwrapper',
		spinnerOptions:{
			message: 'Uw reis wordt opgehaald.',
		},
		onComplete: function(responseTree, responseElements, responseHTML, responseJavaScript){
			//console.log(responseHTML);
			if(document.id('bookwrapper_comp')){
				document.id('bookwrapper_comp').innerHTML = responseElements.filter('#bookwrapper_comp')[0].innerHTML;
			}
			if(document.id('calculation_wrapper')){
				document.id('calculation_wrapper').innerHTML = 'Maak aub een selectie';
			}
			if(document.id('bookingformwrapper')){
				document.id('bookingformwrapper').innerHTML = '';
			}
		}	
	}).send('triptype_id='+triptype+'&trip_id='+trip);
}

function getPersons(triptype, trip, departure){
	if(departure.indexOf('-') != -1 || document.id('willekeurig')){
		if(departure.length != 10){
			alert('Foutieve datum, de datum mag niet voor de huidige datum liggen');
			return;
		}
		var selecteddate = departure.split('-');
		if(selecteddate.length != 3){
			if(document.id('person')){
				document.id('person').dispose();
			}
			alert('Foutieve datum, de datum mag niet voor de huidige datum liggen');
			return;
		}
		else{
			var today = new Date();
			var newdate = new Date(selecteddate[2], selecteddate[1]-1, selecteddate[0]);
			
			if(newdate < today){
				alert('Foutieve datum, de datum mag niet voor de huidige datum liggen');
				return;
			}
		}
	}
	
	var myHTMLRequest = new Request.HTML({
		url: '/index.php?option=com_trip&view=book&Itemid=151',
		useSpinner: true,
		spinnerTarget: 'bookwrapper',
		spinnerOptions:{
			message: 'Uw reis wordt opgehaald.',
		},
		onComplete: function(responseTree, responseElements, responseHTML, responseJavaScript){
			if(document.id('bookwrapper_comp')){
				document.id('bookwrapper_comp').innerHTML = responseElements.filter('#bookwrapper_comp')[0].innerHTML;
			}
			if(document.id('calculation_wrapper')){
				document.id('calculation_wrapper').innerHTML = 'Maak aub een selectie';
			}
			if(document.id('bookingformwrapper')){
				document.id('bookingformwrapper').innerHTML = '';
			}
		}	
	}).send('triptype_id='+triptype+'&trip_id='+trip+'&departure_id='+departure);
}

function getBookingsForm(triptype, trip, departure, person){
	if(person > 0){
		// module calculatie herladen
		getCalculationBox(triptype, trip, departure, person, null, null);
		var myHTMLRequest = new Request.HTML({
			url: '/index.php?option=com_trip&view=book&layout=bookingform&Itemid=151',
			onComplete: function(responseTree, responseElements, responseHTML, responseJavaScript){
				if(document.id('bookingformwrapper')){
					document.id('bookingformwrapper').innerHTML = responseElements.filter('#bookingformcontent')[0].innerHTML;
				}
				if(document.id('bookingsform')){
					 myFormValidator = new Form.Validator.Inline(document.id('bookingsform'), {
				      stopOnFailure: true,
				      useTitles: true,
				      errorPrefix: "",
				      ignoreHidden:false,
				           
				      onFormValidate: function(passed, form, event) {
				         if (passed) {
				            	form.submit();
				         }
				      },
				       onElementValidate: function(passed, element, validator, is_warn) {
				         if (element.get('name') == 'akkoord' && !passed) {
				            //alert('U dient akkoord te gaan met de algemene voorwaarden om uw boeking definitief te maken.');
				         }
				      }
				    });
					 
					if (document.id('continue')) {
					    document.id('continue').addEvents({   	
					       'click': function() { myFormValidator.validate(); }
					    });
					}
				}	
			}	
		}).send('triptype_id='+triptype+'&trip_id='+trip+'&departure_id='+departure+'&person='+person);
	}
	
}

function getCalculationBox(triptype, trip, departure, person, defaultoptions, extraoptions){
	var myHTMLRequest = new Request.HTML({
		url: '/index.php?option=com_trip&view=book&layout=bookingform&Itemid=151',
		useSpinner: true,
		spinnerTarget: 'spinnertrigger',
		spinnerOptions:{
			message: 'Uw reis wordt berekend.',
		},
		onComplete: function(responseTree, responseElements, responseHTML, responseJavaScript){
			if(document.id('calculationbox')){
				document.id('calculationbox').innerHTML = responseElements.filter('#calculationbox')[0].innerHTML;
			}
			if(document.id('tripoptions')){
				document.id('tripoptions').innerHTML = responseElements.filter('#tripoptions')[0].innerHTML;
			}
		}	
	}).send('triptype_id='+triptype+'&trip_id='+trip+'&departure_id='+departure+'&person='+person+'&'+defaultoptions+'&'+extraoptions);
}

function getExtraOptionsChecked(){
	var options = '';
	if(document.id('extraoptions')){
		var optionsselected = document.id('extraoptions').getElements("input[type='checkbox']");
		
		
		if(optionsselected){
			optionsselected.each(function(e){
				if(e.checked){
					options += 'extraoptions['+e.getAttribute('data-target')+']=true&';
				}
			});
			
			options = options.substring(0, options.length-1);
		}
	}
	return options;
}

function getOptionsChecked(){
	var optionsselected = document.id('defaultoptions').getElements("input[type='checkbox']");
	var extraoptions = '';
	
	if(optionsselected){
		optionsselected.each(function(e){
			if(e.checked){
				extraoptions += 'defaultoptions['+e.getAttribute('data-target')+']=true&';
			}
		});
		
		extraoptions = extraoptions.substring(0, extraoptions.length-1);
		
	}
	
	return extraoptions;
}

function fillinForm(id){
	if(document.id('street'+id)){
		document.id('street'+id).value = document.id('street1').value;
	}
	if(document.id('housenr'+id)){
		document.id('housenr'+id).value = document.id('housenr1').value;
	}
	if(document.id('extra'+id)){
		document.id('extra'+id).value = document.id('extra1').value;
	}
	if(document.id('zipandcity'+id)){
		document.id('zipandcity'+id).value = document.id('zipandcity1').value;
	}
}