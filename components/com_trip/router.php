<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_weblinks
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

 /* Weblinks Component Route Helper
 *
 * @package		Joomla.Site
 * @subpackage	com_weblinks
 * @since 1.6
 */

defined('_JEXEC') or die;

jimport('joomla.application.categories');

/**
 * Build the route for the com_weblinks component
 *
 * @param	array	An array of URL arguments
 *
 * @return	array	The URL arguments to use to assemble the subsequent URL.
 */
function TripBuildRoute(&$query)
{
	$segments = array();
	
	if(isset($query['view'])){
		if($query['view'] == 'trip'){
			$segments[] = 'reis';
		}
		else{
			$segments[] = $query['view'];
		}	
		unset($query['view']);
	}
	
	if(isset($query['triptype_id'])){
		$segments[] = $query['triptype_id'];
		unset($query['triptype_id']);
	}
	
	if(isset($query['trip_id'])){
		$segments[] = $query['trip_id'];
		unset($query['trip_id']);
	}
	
	if(isset($query['cat_id'])){
		$segments[] = $query['cat_id'];
		unset($query['cat_id']);
	}
	
	if(isset($query['departure_id'])){
		$segments[] = $query['departure_id'];
		unset($query['departure_id']);
	}
	
	if(isset($query['destination_id'])){
		$segments[] = $query['destination_id'];
		unset($query['destination_id']);
	}
	
	if(isset($query['search_date_start'])){
		$segments[] = $query['search_date_start'];
		unset($query['search_date_start']);
	}
	
	if(isset($query['search_date_end'])){
		$segments[] = $query['search_date_end'];
		unset($query['search_date_end']);
	}
	
	return $segments;
}
/**
 * Parse the segments of a URL.
 *
 * @param	array	The segments of the URL to parse.
 *
 * @return	array	The URL attributes to be used by the application.
 */
function TripParseRoute($segments)
{
	$vars = array();

	switch($segments[0]){
		case 'reis':
			$vars['view'] = 'trip';
			if(count($segments) == 2){
				$vars['trip_id'] = $segments[1];
			}
			
			if(count($segments) == 3){
				$vars['trip_id'] = $segments[1];
				$vars['cat_id'] = $segments[2];
			}
		break;
		case 'book':
			$vars['view'] = 'book';
			if(count($segments) == 2){
				$vars['trip_id'] = $segments[1];
			}
			if(count($segments) == 2){
				$vars['triptype_id'] = $segments[1];
			}
			if(count($segments) == 3){
				$vars['triptype_id'] = $segments[1];
				$vars['trip_id'] = $segments[2];
			}
			if(count($segments) == 4){
				$vars['triptype_id'] = $segments[1];
				$vars['trip_id'] = $segments[2];
				$vars['departure_id'] = $segments[3];
			}
		break;
		case 'search':
			$vars['view'] = 'search';
			if(count($segments) == 2){
				$vars['triptype_id'] = $segments[1];
			}
			if(count($segments) == 3){
				$vars['triptype_id'] = $segments[1];
				$vars['destination_id'] = $segments[2];
			}
			if(count($segments) == 5){
				$vars['triptype_id'] = $segments[1];
				$vars['destination_id'] = $segments[2];
				$vars['search_date_start'] = $segments[3];
				$vars['search_date_end'] = $segments[4];
			}
		break;
		case 'trips':
			$vars['view'] = 'trips';
		break;
		case 'destinations':
			$vars['view'] = 'destinations';
		break;
		case 'process':
			$vars['view'] = 'process';
			if(isset($segments[1])){
				$vars['trip_id'] = $segments[1];
			}
		break;
		default:
			 JError::raiseError(404, JText::_("Page Not Found"));		
		break;
		//$vars['view'] = 'trip';
	}
	
	/*
	// gerelateerde reizen of detailpagina reis
	if(count($segments) == 1){
		if(isset($segments[1])){
			$vars['trip_id'] = $segments[1];
		}
	}
	
	// trip detail pagina met categorie
	if(count($segments) == 2){
		if(isset($segments[1])){
			$vars['trip_id'] = $segments[1];
		}
		
		if(isset($segments[2])){
			$vars['cat_id'] = $segments[2];
		}
	}
	
	// boekingspagina
	if(count($segments) == 3){
		$vars['triptype_id'] = $segments[1];
		$vars['trip_id'] = $segments[2];
		$vars['departure_id'] = $segments[3];
	}
	*/

	return $vars;
}
