<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

class Route66Plugin extends JPlugin
{
    protected $rules = array();

    public function onRoute66AddRules()
    {
        $language = plgSystemRoute66::getLanguage();
        $prefix = $language->sef ? $language->sef.'/' : '';
        $array = array();

        foreach ($this->rules as $ruleName) {
            $patterns = $this->params->get($ruleName);
            if (is_object($patterns)) {
                $key = $language->code;
                $pattern = isset($patterns->$key) ? $patterns->$key : null;
                if ($pattern) {
                    include_once JPATH_SITE.'/plugins/route66/'.$this->get('_name').'/rules/'.$ruleName.'.php';
                    $className = 'Route66Rule'.ucfirst($this->get('_name')).ucfirst($ruleName);
                    $rule = new $className($prefix.$pattern);
                    $array[] = $rule;
                }
            }
        }

        return $array;
    }
}
