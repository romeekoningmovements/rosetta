<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

require_once JPATH_SITE.'/plugins/system/route66/lib/rule.php';

class Route66RuleContentArticle extends Route66Rule
{
    private $cache = array();
    protected $variables = array('option' => 'com_content', 'view' => 'article', 'id' => '@', 'catid' => '');

    public function getTokensValues($query)
    {
        // Cache key
        $key = (int) $query['id'];

        // Check cache
        if(isset($this->cache[$key])) {
            return $this->cache[$key];
        }

        // Get database
        $db = JFactory::getDbo();

        // Get query
        $dbQuery = $db->getQuery(true);

        // Initialize values
        $values = array();

        // Iterate over the tokens
        foreach ($this->tokens as $token) {

            // ID
            if ($token == '{articleId}') {
                $values[] = (int) $query['id'];
                $dbQuery->select($db->qn('article.id'));
            }
            // Alias
            elseif ($token == '{articleAlias}') {
                if (strpos($query['id'], ':')) {
                    $parts = explode(':', $query['id']);
                    $values[] = $parts[1];
                }
                $dbQuery->select($db->qn('article.alias'));
            }
            // Category alias
            elseif ($token == '{categoryAlias}') {
                $dbQuery->select($db->qn('category.alias'));
            }
            // Article year
            elseif ($token == '{articleYear}') {
                $dbQuery->select('YEAR('.$db->qn('article.created').')');
            }
            // Article month
            elseif ($token == '{articleMonth}') {
                $dbQuery->select('MONTH('.$db->qn('article.created').')');
            }
            // Article day
            elseif ($token == '{articleDay}') {
                $dbQuery->select('DAY('.$db->qn('article.created').')');
            }
            // Article date
            elseif ($token == '{articleDate}') {
                $dbQuery->select('DATE('.$db->qn('article.created').')');
            }
            // Article author
            elseif ($token == '{articleAuthor}') {
                $dbQuery->select('CASE WHEN '.$db->qn('article.created_by_alias').' = '.$db->q('').' THEN '.$db->qn('article.created_by').' ELSE '.$db->qn('article.created_by_alias').' END ');
            }
        }

        // Check if we already have what we need
        if (count($this->tokens) === count($values)) {
            $this->cache[$key] = $values;
            return $values;
        }

        // If not let's query the database
        $dbQuery->from($db->qn('#__content', 'article'));
        $dbQuery->innerJoin($db->qn('#__categories', 'category').' ON '.$db->qn('article.catid').' = '.$db->qn('category.id'));
        $dbQuery->where($db->qn('article.id').' = '.(int) $query['id']);
        $db->setQuery($dbQuery);
        $values = $db->loadRow();

        // Some values need processing
        $author = array_search('{articleAuthor}', $this->tokens);
        if ($author !== false) {
            if (is_numeric($values[$author])) {
                $values[$author] = JFactory::getUser($values[$author])->name;
            }
            $values[$author] = JFilterOutput::stringURLUnicodeSlug($values[$author]);
        }
        $this->cache[$key] = $values;

        return $values;
    }

    public function getQueryValue($key, $tokens)
    {
        if ($key == 'id') {
            // First check that ID is not already in the URL
            if (isset($tokens['{articleId}'])) {
                return $tokens['{articleId}'];
            }

            // Check for alias
            if (isset($tokens['{articleAlias}'])) {
                return $this->getArticleIdFromAlias($tokens['{articleAlias}']);
            }
        } else {
            return;
        }
    }

    public function getItemid($variables)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select($db->qn('id'))->select($db->qn('catid'))->select($db->qn('language'))->from($db->qn('#__content'))->where($db->qn('id').' = '.$db->q($variables['id']));
        $db->setQuery($query);
        $article = $db->loadObject();

        include_once JPATH_SITE.'/components/com_content/helpers/route.php';
        $route = ContentHelperRoute::getArticleRoute($article->id, $article->catid, $article->language);
        parse_str($route, $result);
        $Itemid = isset($result['Itemid']) ? $result['Itemid'] : '';

        return $Itemid;
    }

    private function getArticleIdFromAlias($alias)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('id')->from('#__content')->where('alias = '.$db->q($alias));
        $db->setQuery($query);
        $id = $db->loadResult();

        return $id;
    }
}
