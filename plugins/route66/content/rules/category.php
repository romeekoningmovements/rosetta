<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

require_once JPATH_SITE.'/plugins/system/route66/lib/rule.php';

class Route66RuleContentCategory extends Route66Rule
{
    private $cache = array();
    protected $variables = array('option' => 'com_content', 'view' => 'category', 'id' => '@');

    public function getTokensValues($query)
    {
        // Cache key
        $key = (int) $query['id'];

        // Check cache
        if(isset($this->cache[$key])) {
            return $this->cache[$key];
        }

        // Get database
        $db = JFactory::getDbo();

        // Get query
        $dbQuery = $db->getQuery(true);

        // Initialize values
        $values = array();

        // Iterate over the tokens
        foreach ($this->tokens as $token) {

            // ID
            if ($token == '{categoryId}') {
                $values[] = (int) $query['id'];
                $dbQuery->select($db->qn('id'));
            }
            // Alias
            elseif ($token == '{categoryAlias}') {
                if (strpos($query['id'], ':')) {
                    $parts = explode(':', $query['id']);
                    $values[] = $parts[1];
                }
                $dbQuery->select($db->qn('alias'));
            }
        }

        // Check if we already have what we need
        if (count($this->tokens) === count($values)) {
            $this->cache[$key] = $values;
            return $values;
        }

        // If not let's query the database
        $dbQuery->from($db->qn('#__categories'));
        $dbQuery->where($db->qn('id').' = '.(int) $query['id']);
        $db->setQuery($dbQuery);
        $values = $db->loadRow();
        $this->cache[$key] = $values;

        return $values;
    }

    public function getQueryValue($key, $tokens)
    {
        if ($key == 'id') {
            // First check that ID is not already in the URL
            if (isset($tokens['{categoryId}'])) {
                return $tokens['{categoryId}'];
            }

            // Check for alias
            if (isset($tokens['{categoryAlias}'])) {
                return $this->getCategoryIdFromAlias($tokens['{categoryAlias}']);
            }
        } else {
            return;
        }
    }

    public function getItemid($variables)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select($db->qn('id'))->select($db->qn('language'))->from($db->qn('#__categories'))->where($db->qn('id').' = '.$db->q($variables['id']));
        $db->setQuery($query);
        $category = $db->loadObject();

        include_once JPATH_SITE.'/components/com_content/helpers/route.php';
        $route = ContentHelperRoute::getCategoryRoute($category->id, $category->language);
        parse_str($route, $result);
        $Itemid = isset($result['Itemid']) ? $result['Itemid'] : '';
        return $Itemid;
    }

    private function getCategoryIdFromAlias($alias)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('id')->from('#__categories')->where('alias = '.$db->q($alias));
        $db->setQuery($query);
        $id = $db->loadResult();

        return $id;
    }
}
