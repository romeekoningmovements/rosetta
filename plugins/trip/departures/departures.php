<?php
/**
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.html.pagination');
jimport('joomla.html.html.sliders');
jimport('joomla.html.html.tabs');
jimport('joomla.utilities.utility');

class plgTripDepartures extends JPlugin
{
	/**
	 * Constructor
	 *
	 * @access      protected
	 * @param       object  $subject The object to observe
	 * @param       array   $config  An array that holds the plugin configuration
	 * @since       1.5
	 */
	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
	}
	
	public function loadDepartures($trip){
		$db =& JFactory::getDBO();
		if(JRequest::getInt('cat_id') != 5){
			return false;
		}
		//ucfirst(strftime("%A %e %B %Y",strtotime($value->vertrek))
		if($trip->willekeurige_vertrek_datum){
			$html = "
				<table class='table table-bordered table-hover table-condensed'>
					<thead>
						<tr>
							<th>Vertrek</th>
							<th>Terug</th>
							<th>Reissom</th>
							<th>Nog plaats?</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>";
			$html .= "<tr>";
			$html .= "<td colspan='5'>Vertrek op basis van een zelf te bepalen vertrekdatum, klik <a href='".JRoute::_('index.php?option=com_trip&view=book&trip_id='.$trip->trip_id.':'.$alias.'&triptype_id='.$trip->alias.'&departure_id=willekeurig&Itemid=151')."'>hier</a> om de prijs te berekenen.</td>";			
			$html .= "</tr>";
			$html .= "
					</tbody>
				</table>";
			return $html;
		}
		$query  = " SELECT d.*, t.willekeurige_vertrek_datum FROM jos_xtrip_departures d";
		$query .= " INNER JOIN jos_xtrip_trips t ON t.trip_id=d.tripid ";
		$query .= " WHERE d.tripid=".$db->Quote($trip->trip_id)." AND (d.vertrek > ".$db->Quote(date('Y-m-d'))." || t.willekeurige_vertrek_datum = 1) ORDER BY d.vertrek ASC";
		$db->setQuery($query);
		if($departures = $db->loadObjectList()){
			$willekeurig = false;
			$html = "
				<table class='table table-bordered table-hover table-condensed'>
					<thead>
						<tr>
							<th>Vertrek</th>
							<th>Terug</th>
							<th>Reissom</th>
							<th>Nog plaats?</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>";
			foreach($departures as $departure){
				$alias = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($trip->title, ENT_QUOTES, 'UTF-8'));
				$alias = strtolower($alias);
				$alias = str_replace("  "," ",$alias);
				$alias = str_replace(" ","-",$alias);
				if($departure->willekeurige_vertrek_datum != 1){
					$gaatdoor = "Ja";
					if($departure->gaatdoor == 1){
						$gaatdoor = "Nee";
					}
					

					$html .= "		
							<tr>
								<td>".ucfirst(strftime("%a %e-%m-%Y",strtotime($departure->vertrek)))."</td>
								<td>".ucfirst(strftime("%a %e-%m-%Y",strtotime($departure->terugkomst)))."</td>
								<td>&euro;".number_format($departure->reissom, 2, ',','.')."</td>
								<td>".$gaatdoor."</td>";
								if($departure->beschikbaar && $departure->vertrek > date('Y-m-d')){
									if($departure->id){
										$html .= "<td><a class='btn' href='".JRoute::_('index.php?option=com_trip&view=book&trip_id='.$trip->trip_id.':'.$alias.'&triptype_id='.$trip->alias.'&departure_id='.$departure->id.'&Itemid=151')."'>Boeken</a></td>";
									}
									//else{
									//	$html .= "<td><a href='".JRoute::_('index.php?option=com_trip&view=book&trip_id='.$trip->trip_id.':'.$alias.'&Itemid=151')."'>Boeken</a></td>";
									//}
								}
								else{
									$html .= "<td>Helaas niet (meer) boekbaar</td>";
								}
					$html .= "</tr>";
				}
				else{				
					$willekeurig = true;
				}
			}
			
			if($willekeurig){
				$html .= "<tr>";
				$html .= "<td colspan='5'>Vertrek op basis van een zelf te bepalen vertrekdatum, klik <a href='".JRoute::_('index.php?option=com_trip&view=book&trip_id='.$trip->trip_id.':'.$alias.'&triptype_id='.$trip->alias.'&departure_id=willekeurig&Itemid=151')."'>hier</a> om de prijs te berekenen.</td>";			
				$html .= "</tr>";
			}
			$html .= "
					</tbody>
				</table>";
			return $html; 
		}
		else{
			$html = "
			<table class='table table-bordered table-hover table-condensed'>				
				<tbody>
					<tr>
						<td><strong>Geen vertrek data beschikbaar voor deze reis, neem aub contact met ons op.</strong></td>
					</tr>
				</tbody>
			</table>";
			return $html;
		}
	}
}
