/* !SPAMBOT PROTECTION -------------------------------------------------- */

$(function() {
 $('a[href^="mailto:"]').each(function() {
  this.href = this.href.replace('(at)', '@').replace(/\(dot\)/g, '.');
  // Remove this line if you don't want to set the email address as link text:
  this.innerHTML = this.href.replace('mailto:', '');
 });
});





// HEADER & SPOTLIGHT ROTATOR
$(document).ready(function() {
    $('.imgcontainer').cycle({
		fx: 'fade',
		speed: 2500,
		delay: ''
	});
	
	$('.spotlighttrips').cycle({
		fx: 'fade',
		speed: 2500, 
		delay: ''
	});
	
	$('#datestart').datepicker().on('changeDate', function(ev){
		$('#datestart').datepicker('hide');
	});
		
	$('#dateend').datepicker().on('changeDate', function(ev){
		$('#dateend').datepicker('hide');
	});
	
	
	$('.tab-content a[name]').not('.tab-content a[href]').each(function(index){
		$(this).addClass('anchorOffset');		
		$('.anchorLinksContainer ul').append('<li><a href="#'+this.name+'">'+this.name+'</a></li>');
	});
	
	$('.tab-content img').not('.tab-content img.thumb').click(function(){
		
		var newsrc = $(this).attr('src');
		$('#contentImageBig').attr('src', newsrc);
		$('#contentImageContainer').modal('show');
	});
	
	
});


// AFFIX FIX
$(document).ready(function(){	
	/*
	var $window = $(window)

	// side bar
	if($window.height() > $('#sidebar').height() && $('#content-container').height() > 750){
		$('#sidebar').affix({
			offset: {
				top: $('#header').height()
				, bottom: 270
			}
		});
	}
	*/
});


// SUB-MENU FIX VOOR IPAD!
$('body').on('touchstart.dropdown', '.dropdown-menu', function (e) { e.stopPropagation(); });



