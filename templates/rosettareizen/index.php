<?php
  //error_reporting(E_ALL);
  //ini_set('display_errors', 1);
  $menu = &JSite::getMenu();
  $catid = 0;
  $home = 0;
  $trip_id = JRequest::getInt('trip_id');
  if($menu->getActive()){
    $getactive = $menu->getActive()->id;
    $active = $menu->getItem($getactive);
    $home = $active->home;
    $catid = $active->params->get('triptype_id');
  }
  
  $db =& JFactory::getDBO();
  $class = '';
  $image = '';
  
  if($trip_id){
    $query = "SELECT c.*, IF((LENGTH(t.image)>2), t.image, tt.image) as tripimage, t.title FROM #__xtrip_colors c INNER JOIN #__xtrip_triptypes tt ON tt.color=c.id INNER JOIN #__xtrip_trips t ON t.triptype_id=tt.id WHERE t.trip_id=".$db->Quote($trip_id);
    $db->setQuery($query);
    $result = $db->loadObject();
    // main image for triptype
    $image = $result->tripimage;
    $title = $result->title;
    // class for the body colortype
    $class = " class=".$result->color;
  }
  elseif($catid){
    $query = "SELECT c.*, t.image FROM #__xtrip_colors c INNER JOIN #__xtrip_triptypes t ON t.color=c.id WHERE t.id=".$db->Quote($catid);
    $db->setQuery($query);
    $result = $db->loadObject();

    // main image for triptype
    $image = $result->image;
    $title = $result->triptype;
    // class for the body colortype
    $class = " class=".$result->color;
  }
  
  
  function curPageURL() {
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
    $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
    $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $pageURL;
  }
  
  
  $app =& JFactory::getApplication();
  $doc =& JFactory::getDocument();
  $doc->setGenerator('Movements Group BV - www.movements.nl');
  
  /*
  if($home){
    $doc->setTitle($app->getCfg('sitename'));
    $doc->setTitle('test');
  }
  */
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" >
<head>
  <jdoc:include type="head" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/bootstrap.css" type="text/css"/>
  <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/bootstrap-responsive.css" type="text/css"/>
  <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template.css" type="text/css"/>
  <link href='https://fonts.googleapis.com/css?family=Dosis:500' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    
  <meta property="og:title" content="<?php echo $this->title;?>"/>

    <meta property="og:url" content="<?php echo JURI::current(); ?>"/>
    <meta property="og:image" content="<?php echo JURI::base(); ?><?php echo $image; ?>"/>
    <meta property="og:site_name" content="<?php echo $doc->getTitle("title");?>"/>
    <meta property="og:description" content="<?php echo $doc->getMetaData("description");?>"/>
  
  <script>(function() {
    var _fbq = window._fbq || (window._fbq = []);
      if (!_fbq.loaded) {
        var fbds = document.createElement('script');
        fbds.async = true;
        fbds.src = '//connect.facebook.net/en_US/fbds.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(fbds, s);
        _fbq.loaded = true;
        }
    
        _fbq.push(['addPixelId', '650607948342770']);
    })();
    
    window._fbq = window._fbq || [];
    window._fbq.push(['track', 'PixelInitialized', {}]);
  </script>
  <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=650607948342770&amp;ev=NoScript" /></noscript>
  <meta name="google-site-verification" content="_1HDYwuTKG2CeInuRorshvSMYwt_2_wX4cmwBw4WW_8" />
  <!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body <?php echo $class; ?>>


<a name="top"></a>
<!-- !Navbar ================================================== -->
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
    
    <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      
      <!-- Be sure to leave the brand out there if you want it shown -->
      <a class="brand" href="<?php echo $this->baseurl ?>" title="<?php echo $app->getCfg('sitename'); ?>"><img alt="<?php echo $app->getCfg('sitename'); ?>" width="240" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/img/rosetta_reizen_logo.png"/></a>
       
      <!-- Everything you want hidden at 940px or less, place within here -->
      <div class="nav-collapse pull-right">
        <jdoc:include type="modules" name="nav"/>
      </div>
    
    </div>
  </div>
</div>



<!-- !Header ================================================== -->

<header id="header" class="header <? if($home){ ?>homepage<? }; ?>"> <!--  add 'homepage' to class to get a big header-->
  <div class="imgcontainer">
    <?php
      
      if(!$image){
    ?>
        <jdoc:include type="modules" name="header"/>
    <?php
      }
      else{
        ?>
          <img  width="100%" src="<? echo $image; ?>" alt="<?php echo $app->getCfg('sitename'); ?> - <?php echo $title; ?>" title="<?php echo $app->getCfg('sitename'); ?> - <?php echo $title; ?>"/>
        <?php
        
      } 
    ?>
  </div>
  
  <div class="container">
    <div class="row">
      <div class="span6 offset6 header-module-bg"></div>
      <div class="span3 offset9 header-module"> 
        
        <jdoc:include type="modules" name="header-mod" style="xhtml"/>
      </div>
    </div>
    </div>
</header>



<!-- !Container ================================================== -->
<div id="content-container" class="container">
  <div class="row">
    <!-- !Content ================================================== -->
    
    <div class="span9 content">
      <div class="temp-debugoff"></div> <!--  NOG VERWIJDEREN!-->
      <jdoc:include type="component" />
      <jdoc:include type="message" />
      <div class="social clear">
        <div class="backtotop hidden-phone">
          <a href="#top" class="btn btn-mini"><strong>Terug naar boven</strong></a>
        </div>
        <div class="twitter">
          <a href="https://twitter.com/share" class="twitter-share-button" data-via="dromenoplocatie" data-lang="nl">Tweeten</a>
        </div>
        <div class="facebook">
          <div class="fb-like" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false"></div>
        </div>
        <div class="facebook">
          <a class="btn btn-mini" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo curPageURL(); ?>" target="_blank">
            Deel op Facebook
          </a>
        </div>
      </div>
      
    </div>
    <!-- !Sidebar ================================================== -->
    <aside  class="span3">
      <div id="sidebar" class="sidebar">
        <?php
          if(JRequest::getInt('trip_id')){
        ?>
        <jdoc:include type="modules" name="sidebar_related" style="xhtml"/>  
        <?php
          }
        ?>
        <jdoc:include type="modules" name="sidebar" style="xhtml"/>  
      </div>
    </aside>
  </div>
</div>


<!-- !Footer ================================================== -->
<footer class="footer">
  <div class="imgwrapper">
    <div class="imgcontainer">
      <?php
        if(!$image){
      ?>
          <jdoc:include type="modules" name="header"/>
      <?php
        }
        else{
          ?>
            <img  width="100%" src="<? echo $image; ?>" alt="<?php echo $app->getCfg('sitename'); ?> - <?php echo $title; ?>" title="<?php echo $app->getCfg('sitename'); ?> - <?php echo $title; ?>"/>
          <?php
          
        } 
      ?>
      
    </div>
    <div class="overlay"></div>
  </div>
  <div class="container">
    <div class="span12">
      <div class="pull-left"><jdoc:include type="modules" name="footer-left" style="xhtml"/></div>
      <div class="pull-right"><jdoc:include type="modules" name="footer-right" style="xhtml"/></div>
    </div>
  </div>
</footer>



<!-- !Scripts ================================================== -->
  <script src="https://code.jquery.com/jquery-latest.js"></script>
    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/jquery.cycle.lite.js"></script>
    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/script.js"></script>

    
    <!-- FACEBOOK LIKE BTN -->
    <div id="fb-root"></div>
  <script>
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/nl_NL/all.js#xfbml=1&appId=143928955711173";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  </script>
  
  <!-- TWITTER TWEET BTN -->
  <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    
    
    <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-25295385-36']);
    _gaq.push(['_setDomainName', 'rosettareizen.nl']);
    _gaq.push(['_setAllowLinker', true]);
    _gaq.push(['_trackPageview']);
  
    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      /* ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'https://www') + '.google-analytics.com/ga.js'; */
      ga.src = ('https:' == document.location.protocol ? 'https://' : 'https://') + 'stats.g.doubleclick.net/dc.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
  
  </script>
  
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-25295385-36', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

</script>

  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
   
    ga('create', 'UA-19100441-1', 'rosettareizen.nl');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
   
  </script>
</body>
</html>