<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_menu
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

// Note. It is important to remove spaces between elements.

?>

<ul class="menu<?php echo $class_sfx;?>"<?php
	$tag = '';
	if ($params->get('tag_id')!=NULL) {
		$tag = $params->get('tag_id').'';
		echo ' id="'.$tag.'"';
	}
?>>
<?php
	
	$sorted = array();
	foreach($list as $key=>$item){
		if($item->parent_id == 105){
			$sorted[$key] = $item;
			unset($list[$key]);
		}
	}
	
	function cmp( $a, $b )
	{ 
	  if(  $a->title ==  $b->title ){ return 0 ; } 
	  return ($a->title < $b->title) ? -1 : 1;
	} 
	
	uasort($sorted,'cmp');
	
	$keys = array_keys($sorted);
	asort($keys);
	$startfrom = $keys[0];
	
	$newarray = array();
	foreach($sorted as $sort){
		$sort->level_diff = 0;
		$sort->shallower = '';
		$newarray[$startfrom] = $sort;
		$startfrom++;
	}
	
	end($newarray)->level_diff = 1;
	end($newarray)->shallower = 1;

	$list = $list + $newarray;
	
	ksort($list);
	
foreach ($list as $i => &$item) :

	$class = 'item-'.$item->id;
	if ($item->id == $active_id) {
		$class .= ' current';
	}

	if (in_array($item->id, $path)) {
		$class .= ' active';
	}
	elseif ($item->type == 'alias') {
		$aliasToId = $item->params->get('aliasoptions');
		if (count($path) > 0 && $aliasToId == $path[count($path)-1]) {
			$class .= ' active';
		}
		elseif (in_array($aliasToId, $path)) {
			$class .= ' alias-parent-active';
		}
	}
	
	if ($item->deeper) {
		$class .= ' dropdown';
	}
	if($item->level == 2){
		$class .= ' dropdown-submenu';
	}


	if ($item->parent) {
		$class .= ' parent';
	}

	if (!empty($class)) {
		$class = ' class="'.trim($class) .'"';
	}

	echo '<li'.$class.'> ';

	// Render the menu item.
	switch ($item->type) :
		case 'separator':
			
		case 'url':
		case 'component':
			require JModuleHelper::getLayoutPath('mod_menu', 'default_'.$item->type);
			break;

		default:
			require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
			break;
	endswitch;

	// The next item is deeper.
	if ($item->deeper) {
		echo '<ul class="dropdown-menu">';
	}
	// The next item is shallower.
	elseif ($item->shallower) {
		echo '</li>';
		echo str_repeat('</ul></li>', $item->level_diff);
	}
	// The next item is on the same level.
	else {
		echo '</li>';
	}
endforeach;
?></ul>
