<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_menu
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

require_once(JPATH_BASE.DS.'components'.DS.'com_trip'.DS.'helpers'.DS.'route.php');


// Note. It is important to remove spaces between elements.
$class = $item->anchor_css ? 'class="no-hyperlink-tablet '.$item->anchor_css.'" ' : '';
$title = $item->anchor_title ? 'title="'.$item->anchor_title.'" ' : '';
if ($item->menu_image) {
		$item->params->get('menu_text', 1 ) ?
		$linktype = '<img src="'.$item->menu_image.'" alt="'.$item->title.'" /><span class="image-title">'.$item->title.'</span> ' :
		$linktype = '<img src="'.$item->menu_image.'" alt="'.$item->title.'" />';
}
else { $linktype = $item->title;
}

switch ($item->browserNav) :
	default:
	case 0:

		?><a <?php echo $class; ?> href="<?php echo $item->flink; ?>" <?php echo $title; ?>><?php echo $linktype; ?></a><?php
		// geen menuitems instellen in de backend, deze automatisch zelf ophalen en toevoegen.
		if(strpos($item->link, 'view=trip')){

			$triptype_id = $item->params->get('triptype_id');
			if($triptype_id){
				$db =& JFactory::getDBO();
				$query = "SELECT * FROM #__xtrip_trips WHERE triptype_id=".$db->Quote($triptype_id)." AND state=1 ORDER BY title ASC ";
				$db->setQuery($query);
				if($trips = $db->loadObjectList()){
					?>
					<ul class="dropdown-menu">
						<?php
							foreach($trips as $trip){
								//$alias =  strtolower(preg_replace("/[^a-zA-Z0-9\s]/", "", $trip->title));
								//$alias = $trip->title;
								$alias = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($trip->title, ENT_QUOTES, 'UTF-8'));
								$alias = strtolower($alias);
								$alias = str_replace("  "," ",$alias);
								$alias = str_replace(" ","-",$alias);
								?>
									<!--<li><a title="<?php echo $trip->title; ?>" href="<?php echo JRoute::_('index.php?option=com_trip&view=trip&trip_id='.$trip->trip_id.":".$alias."&Itemid=".$item->id); ?>"><?php echo $trip->title; ?></a></li>-->
								<?php
							}
						?>
					</ul>
					<?php
				}
			}
		}
		elseif(strpos($item->link, 'view=destination')){
			$destination_id = $item->params->get('destination_id');
			if($destination_id){
				$db =& JFactory::getDBO();
				$query = " SELECT title FROM #__xtrip_destinations WHERE destination_id=".$db->Quote($destination_id);
				$db->setQuery($query);
				$departure_title = $db->loadObject()->title;

				$query  = " SELECT t.*, tt.triptype FROM #__xtrip_trips t ";
				$query .= " INNER JOIN #__xtrip_triptypes tt ON tt.id=t.triptype_id ";
				if($departure_title){
					$query .= " WHERE (t.destination_id=".$db->Quote($destination_id)." || t.title LIKE '%".$departure_title."%') AND t.state=1 ";
				}
				else{
					$query .= " WHERE t.destination_id=".$db->Quote($destination_id)." AND t.state=1 ";
				}

				$query .= " ORDER BY t.title ASC ";

				$db->setQuery($query);
				if($trips = $db->loadObjectList()){
					?>
					<ul class="dropdown-menu">
						<li><a class="no-dropdown" href="<?php echo $item->flink; ?>" <?php echo $title; ?>>Rondreis <?php echo $linktype; ?></a></li>
						<?php
							foreach($trips as $trip){
								//$alias =  strtolower(preg_replace("/[^a-zA-Z0-9\s]/", "", $trip->title));
								$alias = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($trip->title, ENT_QUOTES, 'UTF-8'));
								$alias = strtolower($alias);
								$alias = str_replace("  "," ",$alias);
								$alias = str_replace(" ","-",$alias);
								?>
									<li><a title="<?php echo $trip->title; ?>" href="<?php echo JRoute::_('index.php?option=com_trip&view=trip&trip_id='.$trip->trip_id.":".$alias."&Itemid=".TripHelperRoute::getItemIdByTriptype($trip->triptype_id)); ?>"><?php echo $trip->title; ?> ( <?php echo $trip->triptype; ?> )</a></li>
								<?php
							}
						?>
					</ul>
					<?php
				}
			}
		}

		break;
	case 1:
		// _blank
?><a <?php echo $class; ?>href="<?php echo $item->flink; ?>" target="_blank" <?php echo $title; ?>><?php echo $linktype; ?></a><?php
		break;
	case 2:
	// window.open
?><a <?php echo $class; ?>href="<?php echo $item->flink; ?>" onclick="window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes');return false;" <?php echo $title; ?>><?php echo $linktype; ?></a>
<?php
		break;
endswitch;
