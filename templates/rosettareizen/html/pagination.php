<?php defined('_JEXEC') or die;
/**
 * @package        Template Framework for Joomla!+
 * @author        Cristina Solana http://nightshiftcreative.com
 * @author        Matt Thomas http://construct-framework.com | http://betweenbrain.com
 * @copyright    Copyright (C) 2009 - 2012 Matt Thomas. All rights reserved.
 * @license        GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 */

/**
 * This is a file to add template specific chrome to pagination rendering.
 *
 * pagination_list_footer
 *     Input variable $list is an array with offsets:
 *         $list[limit]        : int
 *         $list[limitstart]    : int
 *         $list[total]        : int
 *         $list[limitfield]    : string
 *         $list[pagescounter]    : string
 *         $list[pageslinks]    : string
 *
 * pagination_list_render
 *     Input variable $list is an array with offsets:
 *         $list[all]
 *             [data]        : string
 *             [active]    : boolean
 *         $list[start]
 *             [data]        : string
 *             [active]    : boolean
 *         $list[previous]
 *             [data]        : string
 *             [active]    : boolean
 *         $list[next]
 *             [data]        : string
 *             [active]    : boolean
 *         $list[end]
 *             [data]        : string
 *             [active]    : boolean
 *         $list[pages]
 *             [{PAGE}][data]        : string
 *             [{PAGE}][active]    : boolean
 *
 * pagination_item_active
 *     Input variable $item is an object with fields:
 *         $item->base    : integer
 *         $item->link    : string
 *         $item->text    : string
 *
 * pagination_item_inactive
 *     Input variable $item is an object with fields:
 *         $item->base    : integer
 *         $item->link    : string
 *         $item->text    : string
 *
 * This gives template designers ultimate control over how pagination is rendered.
 *
 * NOTE: If you override pagination_item_active OR pagination_item_inactive you MUST override them both
 */
function pagination_list_footer($list)
{	
		$html  = '<div class="pagination pagination-centered">';
		$html .= '<ul>';
		$html .= "<li class=\"disabled\"><span>" . JText::_('JGLOBAL_DISPLAY_NUM')  . $list['limitfield'] . "</span></li>";
		$html .= $list['pageslinks'];

		$html .= "<input type=\"hidden\" name=\"" . $list['prefix'] . "limitstart\" value=\"" . $list['limitstart'] . "\" />";
		$html .= '</ul>';
		$html .= '</div><div class="clearfix"></div>';

		return $html;
}
	
function pagination_list_render($list)
{
    // Initialize variables
   // Reverse output rendering for right-to-left display.
		
		$html = $list['start']['data'];
		$html .= $list['previous']['data'];
		foreach ($list['pages'] as $page)
		{
			$html .=  $page['data'];
		}
		$html .= $list['next']['data'];
		$html .= $list['end']['data'];
		

		return $html;
}

function pagination_item_active(&$item)
{
	$app = JFactory::getApplication();
		if ($app->isAdmin())
		{
			if ($item->base > 0)
			{
				return "<li><a title=\"" . $item->text . "\" onclick=\"document.adminForm." . $this->prefix . "limitstart.value=" . $item->base
					. "; Joomla.submitform();return false;\">" . $item->text . "</a></li>";
			}
			else
			{
				return "<li><a title=\"" . $item->text . "\" onclick=\"document.adminForm." . $this->prefix
					. "limitstart.value=0; Joomla.submitform();return false;\">" . $item->text . "</a></li>";
			}
		}
		else
		{
			return "<li><a title=\"" . $item->text . "\" href=\"" . $item->link . "\">" . $item->text . "</a></li>";
		}
}

function pagination_item_inactive(&$item)
{
   $app = JFactory::getApplication();
		if ($app->isAdmin())
		{
			return "<li class=\"disabled\"><span>" . $item->text . "</span></li>";
		}
		else
		{
			if(is_numeric($item->text)){
				return "<li class=\"active\"><span>" . $item->text . "</span></li>";
			}
			else{
				return "<li class=\"disabled\"><span>" . $item->text . "</span></li>";
			}
		}

}

?>