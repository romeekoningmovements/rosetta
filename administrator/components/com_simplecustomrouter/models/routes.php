<?php
/**
 * @copyright	Copyright (C) 2012 Daniel Calviño Sánchez
 * @license		GNU Affero General Public License version 3 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Model for the manager of the routes.
 * It uses Joomla infrastructure.
 */
class SimpleCustomRouterModelRoutes extends JModelList {

    /**
     * Creates a new SimpleCustomRouterModelRoutes.
     * The id, path and query are set as filter fields (so the list can be
     * ordered by those fields in the view).
     * 
	 * @param array $config An optional associative array of configuration
	 *        settings.
     */
    public function __construct($config = array()) {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'id',
                'path',
                'query'
            );
        }

        parent::__construct($config);
    }
    
    /**
	 * Implementation of JModelList::getListQuery to get a JDatabaseQuery object
	 * for retrieving the data set from a database.
	 * The query selects all the fields from #__simplecustomrouter, ordered by
	 * the column and direction set in the state by the parent model.
	 *
	 * @return JDatabaseQuery A JDatabaseQuery object to retrieve the data set.
     */
    protected function getListQuery() {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        $query->select('id,path,query,itemId');
        $query->from('#__simplecustomrouter');

        $orderColumn = $this->state->get('list.ordering', 'id');
        $orderDirection = $this->state->get('list.direction', 'ASC');
        $query->order($db->getEscaped($orderColumn.' '.$orderDirection));

        return $query;
    }
}
