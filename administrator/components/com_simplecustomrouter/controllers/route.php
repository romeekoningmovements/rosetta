<?php
/**
 * @copyright	Copyright (C) 2012 Daniel Calviño Sánchez
 * @license		GNU Affero General Public License version 3 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
 
jimport('joomla.application.component.controllerform');
 
/**
 * Controller for the form to edit a route.
 * It uses Joomla infrastructure.
 */
class SimpleCustomRouterControllerRoute extends JControllerForm {
}
