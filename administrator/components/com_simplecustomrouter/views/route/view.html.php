<?php
/**
 * @copyright	Copyright (C) 2012 Daniel Calviño Sánchez
 * @license		GNU Affero General Public License version 3 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
 
jimport('joomla.application.component.view');

/**
 * View for the form to edit a route.
 * It uses Joomla infrastructure.
 */
class SimpleCustomRouterViewRoute extends JView {

    /**
     * Displays the view.
     */
    public function display($tpl = null) {
        $form = $this->get('Form');
        $item = $this->get('Item');

        if (count($errors = $this->get('Errors'))) {
            JError::raiseError(500, implode('<br />', $errors));
            return false;
        }

        $this->form = $form;
        $this->item = $item;

        JRequest::setVar('hidemainmenu', true);
        
        $this->addToolBar();

        parent::display($tpl);

        $this->setTitle();
    }

    /**
     * Sets the toolbar.
     */
    protected function addToolBar() {
        $canDo = SimpleCustomRouterHelper::getActions();

        $isNew = $this->item->id == 0;
        if ($isNew) {
            JToolBarHelper::title(JText::_('COM_SIMPLECUSTOMROUTER_MANAGER_ROUTE_NEW'));
            
            if ($canDo->get('core.create')) {
                JToolBarHelper::apply('route.apply', 'JTOOLBAR_APPLY');
                JToolBarHelper::save('route.save', 'JTOOLBAR_SAVE');
                JToolBarHelper::custom('route.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
            }
            
            JToolBarHelper::cancel('route.cancel', 'JTOOLBAR_CANCEL');
        } else {
            JToolBarHelper::title(JText::_('COM_SIMPLECUSTOMROUTER_MANAGER_ROUTE_EDIT'));
            
            if ($canDo->get('core.edit')) {
                JToolBarHelper::apply('route.apply', 'JTOOLBAR_APPLY');
                JToolBarHelper::save('route.save', 'JTOOLBAR_SAVE');

                if ($canDo->get('core.create')) {
                    JToolBarHelper::custom('route.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
                }
            }
            
            if ($canDo->get('core.create')) {
                JToolBarHelper::custom('route.save2copy', 'save-copy.png', 'save-copy_f2.png', 'JTOOLBAR_SAVE_AS_COPY', false);
            }
            
            JToolBarHelper::cancel('route.cancel', 'JTOOLBAR_CLOSE');
        }
    }

    /**
     * Sets the document title.
     */
    protected function setTitle() {
        $document = JFactory::getDocument();
        $isNew = $this->item->id == 0;
        if ($isNew) {
            $document->setTitle(JText::_('COM_SIMPLECUSTOMROUTER_ROUTE_CREATING'));
        } else {
            $document->setTitle(JText::_('COM_SIMPLECUSTOMROUTER_ROUTE_EDITING'));
        }
    }
}
