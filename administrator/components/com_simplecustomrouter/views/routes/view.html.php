<?php
/**
 * @copyright	Copyright (C) 2012 Daniel Calviño Sánchez
 * @license		GNU Affero General Public License version 3 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
 
jimport('joomla.application.component.view');

/**
 * View for the manager of the routes.
 * It uses Joomla infrastructure.
 */
class SimpleCustomRouterViewRoutes extends JView {

    /**
     * Displays the view.
     */
    function display($tpl = null) {
        $items = $this->get('Items');
        $pagination = $this->get('Pagination');
        $state = $this->get('State');
        
        if (count($errors = $this->get('Errors'))) {
            JError::raiseError(500, implode('<br />', $errors));
            return false;
        }

        $this->items = $items;
        $this->pagination = $pagination;

        $this->listOrder = $this->escape($state->get('list.ordering'));
        $this->listDirection = $this->escape($state->get('list.direction'));
        
        $this->addToolBar();

        parent::display($tpl);

        $this->setTitle();
    }

    /**
     * Sets the toolbar.
     */
    protected function addToolBar() {
        $canDo = SimpleCustomRouterHelper::getActions();
        
        JToolBarHelper::title(JText::_('COM_SIMPLECUSTOMROUTER_MANAGER_ROUTES'), 'routes');
        
        if ($canDo->get('core.create')) {
            JToolBarHelper::addNew('route.add', 'JTOOLBAR_NEW');
        }
        
        if ($canDo->get('core.edit')) {
            JToolBarHelper::editList('route.edit', 'JTOOLBAR_EDIT');
        }
        
        if ($canDo->get('core.delete')) {
            JToolBarHelper::deleteList('', 'routes.delete', 'JTOOLBAR_DELETE');
        }
        
        if ($canDo->get('simplecustomrouter.test')) {
            JToolBarHelper::divider();
            JToolBarHelper::custom('test', 'default', 'default', 'COM_SIMPLECUSTOMROUTER_TOOLBAR_TEST', false);
        }
        
        if ($canDo->get('core.admin')) {
            JToolBarHelper::divider();
            JToolBarHelper::preferences('com_simplecustomrouter');
        }
    }

    /**
     * Sets the document title.
     */
    protected function setTitle() {
        $document = JFactory::getDocument();
        $document->setTitle(JText::_('COM_SIMPLECUSTOMROUTER_ADMINISTRATION'));
    }
}
