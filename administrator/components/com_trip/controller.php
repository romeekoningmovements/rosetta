
<?php

// No direct access
defined('_JEXEC') or die;


class TripController extends JControllerLegacy
{
	function display($cachable = false) 
	{
		// Set default view if not set
		JRequest::setVar('view', JRequest::getCmd('view', 'Trips'));
		JHTML::_('behavior.framework');
		JHTML::_('behavior.modal');
				// Load the submenu.
		TripHelper::addSubmenu(JRequest::getCmd('view', 'trips'));
			
		$view		= JRequest::getCmd('view', 'trip');
		$layout 	= JRequest::getCmd('layout', 'default');
		$id			= JRequest::getInt('id');
		$task 	    = JRequest::getVar('task');
		$app 		=& JFactory::getApplication();
		
		
		parent::display();

		return $this;
	}
}

