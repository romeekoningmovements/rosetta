<?php
// No direct access
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');

JHtml::_('behavior.formvalidation');
$params = $this->form->getFieldsets('params');

?>
<form action="<?php echo JRoute::_('index.php?option=com_trip&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="tripcategory-form" class="form-validate">
	   <div class="width-100 fltlft">
	      <fieldset class="adminform">
	         <legend><?php echo JText::_( 'COM_TRIP_TRIPCATEGORY_BASIC' ); ?></legend>
	          <table>
	         	<tbody>
	       
		            <?php foreach($this->form->getFieldset('basic') as $field): ?>
		            	<tr>
		         			<td><?php echo $field->label; ?></td>
		         			<td><?php echo $field->input;?></td>
		         		</tr>
		            <?php endforeach; ?>
	         
	          </tbody>
	         </table>
	      </fieldset>
	   </div>
   <input type="hidden" name="task" value="tripcategory.edit" />
   <?php echo JHtml::_('form.token'); ?>
</form>