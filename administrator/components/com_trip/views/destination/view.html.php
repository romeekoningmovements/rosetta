<?php

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');
JHTML::_('behavior.framework');
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidator');

JFactory::getDocument()->addScriptDeclaration('
	Joomla.submitbutton = function(task)
	{
		if (task == "destination.cancel" || document.formvalidator.isValid(document.getElementById("destination-form")))
		{
			Joomla.submitform(task, document.getElementById("destination-form"));
		}
	};
');

class TripViewDestination extends JViewLegacy
{
	public function display($tpl = null)
	{
		$form   = $this->get('Form');
		$item   = $this->get('Item');
		$script = $this->get('Script');
		$contentypes = $this->get('Contenttypes');
 		
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Assign the Data
		$this->form = $form;
		$this->item = $item;
		$this->script = $script;
		$this->contenttypes = $contentypes;
 
		// Set the toolbar
		$this->addToolBar();
		
		// Display the template
		parent::display($tpl);
		
		// Set the document
		$this->setDocument();
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar() 
	{
		$input = JFactory::getApplication()->input;
		$input->set('hidemainmenu', true);
		$user = JFactory::getUser();
		$userId = $user->id;
		$isNew = $this->item->destination_id == 0;
	
		$canDo = TripHelper::getActions($this->item->destination_id);
		//TripHelper::addSubmenu();
		$this->canDo = $canDo;
		JToolBarHelper::title($isNew ? JText::_('COM_SHOP_MANAGER_DESTINATION_NEW') : JText::_('COM_SHOP_MANAGER_DESTINATION_EDIT').': <small><small>[  '.$this->item->title.'  ]</small></small>', 'icon-com_shop');
		// Built the actions for new and existing records.
		if ($isNew) 
		{
			// For new records, check the create permission.
			if ($canDo->get('core.create')) 
			{
				JToolBarHelper::apply('destination.apply', 'JTOOLBAR_APPLY');
				JToolBarHelper::save('destination.save', 'JTOOLBAR_SAVE');
				JToolBarHelper::custom('destination.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
			}
			JToolBarHelper::cancel('destination.cancel', 'JTOOLBAR_CANCEL');
		}
		else
		{

			if ($canDo->get('core.edit'))
			{
				// We can save the new record
				JToolBarHelper::apply('destination.apply', 'JTOOLBAR_APPLY');
				JToolBarHelper::save('destination.save', 'JTOOLBAR_SAVE');
 
				// We can save this record, but check the create permission to see
				// if we can return to make a new one.
				if ($canDo->get('core.create')) 
				{
					JToolBarHelper::custom('destination.save2new', 'save-new.png', 'save-new_f2.png',
					                       'JTOOLBAR_SAVE_AND_NEW', false);
				}
			}
			if ($canDo->get('core.create')) 
			{
				//JToolBarHelper::custom('product.save2copy', 'save-copy.png', 'save-copy_f2.png', 'JTOOLBAR_SAVE_AS_COPY', false);
			}
			JToolBarHelper::cancel('destination.cancel', 'JTOOLBAR_CLOSE');
		}
	}
	
	protected function setDocument() 
	{
		$isNew = $this->item->destination_id == 0;
		$document = JFactory::getDocument();
		$document->setTitle($isNew ? JText::_('COM_SHOP_DESTINATION_CREATING')
		                           : JText::_('COM_SHOP_DESTINATION_EDITING'));
		$document->addScript(JURI::root() . $this->script);
		$document->addScript(JURI::root() . "/administrator/components/com_trip"
		                                  . "/views/destination/submitbutton.js");
		JText::script('COM_SHOP_DESTINATION_ERROR_UNACCEPTABLE');
	}
}
