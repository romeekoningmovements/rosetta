<?php
// No direct access
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
$doc =& JFactory::getDocument();


$params = $this->form->getFieldsets('params');

?>

<form action="<?php echo JRoute::_('index.php?option=com_trip&layout=edit&id='.(int) $this->item->destination_id); ?>" method="post" name="adminForm" id="trip-form" class="form-validate">
	<div class="width-100 fltlft">
		<fieldset class="adminform">
			<legend><?php echo JText::_( 'COM_SHOP_TRIP_BASIC' ); ?></legend>
			<table class="table">
				<?php foreach($this->form->getFieldset('basic') as $field): ?>
					<?php 
						if($field->fieldname != 'id' || (int)$this->item->destination_id != 0){
					?>
					<tr>
						<td><?php echo $field->label; ?></td>
						<td><?php echo $field->input;?></td>
					</tr>
					<?php 
					}
					?>
				<?php endforeach; ?>  
			</table>
		</fieldset>
	</div>
    <input type="hidden" name="task" value="trip.edit" />
    <?php echo JHtml::_('form.token'); ?>
</form>
