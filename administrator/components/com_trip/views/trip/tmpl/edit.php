<?php
// No direct access
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

$params = $this->form->getFieldsets('params');

?>

<form action="<?php echo JRoute::_('index.php?option=com_trip&layout=edit&id='.(int) $this->item->trip_id); ?>" method="post" name="adminForm" id="trip-form" class="form-validate">
	<div>
		<fieldset class="adminform">
			<legend><?php echo JText::_( 'COM_SHOP_TRIP_BASIC' ); ?></legend>
			<ul class="adminformlist">
				<li><?php echo $this->form->getLabel('title'); ?>
				<?php echo $this->form->getInput('title'); ?></li>
				<li><?php echo $this->form->getLabel('triptype_id'); ?>
				<?php echo $this->form->getInput('triptype_id'); ?></li>
				<li><?php echo $this->form->getLabel('destination_id'); ?>
				<?php echo $this->form->getInput('destination_id'); ?></li>
				<li><?php echo $this->form->getLabel('state'); ?>
				<?php echo $this->form->getInput('state'); ?></li>
			</ul>
			<div class="clr"></div>
			<?php echo $this->form->getLabel('metadescription'); ?>
			<?php echo $this->form->getInput('metadescription'); ?>
			<div class="clr"></div>
			<?php echo $this->form->getLabel('metakeywords'); ?>
			<?php echo $this->form->getInput('metakeywords'); ?>
			<div class="clr"></div>
			<?php echo $this->form->getLabel('extrainfo'); ?>
			<?php echo $this->form->getInput('extrainfo'); ?>
		</fieldset>
		<fieldset class="adminform">
			<legend><?php echo JText::_( 'COM_SHOP_TRIP_IMAGES' ); ?></legend>
			<ul class="adminformlist">		
				<li><?php echo $this->form->getLabel('image'); ?>
				<?php echo $this->form->getInput('image'); ?></li>
				<li><?php echo $this->form->getLabel('imagethumb'); ?>
				<?php echo $this->form->getInput('imagethumb'); ?></li>
				<li><?php echo $this->form->getLabel('route'); ?>
				<?php echo $this->form->getInput('route'); ?></li>
			</ul>
		</fieldset>			
		<fieldset class="adminform">
			<legend><?php echo JText::_( 'COM_SHOP_TRIP_COSTS' ); ?></legend>
			<ul class="adminformlist">
			<li><?php echo $this->form->getLabel('reissom'); ?>
				<?php echo $this->form->getInput('reissom'); ?></li>
				<li><?php echo $this->form->getLabel('luchthaventax'); ?>
				<?php echo $this->form->getInput('luchthaventax'); ?></li>
				<li><?php echo $this->form->getLabel('boekingsbijdrage'); ?>
				<?php echo $this->form->getInput('boekingsbijdrage'); ?></li>
				<li><?php echo $this->form->getLabel('calamiteitenfonds'); ?>
				<?php echo $this->form->getInput('calamiteitenfonds'); ?></li>
				<li><?php echo $this->form->getLabel('visumnodig'); ?>
				<?php echo $this->form->getInput('visumnodig'); ?></li>
				<li><?php echo $this->form->getLabel('visumkosten'); ?>
				<?php echo $this->form->getInput('visumkosten'); ?></li>		
			</ul>
		</fieldset>
		<fieldset class="adminform">
			<legend><?php echo JText::_( 'COM_SHOP_TRIP_DEPARTURES' ); ?></legend>
			<ul class="adminformlist">
				<li>
					<?php echo $this->form->getLabel('willekeurige_vertrek_datum'); ?>
					<?php echo $this->form->getInput('willekeurige_vertrek_datum'); ?>
				</li>
				<li>
					<?php echo $this->form->getLabel('departure'); ?>
					<?php echo $this->form->getInput('departure'); ?>
				</li>		
			</ul>
		</fieldset>
		<fieldset class="adminform">
			<legend><?php echo JText::_( 'COM_SHOP_TRIP_OPTIONS' ); ?></legend>
			<ul class="adminformlist">
				<li>
					<?php echo $this->form->getLabel('options'); ?>
					<?php echo $this->form->getInput('options'); ?>
				</li>		
			</ul>
		</fieldset>
		<fieldset class="adminform">
			<legend><?php echo JText::_( 'COM_SHOP_TRIP_INTROTEXT' ); ?></legend>
			<div class="clr"></div>
			<?php echo $this->form->getLabel('introtext'); ?>
			<div class="clr"></div>
			<?php echo $this->form->getInput('introtext'); ?>
		</fieldset>
		<fieldset class="adminform">
			<legend><?php echo JText::_( 'COM_SHOP_TRIP_FULLTEXT' ); ?></legend>
			<div class="clr"></div>
			<?php echo $this->form->getLabel('fulltext'); ?>
			<div class="clr"></div>
			<?php echo $this->form->getInput('fulltext'); ?>
		</fieldset>
		
	</div>

	<div>
		<?php echo JHtml::_('sliders.start', 'content-sliders-'.$this->item->trip_id, array('useCookie'=>1)); ?>
		<?php
			$contenttypes = TripHelper::getContentTypes();
			if($contenttypes){
				$db =& JFactory::getDBO();
				$query = "SELECT * FROM #__xtrip_content WHERE trip_id=".$db->Quote($this->item->trip_id);
				$db->setQuery($query);
				$results = $db->loadObjectList('cat_id');
				foreach($contenttypes as $contenttype){
					?>
					<?php echo JHtml::_('sliders.panel', $contenttype->cattitle, 'publishing-details'); ?>
						<fieldset class="panelform">
							<ul class="adminformlist">
								<input type="hidden" name="jform[contenttypes][{$contenttype->catid}]_tripcontent_category" value="<?php echo $contenttype->catid; ?>"/>
								
								<div class="clr"></div>
								<?php
									 $editor = JFactory::getConfig()->get('editor');
									 $editor = JEditor::getInstance($editor);
									 echo $editor->display( "jform[contenttypes][{$contenttype->catid}]_tripcontent_category", @$results[$contenttype->catid]->introtext, '100%',400,40,20,true,null,20,30);
								?>
								<div class="clr" style="margin:20px 0; float:left; width:100%; display:block; border-bottom:solid 1px #CCC;"></div>
								<li>
									<label>Metadata omschrijving</label>
									<textarea name="jform[contenttypes][<?php echo $contenttype->catid; ?>][tripcontent_metadata_desc]" cols="30" rows="3" class="inputbox"><?php echo @$results[$contenttype->catid]->metadescription; ?></textarea>
								</li>
																
								<li>
									<label>Metadata sleutelwoorden</label>
									<input type="text" size="80" name="jform[contenttypes][<?php echo $contenttype->catid; ?>][tripcontent_metadata_keywords]" value="<?php echo @$results[$contenttype->catid]->metakeywords; ?>"/>
								</li>
							</ul>
						</fieldset>
					<?php
				}
			}
		?>
		<?php echo JHtml::_('sliders.end'); ?>
	</div>
	<div class="clr"></div>
	<input type="hidden" name="task" value="trip.edit" />
	<?php echo JHtml::_('form.token'); ?>
</form>


		   		
		   		
		   		
<!--
<form action="<?php echo JRoute::_('index.php?option=com_trip&layout=edit&id='.(int) $this->item->trip_id); ?>" method="post" name="adminForm" id="trip-form" class="form-validate">
	   <div class="well" style='float:left; width:50%; height:auto;'>
	   	  <h2><?php echo JText::_( 'COM_SHOP_TRIP_BASIC' ); ?></h2>
	   	  <div class="well width-50 fltlft">
	   	  	<table class="table">
			   	  <?php foreach($this->form->getFieldset('basic') as $field): ?>
		            <?php 
		            	if($field->fieldname != 'id' || (int)$this->item->trip_id != 0){
		            ?>
	            	<tr>
	         			<td><?php echo $field->label; ?></td>
	         			<td><?php echo $field->input;?></td>
	         		</tr>
	             <?php 
	            	}
	             ?>
	            <?php endforeach; ?>  
	   	  	</table>
	   	  </div>
	   </div>
	   <div style='float:left; width:50%; height:auto;'>
		   	<div class="accordionwrapper">
		   		<?php
		   			$contenttypes = TripHelper::getContentTypes();
		   			if($contenttypes){
		   				$db =& JFactory::getDBO();
		   				$query = "SELECT * FROM #__xtrip_content WHERE trip_id=".$db->Quote($this->item->trip_id);
		   				$db->setQuery($query);
		   				$results = $db->loadObjectList('cat_id');
			   			//print "<pre>";
			   			//print_R($results);
			   			//print "</pre>";
		   				//print $query;
		   				foreach($contenttypes as $contenttype){
		   				//print "<pre>";
		   				//print_R($contenttype);
		   				//print "</pre>";
		   					?>
		   						 <h2 class="toggler" style="background:red;"><?php echo $contenttype->cattitle; ?></h2>
							   	  <div class="element">
								   	  <div class="width-50 fltlft">
									   	 	<input type="hidden" name="jform[contenttypes][<?php echo $contenttype->catid; ?>][tripcontent_category]" value="<?php echo $contenttype->catid; ?>"/>
											<table>
												<tbody>
													<tr>
														<td><label>Introtekst</label></td>
														<td>
															<?php
																 $editor =& JFactory::getEditor();
																 $editor->getButtons($editor->get('_name'), true);
																
														         echo $editor->display("jform[contenttypes][".$contenttype->catid."][tripcontent_introtext]",@$results[$contenttype->catid]->introtext,500,400,40,20,1);
															?>
														</td>
													</tr>
													<tr>
														<td><label>Volledige tekst</label></td>
														<td>
															<?php
																 $editor =& JFactory::getEditor();
														         echo $editor->display("jform[contenttypes][".$contenttype->catid."][tripcontent_fulltext]",@$results[$contenttype->catid]->fulltext,500,400,40,20,1);
															?>
														</td>
													</tr>
													<tr>
														<td><label>Metadata omschrijving</label></td>
														<td>
															<textarea name="jform[contenttypes][<?php echo $contenttype->catid; ?>][tripcontent_metadata_desc]" rows="15" cols="70"><?php echo @$results[$contenttype->catid]->metadescription; ?></textarea>
														</td>
													</tr>
													<tr>
														<td><label>Metadata sleutelwoorden</label></td>
														<td>
															<input type="text" size="80" name="jform[contenttypes][<?php echo $contenttype->catid; ?>][tripcontent_metadata_keywords]" value="<?php echo @$results[$contenttype->catid]->metakeywords; ?>"/>
														</td>
													</tr>
												</tbody>
											</table>
								   	  </div>
							   	  </div>
		   					<?php
		   				}
		   			}
		   		?>
		   	</div>
	   </div>
	   <div class="clear"></div>
      <input type="hidden" name="task" value="trip.edit" />
      <?php echo JHtml::_('form.token'); ?>
   </div>
</form>
-->
