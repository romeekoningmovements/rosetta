<?php
	defined('_JEXEC') or die('Restricted access');

	$product_sku = JRequest::getVar('product_sku');
	$fieldname   = JRequest::getVar('fieldname');
	$task 		 = JRequest::getCmd('task');
		
	require_once(JPATH_COMPONENT.DS.'helpers'.DS.'shop.php');

	//Retrieve file details from uploaded file, sent from upload form
	$file = JRequest::getVar('url', null, 'files', 'array');
	
	//Import filesystem libraries. Perhaps not necessary, but does not hurt
	jimport('joomla.filesystem.file');
	 
	//Clean up filename to get rid of strange characters like spaces etc
	$filename = JFile::makeSafe($file['name'][0]);
	 
	//Set up the source and destination of the file
	$src = $file['tmp_name'][0];

	$dest = ShopHelper::imagePath($product_sku, $fieldname, $task);
	//print "src: ".$src."<br/>";
	//print "dest: ".$dest;
	//exit;
	//if(!JFolder::exists()){
	//	JFolder::create($dest, $mode);
	//}
	//First check if the file has the right extension, we need jpg only
	if ( strtolower(JFile::getExt($filename) ) == 'jpg' || strtolower(JFile::getExt($filename) ) == 'png' || strtolower(JFile::getExt($filename) ) == 'gif') {
	   if ( JFile::upload($src, $dest) ) {
	   		echo str_replace(JPATH_SITE.DS,'', $dest);
	   		exit;
	      //Redirect to a page of your choice
	   } 
	   else {
	      //Redirect and throw an error message
	   }
	} 
	else {
	   //Redirect and notify user file is not right extension
	}

?>