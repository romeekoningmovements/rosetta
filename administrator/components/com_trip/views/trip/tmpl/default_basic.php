<?php
	defined('_JEXEC') or die('Restricted access');
	$product = $this->product;
?>

<fieldset>
	<legend><?php echo JText::_('BASIC_INFORMATION'); ?></legend>
	<table style="float:left;">
		<tr>
			<td><?php echo JText::_('ID'); ?></td>
			<td>
				124422
			</td>
		</tr>
		<tr>
			<td><?php echo JText::_('NAME'); ?></td>
			<td>
				<input type="text" name="product_title" value="<?php echo $product->product_title; ?>"/>
			</td>
			<td><?php echo JText::_('SKU'); ?></td>
			<td>
				<input type="text" name="product_sku" value="<?php echo $product->product_sku; ?>"/>
			</td>
		</tr>
		<tr>
			<td><?php echo JText::_('ALIAS'); ?></td>
			<td>
				<input type="text" name="product_alias" value="<?php echo $product->product_alias; ?>"/>
			</td>
			<td><?php echo JText::_('ACTIVATED'); ?></td>
			<td>
				<input type="radio" id="product_status_id0" name="product_status_id" value="0"/>
				<label for="product_status_id0"><?php echo JText::_('NO'); ?></label>
				<input type="radio" id="product_status_id1" name="product_status_id" value="1"/>
				<label for="product_status_id1"><?php echo JText::_('YES'); ?></label>
			</td>
		</tr>
	</table>
</fieldset>

<fieldset>
	<legend><?php echo JText::_('DESCRIPTION'); ?></legend>
	<table style="width:100%;">
		<tbody>
			<tr>
				<td><?php echo JText::_('SHORT_DESCRIPTION'); ?></td>
				<td>
					<?php $editor = &JFactory::getEditor(); ?>
					<?php echo $editor->display( 'product_description_short',  @$product->product_description_short, '100%', '300', '75', '20' ) ; ?>
				</td>
			</tr>
			<tr>
				<td><?php echo JText::_('LONG_DESCRIPTION'); ?></td>
				<td>
					<?php $editor = &JFactory::getEditor(); ?>
					<?php echo $editor->display( 'product_description_long',  @$product->product_description_long, '100%', '300', '75', '20' ) ; ?>
				</td>
			</tr>
		</tbody>
	</table>
</fieldset>