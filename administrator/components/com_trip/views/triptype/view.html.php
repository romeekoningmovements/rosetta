<?php

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');
JHTML::_('behavior.framework');

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidator');

JFactory::getDocument()->addScriptDeclaration('
	Joomla.submitbutton = function(task)
	{
		if (task == "triptype.cancel" || document.formvalidator.isValid(document.getElementById("triptype-form")))
		{
			Joomla.submitform(task, document.getElementById("triptype-form"));
		}
	};
');

class TripViewTriptype extends JViewLegacy
{
	public function display($tpl = null)
	{
		$form   = $this->get('Form');
		$item   = $this->get('Item');
 		
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Assign the Data
		$this->form = $form;
		$this->item = $item;
 
		// Set the toolbar
		$this->addToolBar();
 
		// Display the template
		parent::display($tpl);
		
		// Set the document
		$this->setDocument();
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar() 
	{
		$input = JFactory::getApplication()->input;
		$input->set('hidemainmenu', true);
		$user = JFactory::getUser();
		$userId = $user->id;
		$isNew = $this->item->id == 0;
	
		$canDo = TripHelper::getActions($this->item->id);
		$this->canDo = $canDo;
		JToolBarHelper::title($isNew ? JText::_('COM_TRIP_MANAGER_TRIPTYPE_NEW') : JText::_('COM_TRIP_MANAGER_TRIPTYPE_EDIT').': <small><small>[  '.$this->item->triptype.'  ]</small></small>', 'icon-com_trip');
		// Built the actions for new and existing records.
		if ($isNew) 
		{
			// For new records, check the create permission.
			if ($canDo->get('core.create')) 
			{
				JToolBarHelper::apply('triptype.apply', 'JTOOLBAR_APPLY');
				JToolBarHelper::save('triptype.save', 'JTOOLBAR_SAVE');
				JToolBarHelper::custom('triptype.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
			}
			JToolBarHelper::cancel('triptype.cancel', 'JTOOLBAR_CANCEL');
		}
		else
		{

			if ($canDo->get('core.edit'))
			{
				// We can save the new record
				JToolBarHelper::apply('triptype.apply', 'JTOOLBAR_APPLY');
				JToolBarHelper::save('triptype.save', 'JTOOLBAR_SAVE');
 
				// We can save this record, but check the create permission to see
				// if we can return to make a new one.
				if ($canDo->get('core.create')) 
				{
					JToolBarHelper::custom('triptype.save2new', 'save-new.png', 'save-new_f2.png',
					                       'JTOOLBAR_SAVE_AND_NEW', false);
				}
			}
			if ($canDo->get('core.create')) 
			{
				//JToolBarHelper::custom('product.save2copy', 'save-copy.png', 'save-copy_f2.png', 'JTOOLBAR_SAVE_AS_COPY', false);
			}
			JToolBarHelper::cancel('triptype.cancel', 'JTOOLBAR_CLOSE');
		}
	}
	
	protected function setDocument() 
	{
		$isNew = $this->item->id == 0;
		$document = JFactory::getDocument();
		$document->setTitle($isNew ? JText::_('COM_TRIP_TRIPTYPE_CREATING')
		                           : JText::_('COM_TRIP_TRIPTYPE_EDITING'));
		$document->addScript(JURI::root() . "/administrator/components/com_trip"
		                                  . "/views/triptype/submitbutton.js");
		JText::script('COM_TRIP_TRIPTYPE_ERROR_UNACCEPTABLE');
	}
}
