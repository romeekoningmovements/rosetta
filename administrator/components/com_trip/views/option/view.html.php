<?php

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');
JHTML::_('behavior.framework');

class TripViewOption extends JViewLegacy
{
	public function display($tpl = null)
	{
		$form   = $this->get('Form');
		$item   = $this->get('Item');
		$script = $this->get('Script');
		$contentypes = $this->get('Contenttypes');
 		
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Assign the Data
		$this->form = $form;
		$this->item = $item;
		$this->script = $script;
		$this->contenttypes = $contentypes;
 
		// Set the toolbar
		$this->addToolBar();
		
		// Display the template
		parent::display($tpl);
		
		// Set the document
		$this->setDocument();
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar() 
	{
		$input = JFactory::getApplication()->input;
		$input->set('hidemainmenu', true);
		$user = JFactory::getUser();
		$userId = $user->id;
		$isNew = $this->item->id == 0;

		$canDo = TripHelper::getActions($this->item->id);
		//TripHelper::addSubmenu();
		$this->canDo = $canDo;


		JToolBarHelper::title($isNew ? JText::_('COM_SHOP_MANAGER_OPTION_NEW') : JText::_('COM_SHOP_MANAGER_OPTION_EDIT').': <small><small>[  Opties  ]</small></small>', 'icon-icon-48-icon-com-trip');
		// Built the actions for new and existing records.
		if ($isNew) 
		{
			// For new records, check the create permission.
			if ($canDo->get('core.create')) 
			{
				JToolBarHelper::apply('option.apply', 'JTOOLBAR_APPLY');
				JToolBarHelper::save('option.save', 'JTOOLBAR_SAVE');
				JToolBarHelper::custom('option.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
			}
			JToolBarHelper::cancel('option.cancel', 'JTOOLBAR_CANCEL');
		}
		else
		{

			if ($canDo->get('core.edit'))
			{
				// We can save the new record
				JToolBarHelper::apply('option.apply', 'JTOOLBAR_APPLY');
				JToolBarHelper::save('option.save', 'JTOOLBAR_SAVE');
 
				// We can save this record, but check the create permission to see
				// if we can return to make a new one.
				if ($canDo->get('core.create')) 
				{
					JToolBarHelper::custom('option.save2new', 'save-new.png', 'save-new_f2.png',
					                       'JTOOLBAR_SAVE_AND_NEW', false);
				}
			}
			if ($canDo->get('core.create')) 
			{
				//JToolBarHelper::custom('product.save2copy', 'save-copy.png', 'save-copy_f2.png', 'JTOOLBAR_SAVE_AS_COPY', false);
			}
			JToolBarHelper::cancel('option.cancel', 'JTOOLBAR_CLOSE');
		}
	}
	
	protected function setDocument() 
	{
		$isNew = $this->item->trip_id == 0;
		$document = JFactory::getDocument();
		$document->setTitle($isNew ? JText::_('COM_SHOP_OPTION_CREATING')
		                           : JText::_('COM_SHOP_OPTION_EDITING'));
		$document->addScript(JURI::root() . $this->script);
		$document->addScript(JURI::root() . "/administrator/components/com_shop"
		                                  . "/views/product/submitbutton.js");
		JText::script('COM_SHOP_TRIP_ERROR_UNACCEPTABLE');
	}
}
