<?php

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class TripViewTripcategories extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;
	
	public function display($tpl = null)
	{
	    $this->state		= $this->get('State');
		$this->items		= $this->get('Data');
		$this->pagination	= $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		
		parent::display($tpl);
		
		// Set the document
		$this->setDocument();
	}

	protected function addToolbar()
	{
		$canDo = TripHelper::getActions();

		JToolBarHelper::title(JText::_('COM_TRIP_MANAGER_CATEGORIES').': <small><small>[  categorie&euml;n  ]</small></small>', 'icon-com-trip');
		if ($canDo->get('core.create')) 
		{
			JToolBarHelper::addNew('tripcategory.add', 'JTOOLBAR_NEW');
		}
		if ($canDo->get('core.edit')) 
		{
			JToolBarHelper::editList('tripcategory.edit', 'JTOOLBAR_EDIT');
		}
		if ($canDo->get('core.delete')) 
		{
			JToolBarHelper::deleteList('', 'CATEGORIES.delete', 'JTOOLBAR_DELETE');
		}
	}
	
	protected function setDocument() 
	{
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_TRIP_ADMINISTRATION'));
	}
}
