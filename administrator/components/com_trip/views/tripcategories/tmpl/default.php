
<?php


// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');

$user		= JFactory::getUser();
$userId		= $user->get('id');

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$canOrder	= $user->authorise('core.edit.state', 'com_trip.category');
$saveOrder	= $listOrder == 'a.ordering';
?>

<form action="<?php echo JRoute::_('index.php?option=com_trip&view=tripcategories'); ?>" method="post" name="adminForm" id="adminForm">
	<fieldset id="filter-bar">
		<div class="filter-search fltlft">
			<label class="filter-search-lbl" for="filter_search"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?></label>
			<input type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo JText::_('COM_WEBLINKS_SEARCH_IN_TITLE'); ?>" />
			<button type="submit"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
			<button type="button" onclick="document.id('filter_search').value='';this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
		</div>
	</fieldset>
	<div class="clr"> </div>
	
	<table class="adminlist" style="margin-top: 30px;">
		<thead>
			<tr>
				<td colspan="10">
					<?php
						//print "<pre>";
						//print_R($this->pagination);
						//print "</pre>";
					?>
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
			<tr>
				<th width="25">
					<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
				</th>
				<th width="40" style="text-align:center;" class="nowrap">
					<?php echo JHtml::_('grid.sort',  'JGRID_HEADING_ID', 'id', $listDirn, $listOrder); ?>
				</th>
				<th width="" style="text-align:left;">
					<?php echo JHtml::_('grid.sort',  'JTITLE', 'title', $listDirn, $listOrder); ?>
				</th>
			</tr>
		</thead>
		<tbody>
		<?php 
			if($this->items){
				require_once(JPATH_COMPONENT.DS.'helpers'.DS.'trip.php');
				foreach ($this->items as $i => $item) :
				$ordering	= ($listOrder == 'a.ordering');
								?>
				<tr class="row<?php echo $i % 2; ?>">
					<td class="center" style="text-align:left;">
						<?php echo JHtml::_('grid.id', $i, $item->id); ?>
					</td>
					<td style="text-align:center;">
						<?php echo (int) $item->id; ?>
					</td>
					<td>
						<a href="<?php echo JRoute::_('index.php?option=com_trip&task=tripcategory.edit&id='.(int) $item->id); ?>"><?php echo $this->escape($item->title); ?></a>
					</td>
				</tr>
				<?php endforeach; ?>
				<?php 
			}
			else{
				?>
					<tr>
						<td colspan="7"><?php echo JText::_('NO_TRIPTYPES'); ?></td>
					</tr>
				<?php
			}
				?>
		</tbody>
	</table>

	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
