CREATE TABLE IF NOT EXISTS `#__shop_categories` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__shop_coupons` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_description` varchar(255) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `coupon_value` decimal(6,2) DEFAULT NULL,
  `coupon_percentage` decimal(5,2) DEFAULT NULL,
  `enabled` smallint(6) DEFAULT NULL,
  `max_usage` int(11) DEFAULT NULL,
  `usage` int(11) DEFAULT NULL,
  PRIMARY KEY (`coupon_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__shop_orderitems` (
  `orderitem_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`orderitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__shop_orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `order_value` decimal(6,2) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__shop_products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `product_sku` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `product_title` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `product_alias` varchar(100) COLLATE utf8_bin NOT NULL,
  `product_listprice` decimal(10,5) DEFAULT NULL,
  `product_price` decimal(10,5) DEFAULT NULL,
  `product_type` smallint(6) DEFAULT NULL,
  `product_image` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `product_description_short` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `product_description_long` text COLLATE utf8_bin,
  `product_parameters` text COLLATE utf8_bin,
  `product_enabled` smallint(6) NOT NULL,
  `product_status_id` smallint(6) NOT NULL,
  `product_availability_date` date NOT NULL,
  `product_taxclass_id` smallint(6) NOT NULL,
  PRIMARY KEY (`product_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `fk_category` FOREIGN KEY (`category_id`) REFERENCES `jos_shop_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4215 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE IF NOT EXISTS `#__shop_searchmirror` (
  `key` varchar(50) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__shop_shippingrates` (
  `shippingrate_id` int(11) NOT NULL AUTO_INCREMENT,
  `shippingrate_description` varchar(255) DEFAULT NULL,
  `shippingrate` decimal(5,2) DEFAULT NULL,
  PRIMARY KEY (`shippingrate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__shop_suppliers` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__shop_taxclasses` (
  `taxclass_id` int(11) NOT NULL AUTO_INCREMENT,
  `taxclass_description` varchar(50) DEFAULT NULL,
  `percentage` decimal(4,2) DEFAULT NULL,
  `price_is_tax_included` smallint(6) DEFAULT NULL,
  `enabled` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`taxclass_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;


