<?php
/**
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of weblink records.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_weblinks
 * @since		1.6
 */
class TripModelDestinations extends JModelLegacy
{
	var $_total = null;
	var $_pagination = null;
	/**
	 * Constructor.
	 *
	 * @param	array	An optional associative array of configuration settings.
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
		
		$mainframe = JFactory::getApplication();
 
		// Get pagination request variables
		$limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		// limit all is not allowed, messes up big databases
		if(!$limit){
			$limit = 100;
		}

		$limitstart = JRequest::getVar('limitstart', 0, '', 'int');
	 
		// In case limit has been changed, adjust it
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
	 
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
		$this->setState('filter.state', JRequest::getVar('filter_state'));	
		$this->setState('filter.category', JRequest::getVar('filter_category'));	
		$this->setState('list.ordering', JRequest::getVar('filter_order'));
		$this->setState('list.direction', JRequest::getVar('filter_order_Dir'));		
	}	
	
	function getDestinations(){
		$db =& JFactory::getDBO();
		$query  = "SELECT SQL_CALC_FOUND_ROWS * FROM #__xtrip_destinations d ORDER BY d.title ASC";
		
		
		return $query;
		
	}
	
	function getStatusses(){
		return array(0=>JText::_('DISABLED'), 1=>JText::_('ENABLED'));
	}
	
	function getCategories(){
		$db =& JFactory::getDBO();
		$query = "
			 SELECT 
			 t.id, 
			 t.triptype
			 FROM jos_xtrip_triptypes t
		";
		$db->setQuery($query);
		if($results = $db->loadObjectList()){
			return $results;
		}
		return false;
	}

	function getData() 
    {
	 	// if data hasn't already been obtained, load it
	 	if (empty($this->_data)) {
	 	    $query = $this->getDestinations();
	 	    $this->_data = $this->_getList($query, $this->getState('limitstart'),  $this->getState('limit'));		
	 	}
	 	return $this->_data;
    }
	
	function getTotal()
    {
	 	// Load the content if it doesn't already exist
	 	if (empty($this->_total)) {
	 	    $query =$this->getDestinations();
	 	    $this->_total = $this->_getListCount($query);	
	 	}
	 	return $this->_total;
    }
    
	function getPagination()
    {
    	$db =& JFactory::getDBO();
		$db->setQuery('SELECT FOUND_ROWS();');  //no reloading the query! Just asking for total without limit
		//$pageNav = new JPagination( $db->loadResult(), $this->getState('limitstart'), $this->getState('limit') );
	 	// Load the content if it doesn't already exist
	 	if (empty($this->_pagination)) {
	 	    jimport('joomla.html.pagination');
	 	    $this->_pagination = new JPagination($db->loadResult(), $this->getState('limitstart'), $this->getState('limit') );
	 	}
	 	return $this->_pagination;
    }
}
