<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla modelform library
jimport('joomla.application.component.modeladmin');
 
/**
 * HelloWorld Model
 */
class TripModelDeparture extends JModelAdmin
{
	/**
	 * Method override to check if you can edit an existing record.
	 *
	 * @param	array	$data	An array of input data.
	 * @param	string	$key	The name of the key for the primary key.
	 *
	 * @return	boolean
	 * @since	2.5
	 */
	protected function allowEdit($data = array(), $key = 'id')
	{
		// Check specific edit permission then general edit permission.
		return JFactory::getUser()->authorise('core.edit', 'com_trip.departure.'.
		                                      ((int) isset($data[$key]) ? $data[$key] : 0))
		       or parent::allowEdit($data, $key);
	}
	
    public function deleteDeparture($departure){
    	$row = $this->getTable( 'departure' );
    	$user =& JFactory::getUser();
    	
    	// load the item's data so we'll know with what item were dealing with
		if (!$row->load($departure)) {
		    return JError::raiseWarning( 500, $row->getError() );
		}
		
		// check if the item was checked out, and if it does - whether or not it was checked out by the user
		if($row->isCheckedOut($user->id)) {
		    // check in the item
		    if ($row->checkin()) {
		        echo 'The item was checked in, and can now be edited by other users.';
		    } else {
		        return JError::raiseWarning( 500, $row->getError() );
		    }
		}
		
    	$row->delete( $departure );
    	
		return true;
	}

	public function save(){
	//print "<pre>";
	//print_R($_REQUEST);
	//print "</pre>";
	//exit;
		$user = JFactory::getUser();
		$db =& JFactory::getDBO();
		$ignore = '';
		$from = JRequest::get('post');

		$from = $from['jform'];

		
		$departure = JRequest::getInt('id');


		$row = $this->getTable( 'departure' );
		// load the item's data so we'll know with what item were dealing with
		if (!$row->load($departure)) {
		    return JError::raiseWarning( 500, $row->getError() );
		}
		
		// check if the item was checked out, and if it does - whether or not it was checked out by the user
		if($row->isCheckedOut($user->id)) {
		    // check in the item
		    if ($row->checkin()) {
		        echo 'The item was checked in, and can now be edited by other users.';
		    } else {
		        return JError::raiseWarning( 500, $row->getError() );
		    }
		}

		if (!$row->bind( $from )) {
			return JError::raiseWarning( 500, $row->getError() );
		}
		
		if (!$row->store()) {
			return JError::raiseError(500, $row->getError() );
		}

		$query = "SELECT departure FROM #__xtrip_trips WHERE trip_id=".$db->Quote($from['tripid']);
		$db->setQuery($query);
		$deps = '';
		if($result = $db->loadObject()){		
			//if(!$from['id']){
				if($result->departure){
					$deps = $result->departure.",".$row->id; 
				}
				else{
					$deps = $row->id;
				}
			//}
		}

		$query = "UPDATE #__xtrip_trips SET departure=".$db->Quote($deps)." WHERE trip_id=".$db->Quote($from['tripid']);
		
		$db->setQuery($query);
		$db->Query();
		
		return $row;
		//return true;
	}
	
	public function changeState($departure, $status){
		$user     = JFactory::getUser();
		$ignore = '';
		$from['state'] = $status;

		$row = $this->getTable( 'departure' );
		
		// load the item's data so we'll know with what item were dealing with
		if (!$row->load($departure)) {
		    return JError::raiseWarning( 500, $row->getError() );
		}
	
		// check if the item was checked out, and if it does - whether or not it was checked out by the user
		if($row->isCheckedOut($user->id)) {
		    // check in the item
		    if ($row->checkin()) {
		        echo 'The item was checked in, and can now be edited by other users.';
		    } else {
		        return JError::raiseWarning( 500, $row->getError() );
		    }
		}

		if (!$row->bind( $from )) {
			return JError::raiseWarning( 500, $row->getError() );
		}
		
		if (!$row->store()) {
			return JError::raiseError(500, $row->getError() );
		}
		
	}
	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	2.5
	 */
	public function getTable($type = 'Departure', $prefix = 'DepartureTable', $config = array()) 
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		Data for the form.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	mixed	A JForm object on success, false on failure
	 * @since	2.5
	 */
	public function getForm($data = array(), $loadData = true) 
	{
		// Get the form.
		
		$form = $this->loadForm('com_trip.departure', 'departure',
		                        array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) 
		{
			return false;
		}
		return $form;
	}


	/**
	 * Method to get the script that have to be included on the form
	 *
	 * @return string	Script files
	 */
	public function getScript() 
	{
		return 'administrator/components/com_shop/models/forms/trip.js';
	}
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	2.5
	 */
	protected function loadFormData() 
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_trip.edit.departure.data', array());
		if (empty($data)) 
		{
			$data = $this->getItem();
		}
		return $data;
	}
}
