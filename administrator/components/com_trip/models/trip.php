<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla modelform library
jimport('joomla.application.component.modeladmin');
 
/**
 * HelloWorld Model
 */
class TripModelTrip extends JModelAdmin
{
	/**
	 * Method override to check if you can edit an existing record.
	 *
	 * @param	array	$data	An array of input data.
	 * @param	string	$key	The name of the key for the primary key.
	 *
	 * @return	boolean
	 * @since	2.5
	 */
	protected function allowEdit($data = array(), $key = 'id')
	{
		// Check specific edit permission then general edit permission.
		return JFactory::getUser()->authorise('core.edit', 'com_trip.trip.'.
		                                      ((int) isset($data[$key]) ? $data[$key] : 0))
		       or parent::allowEdit($data, $key);
	}
	
    public function deleteProducts($trip){
    	$row = $this->getTable( 'trip' );
    	$user =& JFactory::getUser();
    	
    	$db =& JFactory::getDBO();
    	// alles wat bij deze reis hoort verwijderen
    	$query = "DELETE FROM jos_xtrip_options WHERE trip_id=".$db->Quote($trip);
    	$db->setQuery($query);
    	$db->Query();
    	
    	$query = "DELETE FROM jos_xtrip_content WHERE trip_id=".$db->Quote($trip);
    	$db->setQuery($query);
    	$db->Query();
    	
    	$query = "DELETE FROM jos_xtrip_departures WHERE tripid=".$db->Quote($trip);
    	$db->setQuery($query);
    	$db->Query();
    	
    	// load the item's data so we'll know with what item were dealing with
		if (!$row->load($trip)) {
		    return JError::raiseWarning( 500, $row->getError() );
		}
		
		// check if the item was checked out, and if it does - whether or not it was checked out by the user
		if($row->isCheckedOut($user->id)) {
		    // check in the item
		    if ($row->checkin()) {
		        echo 'The item was checked in, and can now be edited by other users.';
		    } else {
		        return JError::raiseWarning( 500, $row->getError() );
		    }
		}
		
    	$row->delete( $trip );
    	
		return true;
	}
	
	public function save(){
	//print "<pre>";
	//print_R($_REQUEST);
	//print "</pre>";
	//exit;
		$user = JFactory::getUser();
		$db =& JFactory::getDBO();
		$ignore = '';
		$from = JRequest::get('post');
		$from = $from['jform'];
		
		$trip = JRequest::getInt('id');
		if(!$trip){
			$from['created_date'] = date('Y-m-d H:i:s');
		}
		
		if(isset($from['imagethumb'])){
			$split = explode('/', $from['imagethumb']);
			$from['imagethumb'] = $split[count($split)-1];
		}
		
		if(isset($from['route'])){
			$split = explode('/', $from['route']);
			$from['route'] = $split[count($split)-1];
		}
		
	
		

		$row = $this->getTable( 'trip' );
		// load the item's data so we'll know with what item were dealing with
		if (!$row->load($trip)) {
		    return JError::raiseWarning( 500, $row->getError() );
		}
		
		// check if the item was checked out, and if it does - whether or not it was checked out by the user
		if($row->isCheckedOut($user->id)) {
		    // check in the item
		    if ($row->checkin()) {
		        echo 'The item was checked in, and can now be edited by other users.';
		    } else {
		        return JError::raiseWarning( 500, $row->getError() );
		    }
		}

		if (!$row->bind( $from )) {
			return JError::raiseWarning( 500, $row->getError() );
		}
		
		if (!$row->store()) {
			return JError::raiseError(500, $row->getError() );
		}

		if($from['contenttypes']){
			foreach($from['contenttypes'] as $contenttype){
				$query = "SELECT id FROM #__xtrip_content WHERE cat_id=".$db->Quote($contenttype['tripcontent_category'])." AND trip_id=".$db->Quote($row->trip_id);
				$db->setQuery($query);
				if($result = $db->loadObject()){
					// bestaat, dan updaten
					$query  = "UPDATE #__xtrip_content SET metadescription=".$db->Quote($contenttype['tripcontent_metadata_desc']).", metakeywords=".$db->Quote($contenttype['tripcontent_metadata_keywords']).", ";
					$query .= "introtext=".$db->Quote($contenttype['tripcontent_introtext']).", ";
					$query .= "state=1, publish_up='', publish_down='', image='' WHERE id=".$db->Quote($result->id);

					$db->setQuery($query);
					$db->Query();
				}
				else{
					// bestaat niet, aanmaken.
					$query  = "INSERT INTO #__xtrip_content (trip_id,cat_id, metadescription, metakeywords,  introtext, created, created_by, state, publish_up, publish_down, image) ";
					$query .=" VALUES (".$db->Quote($row->trip_id).",".$db->Quote($contenttype['tripcontent_category']).", ".$db->Quote($contenttype['tripcontent_metadata_desc']).",";
					$query .= $db->Quote($contenttype['tripcontent_metadata_keywords']).",".$db->Quote($contenttype['tripcontent_introtext']).",";
					$query .= $db->Quote(date('Y-m-d H:i:s')).",".$db->Quote($user->id).",1,'','',''";
					$query .= ")";

					$db->setQuery($query);
					$db->Query();
				}
			}
		}
		return $row;
		//return true;
	}
	
	public function changeState($trip, $status){
		$user     = JFactory::getUser();
		$ignore = '';
		$from['state'] = $status;

		$row = $this->getTable( 'trip' );
		
		// load the item's data so we'll know with what item were dealing with
		if (!$row->load($trip)) {
		    return JError::raiseWarning( 500, $row->getError() );
		}
	
		// check if the item was checked out, and if it does - whether or not it was checked out by the user
		if($row->isCheckedOut($user->id)) {
		    // check in the item
		    if ($row->checkin()) {
		        echo 'The item was checked in, and can now be edited by other users.';
		    } else {
		        return JError::raiseWarning( 500, $row->getError() );
		    }
		}

		if (!$row->bind( $from )) {
			return JError::raiseWarning( 500, $row->getError() );
		}
		
		if (!$row->store()) {
			return JError::raiseError(500, $row->getError() );
		}
		
	}
	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	2.5
	 */
	public function getTable($type = 'Trip', $prefix = 'TripTable', $config = array()) 
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		Data for the form.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	mixed	A JForm object on success, false on failure
	 * @since	2.5
	 */
	public function getForm($data = array(), $loadData = true) 
	{
		// Get the form.
		$form = $this->loadForm('com_shop.trip', 'trip',
		                        array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) 
		{
			return false;
		}
		return $form;
	}


	/**
	 * Method to get the script that have to be included on the form
	 *
	 * @return string	Script files
	 */
	public function getScript() 
	{
		return 'administrator/components/com_shop/models/forms/trip.js';
	}
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	2.5
	 */
	protected function loadFormData() 
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_shop.edit.trip.data', array());
		if (empty($data)) 
		{
			$data = $this->getItem();
		}
		return $data;
	}
}
