<?php

/**
 * @package		Joomla.Tutorials
 * @subpackage	Component
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters, Inc. All rights reserved.
 * @license		License GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

jimport('joomla.form.helper');

JFormHelper::loadFieldClass('list');

class JFormFieldOptions extends JFormField
{
	protected $type = 'Options';

 
	public function getInput() {
		$db =& JFactory::getDBO();
		//print "<pre>";
		//print_R($this);
		//print "</pre>";
		$html = '<br/><br/><span style="color:red;">Sla de reis aub eerst op om een reisoptie toe te kunnen voegen.</span>';

		if(JRequest::getInt('trip_id')){
			$query = "SELECT * FROM #__xtrip_options WHERE trip_id IN (".JRequest::getInt('trip_id').")";
			$db->setQuery($query);
			$html = '';
	
				if($options = $db->loadObjectList()){		
					$html = '
					<table class="table" width="100%" style="float:left; border-color:#CCC; border-width: 0 0 1px 1px; border-spacing: 0; border-collapse: collapse; border-style: solid;" border="1" cellpadding="5" cellspacing="0">
						<thead>
							<tr>
								<th colspan="2">Omschrijving</th>
								<th></th>
							</tr>
						</thead>
						<tbody>';
							foreach($options as $option){	
								$html .= '
									<tr>
										<td>'.$option->description.'</td>
										<td><a href="index.php?option=com_trip&view=option&layout=edit&id='.$option->id.'&trip_id='.$option->trip_id.'">edit</a></td>
										<td><a href="index.php?option=com_trip&view=option&id='.$option->id.'&trip_id='.$option->trip_id.'&task=option.delete">verwijder</a></td>
									</tr>';
							}
						$html .= '
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3">
									<div style="clear:both;"></div><div class="button2-left"><div class="blank"><a href="index.php?option=com_trip&view=option&layout=edit&trip_id='.JRequest::getInt('trip_id').'">Optie toevoegen</a></div></div>
								</td>
							</tr>
						</tfoot>
					</table>';
					
					return $html;
				}
			
			
			$html = '<div style="clear:both;"></div><div class="button2-left"><div class="blank"><a href="index.php?option=com_trip&view=option&layout=edit&trip_id='.JRequest::getInt('trip_id').'">Optie toevoegen</a></div></div>';
		}
		return $html;
		
	}
}
