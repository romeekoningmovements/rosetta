<?php

/**
 * @package		Joomla.Tutorials
 * @subpackage	Component
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters, Inc. All rights reserved.
 * @license		License GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

jimport('joomla.form.helper');

JFormHelper::loadFieldClass('list');

class JFormFieldOrderitems extends JFormField
{
	protected $type = 'Orderitems';

 
	public function getInput() {
		$db =& JFactory::getDBO();
		$query = "SELECT *, (SELECT value FROM #__shop_configuration_fields WHERE id=36) as prefix FROM #__shop_orders o ";
		$query .= "LEFT JOIN #__shop_coupons c ON c.coupon_id=o.coupon_id WHERE order_id=".$db->Quote($this->form->getValue('order_id'));
		$db->setQuery($query);
		$order = $db->loadObject();
		$html = "<table class='adminlist' width='100%'>
					<thead>
						<tr>
							<th>Artikel</th>
							<th>Aantal</th>
							<th>Totaal</th>
						</tr>
					</thead>
					<tbody>
		";
		
		$query  = "SELECT * FROM #__shop_orderitems o INNER JOIN #__shop_products p ON p.product_id=o.product_id ";
		$query .= "WHERE o.order_id=".$db->Quote($this->form->getValue('order_id'));
		//print $query;
		$db->setQuery($query);
		if($products = $db->loadObjectList()){
			foreach($products as $product){
				$html .= "<tr>";
				$html .= "<td>".$product->product_title."<br/><strong>SKU:</strong> ".$product->product_sku."<br/><strong>Prijs:</strong> ".$order->prefix.number_format($product->product_price,2 ,'.',',')."</td>";
				$html .= "<td class='center' style='vertical-align:middle;'>".$product->amount."</td>";
				$html .= "<td style='vertical-align:middle;'>".$order->prefix.(number_format($product->product_price * $product->amount, 2,'.',','))."</td>";
				$html .= "</tr>";
			}
		}
		
		$discount = '';
		if($order->coupon_value){
			$discount = number_format($order->coupon_value,2,'.',',');
		}
		if($order->coupon_percentage){
			$discount = number_format(($order->coupon_percentage / 100 * $order->order_value), 2, '.',',');
		}

		$html .= "  </tbody>
					<tfoot>					
						<tr>
							<td></td>
							<td><strong>Subtotaal:</strong></td>
							<td>".$order->prefix.number_format($order->order_value + $discount, 2,'.',',')."</td>
						</tr>
						<tr>
							<td></td>
							<td><strong>Korting:</strong></td>
							<td>".$order->prefix.$discount."</td>
						</tr>
						<tr>
							<td></td>
							<td><strong>Totaal:</strong></td>
							<td>".$order->prefix.number_format($order->order_value, 2,'.',',')."</td>
						</tr>
					</tfoot>
		         </table>";
		return $html;
	}
}
