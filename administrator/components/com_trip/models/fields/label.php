<?php

/**
 * @package		Joomla.Tutorials
 * @subpackage	Component
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters, Inc. All rights reserved.
 * @license		License GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

jimport('joomla.form.helper');

JFormHelper::loadFieldClass('list');

class JFormFieldLabel extends JFormField
{
	protected $type = 'Label';

 
	public function getInput() {
		$params = ShopHelper::getConfiguration();
		
		return "<label>".$this->value."</label>";
	}
}
