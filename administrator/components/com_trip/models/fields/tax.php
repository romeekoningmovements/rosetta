<?php

/**
 * @package		Joomla.Tutorials
 * @subpackage	Component
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters, Inc. All rights reserved.
 * @license		License GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

jimport('joomla.form.helper');

JFormHelper::loadFieldClass('list');

class JFormFieldTax extends JFormField
{
	protected $type = 'Tax';

 
	public function getInput() {
		$params = ShopHelper::getConfiguration();

		// get the configuration parameters
		$decimalseparator   = $params['config_currency_decimal_separator']->value;
		$thousandseparator  = $params['config_currency_thousands_separator']->value;
		$numberdecimals     = $params['config_currency_number_of_decimals']->value;
		$currencyprefix	    = $params['config_currency_prefix']->value;
		$selected_ajaxtax   = JRequest::getInt('taxclass_id');
		
		$noajax = true;
		$selected_ajaxprice = JRequest::getVar('requested_price');
		$requestedtax       = '';
		$requestedtaxtype   = 0;       
		$requestedprice 	= JRequest::getVar('requested_price',0);

		// gather the tax information and create selectfield.
		$db =& JFactory::getDBO();
		$query = "SELECT * FROM #__shop_taxclasses WHERE enabled=1";
		$db->setQuery($query);
		if($results = $db->loadObjectList()){
			foreach($results as $taxclass){
				if($selected_ajaxtax == $taxclass->taxclass_id){
					$noajax = false;
					$requestedtax = $taxclass->percentage;
					$requestedtaxtype = $taxclass->price_is_tax_included;
				}
				$options[] = JHtml::_('select.option', $taxclass->taxclass_id, $taxclass->taxclass_description);
			}
			
			$query = "SELECT p.product_price, t.price_is_tax_included, t.percentage FROM #__shop_products p INNER JOIN #__shop_taxclasses t ON t.taxclass_id=p.product_taxclass_id WHERE p.product_id=".$db->Quote(JRequest::getInt('product_id'));
			$db->setQuery($query);
			$obj = $db->loadObject();

			$taxtype 	   = !$requestedtaxtype && !$noajax 	? (!(isset($obj->price_is_tax_included) && !$obj->price_is_tax_included) ? 0 : $obj->price_is_tax_included)	: ((isset($obj->price_is_tax_included) && $obj->price_is_tax_included) ? $obj->price_is_tax_included : $requestedtaxtype);
			$taxclass 	   = !$requestedtax     				? (!isset($obj->percentage) ? 0 : $obj->percentage)  														: $requestedtax;
			$price         = !$requestedprice   				? (!isset($obj->product_price) ? 0 : $obj->product_price)													: $requestedprice;
			$price_taxtype = !$taxtype 							? ($price)															  										: ($price - (($taxclass / (100 + $taxclass)) * $price));
			$price_btw 	   = !$taxtype 		    				? (($taxclass / 100) * $price) 						  														: (($taxclass / (100 + $taxclass)) * $price);
			$price_shop    = !$taxtype 		    				? ((100 + $taxclass) / 100 * $price)	  																	: ($price);	
			
			$html = "
				<div id='price_box' style='float:left;'>
					".JHTML::_('select.genericlist', $options, "jform[".$this->fieldname."]", 'onchange="calculatePrices()" class="inputbox"', 'value', 'text', JRequest::getInt('taxclass_id',$this->value))."
				</div>
				<div id='calculationbox'>
					<table class='com_shop-table'>
						<tbody>
							<tr>
								<td class='com_shop-table-td-text'>".JText::_('COM_SHOP_PRICE_BASED_BTW')."&nbsp;&nbsp;</td>
								<td>".$currencyprefix."</td>
								<td class='com_shop-table-td-value'>".number_format(!isset($price_taxtype) ? 0 : $price_taxtype, $numberdecimals, $decimalseparator, $thousandseparator)."</td>
							</tr>
							<tr>
								<td class='com_shop-table-td-text'>".JText::_('COM_SHOP_BTW')."&nbsp;&nbsp;</td>
								<td>".$currencyprefix."</td>
								<td class='com_shop-table-td-value' style='border-bottom:1px dashed #000;'>".number_format(!isset($price_btw) ? 0 : $price_btw, $numberdecimals, $decimalseparator, $thousandseparator)."</td>
							</tr>
							<tr>
								<td class='com_shop-table-td-text'>".JText::_('COM_SHOP_PRICE_ON_SHOP')."&nbsp;&nbsp;</td>
								<td>".$currencyprefix."</td>
								<td class='com_shop-table-td-value'>".number_format(!isset($price_shop) ? 0 : $price_shop, $numberdecimals, $decimalseparator, $thousandseparator)."</td>
							</tr>
						</tbody>
					</table>
				</div>		
			";
			return $html;
		}
		return false;
	}
}
