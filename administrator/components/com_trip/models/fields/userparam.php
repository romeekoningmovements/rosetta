<?php

/**
 * @package		Joomla.Tutorials
 * @subpackage	Component
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters, Inc. All rights reserved.
 * @license		License GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

jimport('joomla.form.helper');
jimport('joomla.application.component.modeladmin');

JFormHelper::loadFieldClass('list');

class JFormFieldUserparam extends JFormField
{
	protected $type = 'Userparam';
	//protected $user = JFactory::getUser(JRequest::getInt('id'));
 
	public function getInput() {
		$value = '';
		$db =& JFactory::getDBO();
		$query = "SELECT params FROM #__users WHERE id=".$db->Quote(JRequest::getInt('id'));
		$db->setQuery($query);
		if($value = $db->loadObject()){
			$params   = new JParameter($value->params);
		}

		if(isset($params)){
			$value = $params->get($this->fieldname) ? $params->get($this->fieldname) : '';
		}
		
		return "<input type='text' name='jform[".$this->fieldname."]' value='".$value."'/>";
	}
}
