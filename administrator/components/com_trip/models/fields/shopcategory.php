<?php

/**
 * @package		Joomla.Tutorials
 * @subpackage	Component
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters, Inc. All rights reserved.
 * @license		License GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

jimport('joomla.form.helper');

JFormHelper::loadFieldClass('list');

class JFormFieldShopcategory extends JFormFieldList
{
	protected $type = 'Product';


	public function getInput() {
		$db =& JFactory::getDBO();
		$query = "SELECT * FROM #__shop_categories WHERE enabled=1 ORDER BY category ASC";
		$db->setQuery($query);
		if($results = $db->loadObjectList()){
			$html = "<select multiple='multiple' name='jform[".$this->fieldname."]' style='height:350px;'>";
			foreach($results as $result){
				$html .= "<option value='".$result->category_id."'";
				if($result->category_id == $this->value){
					$html .= " selected='selected' ";
				}
				$html .= ">".$result->category."</option>";
			}
			$html.= "</select>";
		}
	

		return $html;
	}
}
