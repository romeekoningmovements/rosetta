<?php

/**
 * @package		Joomla.Tutorials
 * @subpackage	Component
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters, Inc. All rights reserved.
 * @license		License GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file

defined('_JEXEC') or die;

jimport('joomla.form.helper');

JFormHelper::loadFieldClass('list');

class JFormFieldDepartures extends JFormField
{
	protected $type = 'Departures';

 
	public function getInput() {
		$db =& JFactory::getDBO();
		//print "<pre>";
		//print_R($this);
		//print "</pre>";
		$html = '<br/><br/><br/><br/><span style="color:red;">Sla de reis aub eerst op om vertrekdata toe te kunnen voegen</span>';
		if(JRequest::getInt('trip_id')){
			$query = "SELECT * FROM #__xtrip_departures WHERE tripid IN (".JRequest::getInt('trip_id').")";
			$db->setQuery($query);
			
			if(!$this->form->getValue('willekeurige_vertrek_datum')){
				if($departures = $db->loadObjectList()){
					//if($departures = $db->loadObjectList()){
						$html = '
						<table class="table" width="100%" style="float:left; border-color:#CCC; border-width: 0 0 1px 1px; border-spacing: 0; border-collapse: collapse; border-style: solid;" border="1" cellpadding="5" cellspacing="0">
							<thead>
								<tr>
									<th>Vertreklocatie heenreis</th>
									<th>Aankomstlocatie heenreis</th>
									<th>Vertreklocatie terugreis</th>
									<th>Aankomstlocatie terugreis</th>
									<th>Beschikbaarheid</th>
									<th>Beschikbaar</th>
									<th>Gaat definitief door?</th>
									<th>Vertrek</th>
									<th>Terugkomst</th>
									<th colspan="3">Reissom</th>
									
								</tr>
							</thead>
							<tbody>';
								foreach($departures as $departure){
									$beschikbaar = 'Ja';
									if(!$departure->beschikbaar){
										$beschikbaar = 'Nee';
									}
									$html .= '
										<tr>
											<td>'.$departure->vertreklocatie_heenreis.'</td>
											<td>'.$departure->aankomstlocatie_heenreis.'</td>
											<td>'.$departure->vertreklocatie_terugreis.'</td>
											<td>'.$departure->aankomstlocatie_terugreis.'</td>
											<td>'.$departure->beschikbaarheid.'</td>
											<td>'.$beschikbaar.'</td>';
									if($departure->gaatdoor == 1){
										$html .= '<td>Ja</td>';
									}
									else{
										$html .= '<td>Nog niet zeker</td>';
									}
									$html .='<td>'.$departure->vertrek.'</td>
											 <td>'.$departure->terugkomst.'</td>
											 <td>'.$departure->reissom.'</td>
											 <td><a href="index.php?option=com_trip&view=departure&layout=edit&id='.$departure->id.'&trip_id='.$departure->tripid.'">edit</a></td>
											 <td><a href="index.php?option=com_trip&view=departure&id='.$departure->id.'&trip_id='.$departure->tripid.'&task=departure.delete">verwijder</a></td>';	
									$html .='</tr>';
								}
							$html .= '
							</tbody>
							<tfoot>
								<tr>
									<td colspan="12">
										<div style="clear:both;"></div><div class="button2-left"><div class="blank"><a href="index.php?option=com_trip&view=departure&layout=edit&trip_id='.JRequest::getInt('trip_id').'">Vertrekdata toevoegen</a></div></div>
									</td>
								</tr>
							</tfoot>
						</table>';
						
						return $html;
					//}
				}
			}
			$html = '<div style="clear:both;"></div><div class="button2-left"><div class="blank"><a href="index.php?option=com_trip&view=departure&layout=edit&trip_id='.JRequest::getInt('trip_id').'">Vertrekdata toevoegen</a></div></div>';
		}
		
		
		
		return $html;
		
	}
}
