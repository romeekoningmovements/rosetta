<?php

/**
 * @package		Joomla.Tutorials
 * @subpackage	Component
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters, Inc. All rights reserved.
 * @license		License GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

jimport('joomla.form.helper');

JFormHelper::loadFieldClass('list');

class JFormFieldDate extends JFormField
{
	protected $type = 'Date';

 
	public function getInput() {
		jimport( 'joomla.utilities.xmlelement' );
		$language =& JFactory::getLanguage();
		// language locale ophalen.
		$locale = $language->getMetadata($language->getTag());
		
		// standaard de taal van de gebruiker als format gebruiken.
		setlocale(LC_ALL, explode(',',$locale['locale']));
		$params = ShopHelper::getConfiguration();

		return "<label>".strftime($this->element->getAttribute('format'), strtotime($this->value))."</label>";
	}
}
