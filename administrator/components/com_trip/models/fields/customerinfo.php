<?php

/**
 * @package		Joomla.Tutorials
 * @subpackage	Component
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters, Inc. All rights reserved.
 * @license		License GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

jimport('joomla.form.helper');

JFormHelper::loadFieldClass('list');

class JFormFieldCustomerinfo extends JFormField
{
	protected $type = 'Orderstatus';
 
	public function getInput() {
		$db =& JFactory::getDBO();
		$user =& JFactory::getUser($this->value);
		$params = $user->getParameters();

		$html = "<h4 style='background:#F7F7F7; padding:10px;'>".$user->name." <a href='".JRoute::_('index.php?option=com_shop&task=user.edit&id='.$user->id)."'>[".$user->id."]</a><br/><span style='font-weight:normal; font-size:8pt;'>".$user->email."</span></h4>";
		
		$html .= "
			<div style='float:left; clear:both; width:100%;'>
				<h3>Factuur gegevens</h3>
				<table class='adminlist' width='100%'>
					<tbody>
						<tr>
							<th width='150'>".JText::_('ADDRESS')."</th><td>".@$params->get('invoice_address')."</td>
						</tr>
						<tr>
							<th>".JText::_('ADDRESS_NR')."</th><td>".@$params->get('invoice_address_nr')."</td>
						</tr>
						<tr>
							<th>".JText::_('ADDRESS_NR_EXTRA')."</th><td>".@$params->get('invoice_address_extra')."</td>
						</tr>
						<tr>
							<th>".JText::_('CITY')."</th><td>".@$params->get('invoice_city')."</td>
						</tr>
						<tr>
							<th>".JText::_('COUNTRY')."</th><td>".@$params->get('invoice_country')."</td>
						</tr>
						<tr>
							<th>".JText::_('ZIPCODE')."</th><td>".@$params->get('invoice_zipcode')."</td>
						</tr>
						<tr>
							<th>".JText::_('PHONE')."</th><td>".@$params->get('invoice_phone')."</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div style='float:left;clear:both; width:100%; margin-top:20px;'>
				<h3>Aflever gegevens</h3>
				<table class='adminlist' width='100%'>
					<tbody>
						<tr>
							<th width='150'>".JText::_('ADDRESS')."</th><td>".@$params->get('delivery_address')."</td>
						</tr>
						<tr>
							<th>".JText::_('ADDRESS_NR')."</th><td>".@$params->get('delivery_address_nr')."</td>
						</tr>
						<tr>
							<th>".JText::_('ADDRESS_NR_EXTRA')."</th><td>".@$params->get('delivery_address_extra')."</td>
						</tr>
						<tr>
							<th>".JText::_('CITY')."</th><td>".@$params->get('delivery_city')."</td>
						</tr>
						<tr>
							<th>".JText::_('COUNTRY')."</th><td>".@$params->get('delivery_country')."</td>
						</tr>
						<tr>
							<th>".JText::_('ZIPCODE')."</th><td>".@$params->get('delivery_zipcode')."</td>
						</tr>
						<tr>
							<th>".JText::_('PHONE')."</th><td>".@$params->get('delivery_phone')."</td>
						</tr>
					</tbody>
				</table>
			</div>
		";
		return $html;
	}
}
