<?php

/**
 * @package		Joomla.Tutorials
 * @subpackage	Component
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters, Inc. All rights reserved.
 * @license		License GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

jimport('joomla.form.helper');

JFormHelper::loadFieldClass('list');

class JFormFieldOrderstatus extends JFormField
{
	protected $type = 'Orderstatus';

 
	public function getInput() {
		$db =& JFactory::getDBO();
		$query = "SELECT description FROM #__shop_orderstatusses WHERE order_status_id=".$db->Quote($this->value);
		$db->setQuery($query);
		if($result = $db->loadObject()){
			return "<label>".$result->description."</label>";
		}
		return "<label>".JText::_('UNKNOWN_ORDER_STATUS_ID')."</label>";
	}
}
