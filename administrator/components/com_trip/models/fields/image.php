<?php

/**
 * @package		Joomla.Tutorials
 * @subpackage	Component
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters, Inc. All rights reserved.
 * @license		License GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

jimport('joomla.form.helper');

JFormHelper::loadFieldClass('list');

class JFormFieldImage extends JFormField
{
	protected $type = 'Image';

 
	public function getInput() {
		require_once(JPATH_COMPONENT.DS.'helpers'.DS.'shop.php');
		
		// get product sku for the mapping structure
		$product_sku = '';
		$db =& JFactory::getDBO();
		$query = "SELECT product_sku FROM #__shop_products WHERE product_id=".$db->Quote(JRequest::getInt('product_id'));
		$db->setQuery($query);
		if($result = $db->loadObject()){
			$product_sku = $result->product_sku;
		}
		
		$mainimagesrc = @ShopHelper::getCurrentImagePath($product_sku, $this->fieldname);
		//$mainimagesrc = ShopHelper::imagePath($product_sku, $this->fieldname, null);
		$mainimagesrc = str_replace(JPATH_SITE.DS,JURI::root(), $mainimagesrc);
		
		$doc =& JFactory::getDocument();
		$doc->addScript(JURI::base().'components'.DS.'com_shop'.DS.'assets'.DS.'upload'.DS.'js'.DS.'Request.File.js');
		$doc->addScript(JURI::base().'components'.DS.'com_shop'.DS.'assets'.DS.'upload'.DS.'js'.DS.'Form.MultipleFileInput.js');
		$doc->addScript(JURI::base().'components'.DS.'com_shop'.DS.'assets'.DS.'upload'.DS.'js'.DS.'Form.Upload.js');
		$doc->addStyleSheet(JURI::base().'components'.DS.'com_shop'.DS.'assets'.DS.'upload'.DS.'html'.DS.'styles.css');
	
		$doc->addScriptDeclaration("
			function deleteImage(wrapper, imagetype){
				var deleteimage = new Request.HTML({
					url: 'index.php?option=com_shop&view=product&task=product.deleteImage',
					useSpinner: true,
					evalScripts: false,
					spinnerTarget: wrapper,
					//spinnerOptions: {'message': 'Calculating prices..'},
					onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript){
						if(responseHTML == '1'){
							document.id(wrapper).empty();
						}
					}
				}).send('product_sku=".$product_sku."&imagetype='+imagetype);
			}

			window.addEvent('domready', function(){
				var upload = new Form.Upload('url_".$this->fieldname."', {
					onComplete: function(response){
						if(response.indexOf('".$product_sku."') != -1 && response.indexOf('".$product_sku."') != 0){	
							document.id('mainimage_preview_".$this->fieldname."').empty();
							
							var img   = new Element('img');
							img.src   = '/'+response;
							img.setStyle('width', '150px');
							img.id    = 'mainimage_".$this->fieldname."';
							img.inject(document.id('mainimage_preview_".$this->fieldname."'));
							
							var closebtn = new Element('span');
							closebtn.innerHTML = 'X';
							closebtn.addEvent('click', function(){
								deleteImage('mainimage_preview_".$this->fieldname."', '".$this->fieldname."');
							});
							closebtn.inject(document.id('mainimage_preview_".$this->fieldname."'), 'top');
						}
						else{
							alert('Sla uw product eerst op om een afbeelding toe te voegen.');
						}
					}
				});
			
				if (!upload.isModern()){
					new iFrameFormRequest('myForm', {
						onComplete: function(response){
							alert('Completed uploading the files');
						}
					}); 
				}
			});
		");
		//print $mainimagesrc;
		$html = "
			<div id='uploadwrapper' style='width:100%;'>
				<form method='post' action='index.php?option=com_shop&view=product&layout=files&product_sku=".$product_sku."&fieldname=".$this->fieldname."&task=upload' enctype='multipart/form-data'>
						<div class='formRow'>
							<div style='float:left;'>
								<span style='float:left; margin-top:5px; margin-right:10px;'>".JText::_('FILE')."</span>&nbsp;&nbsp;<input type='file' style='float:left;' size='40' id='url_".$this->fieldname."' name='url[]'>
							</div>
						</div>
				
					
						<div style='width:150px; height:auto; position:relative; float:left; margin-right:5px;' id='mainimage_preview_".$this->fieldname."'>";
							if($mainimagesrc){
								$html .= "<span class='removeimg' onclick=\"deleteImage('mainimage_preview_".$this->fieldname."', '".$this->fieldname."');\">&times;</span>";
								$html .= "<img class='uploadedimg' style='width:150px;' id='mainimage_".$this->fieldname."' src='".$mainimagesrc."'/>";
							}
		$html .= "		</div>
				</form>
			</div>
		";

		return $html;
	}
}
