<?php

/**
 * @package		Joomla.Tutorials
 * @subpackage	Component
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters, Inc. All rights reserved.
 * @license		License GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

jimport('joomla.form.helper');

JFormHelper::loadFieldClass('list');

class JFormFieldUserGroupMap extends JFormField
{
	protected $type = 'UserGroupMap';

 
	public function getInput() {
		$params = ShopHelper::getConfiguration();
		
		//if(JRequest::getInt('id')){
			$db =& JFactory::getDBO();
			$query = "SELECT * FROM #__usergroups ORDER BY id ASC";
			$db->setQuery($query);
			if($result = $db->loadObjectList()){
				$query = "SELECT group_id FROM #__user_usergroup_map WHERE user_id=".$db->Quote(JRequest::getInt('id'));
				$db->setQuery($query);
				$usermap = $db->loadObject();
				return JHTML::_('select.genericlist', $result, "jform[".$this->fieldname."]", null, 'id', 'title', isset($usermap->group_id) ? $usermap->group_id : '');
			}
		//}
		
		return '';
	}
}
