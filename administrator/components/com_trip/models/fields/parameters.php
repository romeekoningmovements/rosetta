<?php

/**
 * @package		Joomla.Tutorials
 * @subpackage	Component
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters, Inc. All rights reserved.
 * @license		License GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

jimport('joomla.form.helper');

JFormHelper::loadFieldClass('list');

class JFormFieldParameters extends JFormField
{
	protected $type = 'Parameters';

 
	public function getInput() {
		$params = ShopHelper::getConfiguration();
		
		$parameters = $params['config_product_properties']->value;
		if(count($parameters)){
			$parafields = explode(',', $parameters);
			$html = "";
		
			$values = @json_decode($this->value) ? @json_decode($this->value) : '';
			foreach($parafields as $parafield){
				$parafield = trim($parafield);
				$html .= "<label>".$parafield."</label>";
				$html .= "<input type='text' name='jform[config_product_properties][".$parafield."]' value='".(isset($values->$parafield) ? $values->$parafield : '')."'/><br/>";
			}
		}
	
		return $html;
	}
}
