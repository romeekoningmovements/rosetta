<?php

/**
 * @package		Joomla.Tutorials
 * @subpackage	Component
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters, Inc. All rights reserved.
 * @license		License GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

jimport('joomla.form.helper');

JFormHelper::loadFieldClass('list');

class JFormFieldId extends JFormField
{
	protected $type = 'Id';

 
	public function getInput() {
		return "<label>".JRequest::getInt('trip_id', $this->value)."</label><input type='hidden' id='".$this->name."' name='".$this->name."' value='".JRequest::getInt('trip_id', $this->value)."'/>";
	}
}
