<?php
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');
jimport( 'joomla.filesystem.file' );


JHTML::_('behavior.framework');
JHTML::_('behavior.modal');
// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_trip')) 
{
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

JLoader::register('TripHelper', dirname(__FILE__) . DS . 'helpers' . DS . 'trip.php');
require_once JPATH_COMPONENT.'/helpers/trip.php';
//$controller	= JController::getInstance('Product');

// fetch the view
$view = JRequest::getVar( 'view' , 'trips' );
$app =& JFactory::getApplication();
$doc =& JFactory::getDocument();

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_trip')) {
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

$controller	= JControllerLegacy::getInstance('Trip');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
