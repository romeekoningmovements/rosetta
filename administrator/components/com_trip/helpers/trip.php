<?php
// No direct access to this file
defined('_JEXEC') or die;
 
/**
 * HelloWorld component helper.
 */
jimport('joomla.filesystem.folder');
abstract class TripHelper
{
	/**
	 * Configure the Linkbar.
	 */
	public static function addSubmenu($submenu) 
	{
		JSubMenuHelper::addEntry(JText::_('COM_TRIP_SUBMENU_TRIPS'),'index.php?option=com_trip&view=trips');
		//JSubMenuHelper::addEntry(JText::_('COM_TRIP_SUBMENU_DEPARTURES'),'index.php?option=com_trip&view=departures');
		JSubMenuHelper::addEntry(JText::_('COM_TRIP_SUBMENU_TRIPTYPES'),'index.php?option=com_trip&view=triptypes');
		JSubMenuHelper::addEntry(JText::_('COM_TRIP_SUBMENU_DESTINATIONS'),'index.php?option=com_trip&view=destinations');
		JSubMenuHelper::addEntry(JText::_('COM_TRIP_SUBMENU_CATEGORIES'),'index.php?option=com_trip&view=tripcategories');
		
		// set some global property
		$document = JFactory::getDocument();
		$document->addStyleDeclaration('.icon-48-icon-com-trip ' .'{background-image: url(../media/com_trip/images/icon-48-trip.png);}');
	}
	
	public static function addTripSubMenu($submenu){
		JSubMenuHelper::addEntry(JText::_('COM_TRIP_SUBMENU_TRIP'),'index.php?option=com_trip&view=trips');
	} 
	
	public function getContentTypesList(){
		$db =& JFactory::getDBO();
		$query = "SELECT * FROM #__xtrip_contentcategories";
		$db->setQuery($query);
		if($results = $db->loadObjectList()){
			return JHTML::_('select.genericlist',$results,"jform[tripcontent_category][]","", 'id', 'title','');
		}
	}
	
	public function getContentTypes($trip_id=1){
		$db =& JFactory::getDBO();
		$query = "SELECT cc.id as catid, cc.title as cattitle, c.* FROM #__xtrip_contentcategories cc LEFT JOIN #__xtrip_content c ON c.cat_id=cc.id AND trip_id=".$db->Quote($trip_id);
		$db->setQuery($query);
		if($results = $db->loadObjectList()){
			return $results;
		}
	}
	
	/**
	 * Get the actions
	 */
	public static function getActions($productID = 0)
	{	
		jimport('joomla.access.access');
		$user	= JFactory::getUser();
		$result	= new JObject;
 
		if (empty($productID)) {
			$assetName = 'com_trip';
		}
		else {
			$assetName = 'com_trip.trip.'.(int) $productID;
		}
 
		$actions = JAccess::getActions('com_trip', 'component');
 
		foreach ($actions as $action) {
			$result->set($action->name, $user->authorise($action->name, $assetName));
		}
 
		return $result;
	}
}