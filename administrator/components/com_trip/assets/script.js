window.addEvent('domready', function(){		
	var accordion = new Fx.Accordion($$('.config_parent'),$$('.config_items'), {
	    opacity: 0,
	    display:0,
	    alwaysHide:true,
	    onActive: function(toggler) { toggler.setProperty('class', 'toggler_active'); },
	    onBackground: function(toggler) { toggler.setProperty('class', 'toggler'); }
	});  
	
	if(document.getElementById('jform_product_price')){
		document.id('jform_product_price').addEvent('change', function(){
			calculatePrices();
		});
	}
});

function jSelectArticle_jform_request_id(id, title, catid, object, link) {
	 var referer = document.id('sbox-content').getElement('iframe').src;
	 var splitted = referer.split('&');
	 var currentfield = null;
	 splitted.each(function(s){
		 if(s.indexOf('field') != -1){
			 var field = s.split('=');
			 currentfield = field[1];
		 }
	 });

 	  document.id(currentfield).value = (id + ':' + title);
	  document.id('jform_request_id_'+currentfield).value = title;
	  SqueezeBox.close();
}
 
function jInsertFieldValue(value, id) {
	var old_id = document.id(id).value;
	if (old_id != id) {
		var elem = document.id(id);
		elem.value = value;
		elem.fireEvent('change');
	}
}

function resetSearch(){
	document.id('shop_search').value = '';
	document.id('adminForm').submit();
}



function calculatePrices(){
	if(document.id('jformproduct_taxclass_id') && document.id('jform_product_price')){
		var myHTMLRequest = new Request.HTML({
			url: window.location.href,
			useSpinner: true,
			evalScripts: false,
			spinnerTarget: 'price_box',
			//spinnerOptions: {'message': 'Calculating prices..'},
			onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript){
				document.id('calculationbox').innerHTML = responseElements.filter('#calculationbox')[0].innerHTML;
			}
		}).send('taxclass_id='+document.id('jformproduct_taxclass_id').value+'&requested_price='+document.id('jform_product_price').value);
	}
}
