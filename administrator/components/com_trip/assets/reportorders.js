/*!
 * Ext JS Library 3.4.0
 * Copyright(c) 2006-2011 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 */
Ext.chart.Chart.CHART_URL = '/administrator/components/com_shop/assets/ext-3.4.0/resources/charts.swf';

Ext.onReady(function(){

	var jsonRequest = new Request.JSON({
		url: 'index.php?option=com_shop&view=report&task=report.orders', 
		onError: function(text, error){
			alert(error);
		},
		onFailure: function(xhr){
			alert(xhr);
		},
		onSuccess: function(objectreference){
			// orders opgehaald d.m.v json en deze in een object array stoppen.
			var data = new Array();
			
			objectreference.each(function(key, val){
				var obj = new Object();
				obj.name = objectreference[val].name;
				obj.visits = objectreference[val].visits;
				obj.views = objectreference[val].views;
				data.push(obj);
			});

		    var store = new Ext.data.JsonStore({
		        fields:['name', 'visits', 'views'],
		        data: data
		    });

		    // more complex with a custom look
		    new Ext.Panel({
		        iconCls:'chart',
		        title: 'Order overzicht voor Boekhandel Roelants / maand',
		        frame:true,
		        renderTo: 'orderchart',
		        width:500,
		        height:300,
		        layout:'fit',

		        items: {
		            xtype: 'columnchart',
		            store: store,
		            url:'/administrator/components/com_shop/assets/ext-3.4.0/resources/charts.swf',
		            xField: 'name',
		            yAxis: new Ext.chart.NumericAxis({
		                displayName: 'Visits',
		                labelRenderer : Ext.util.Format.numberRenderer('0,0')
		            }),
		            tipRenderer : function(chart, record, index, series){
		                if(series.yField == 'visits'){
		                    return Ext.util.Format.number(record.data.visits, '0,0') + ' visits in ' + record.data.name;
		                }else{
		                    return Ext.util.Format.number(record.data.views, '0,0') + ' orders in ' + record.data.name;
		                }
		            },
		            chartStyle: {
		                padding: 10,
		                animationEnabled: true,
		                font: {
		                    name: 'Arial',
		                    color: 0x444444,
		                    size: 11
		                },
		                dataTip: {
		                    padding: 5,
		                    border: {
		                        color: 0x99bbe8,
		                        size:1
		                    },
		                    background: {
		                        color: 0xDAE7F6,
		                        alpha: .9
		                    },
		                    font: {
		                        name: 'Arial',
		                        color: 0x15428B,
		                        size: 10,
		                        bold: true
		                    }
		                },
		                xAxis: {
		                    color: 0x69aBc8,
		                    majorTicks: {color: 0x69aBc8, length: 4},
		                    minorTicks: {color: 0x69aBc8, length: 2},
		                    majorGridLines: {size: 1, color: 0xeeeeee}
		                },
		                yAxis: {
		                    color: 0x69aBc8,
		                    majorTicks: {color: 0x69aBc8, length: 4},
		                    minorTicks: {color: 0x69aBc8, length: 2},
		                    majorGridLines: {size: 1, color: 0xdfe8f6}
		                }
		            },
		            series: [{
		                type: 'column',
		                displayName: 'Orders',
		                yField: 'views',
		                style: {
		                    image:'/administrator/components/com_shop/assets/ext-3.4.0/examples/chart/bar.gif',
		                    mode: 'stretch',
		                    color:0x99BBE8
		                }
		            },{
		                type:'line',
		                displayName: 'Visits',
		                yField: 'visits',
		                style: {
		                    color: 0x15428B
		                }
		            }]
		        }
		    });
		}
	}).send();
});