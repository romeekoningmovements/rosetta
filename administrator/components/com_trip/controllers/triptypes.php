<?php
/**
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');


/**
 * Weblinks list controller class.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_weblinks
 * @since		1.6
 */
class TripControllerTriptypes extends JControllerAdmin
{
	function __construct($default = array())
    {
        parent::__construct($default);
        $this->registerTask('delete', 'delete');
    }
	
	public function delete(){
		$app =& JFactory::getApplication();
		$model = $this->getModel('triptype');
		$triptypes = JRequest::getVar('cid');
		foreach($triptypes as $triptype){
			$model->deleteProducts($triptype);
		}
		$this->setRedirect('index.php?option=com_trip&view=triptypes&extention=com_trip', JText::_('COM_TRIP_DELETE_SUCCESSFULL'));
	}
}
