<?php

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

 
jimport('joomla.application.component.controllerform');
 
class TripControllerOption extends JControllerForm
{
	function __construct($default = array())
    {
        parent::__construct($default);
    
        $this->registerTask('add', 'edit');
        $this->registerTask('cancel', 'cancel');
        $this->registerTask('apply', 'apply');
        $this->registerTask('unpublish', 'publish');
        $this->registerTask('save2new', 'save2new');
        
    }
       
    function save2new(){
    	$server =& JURI::getInstance();
		$app =& JFactory::getApplication();
   		$model = $this->getModel();
   		if($model->save()){
   			if($_REQUEST['jform']['trip_id']){
		   		$app->redirect('index.php?option=com_trip&view=trip&layout=edit&trip_id='.$_REQUEST['jform']['trip_id'], JText::_('SAVE_PRODUCT_SUCCESSFULLY'));
	   		}
   		}
   		$app->redirect('index.php?option=com_trip&layout=edit&id=0&task=departure.edit', JText::_('SAVE_PRODUCT_UNSUCCESSFULLY'));
    }
    
	function apply(){
		$server =& JURI::getInstance();
		$app =& JFactory::getApplication();
   		$model = $this->getModel();
   		if($result = $model->save()){
	   		if($_REQUEST['jform']['trip_id']){
		   		$app->redirect('index.php?option=com_trip&view=trip&layout=edit&trip_id='.$_REQUEST['jform']['trip_id'], JText::_('SAVE_PRODUCT_SUCCESSFULLY'));
	   		}
   		}
   		$app->redirect('index.php?option=com_trip&layout=edit&id='.$result->id.'&task=departure.edit', JText::_('SAVE_PRODUCT_UNSUCCESSFULLY'));
	}
	
	public function delete(){
		$app =& JFactory::getApplication();
		$model = $this->getModel('option');
		$option = JRequest::getInt('id');
		$model->deleteOption($option);
		
		$this->setRedirect('index.php?option=com_trip&view=trip&layout=edit&trip_id='.JRequest::getInt('trip_id'), JText::_('COM_TRIP_DELETE_SUCCESSFULL'));
	}
   
   function save(){
   		$app =& JFactory::getApplication();
   		$model = $this->getModel();
   		if($model->save()){
   			if($_REQUEST['jform']['trip_id']){
		   		$app->redirect('index.php?option=com_trip&view=trip&layout=edit&trip_id='.$_REQUEST['jform']['trip_id'], JText::_('SAVE_PRODUCT_SUCCESSFULLY'));
	   		}
   			//$app->redirect('index.php?option=com_trip&view=departures', JText::_('SAVE_PRODUCT_SUCCESSFULLY'));
   		}
   		$app->redirect('index.php?option=com_trip&view=departures', JText::_('SAVE_PRODUCT_UNSUCCESSFULLY'));
   }
   
   function cancel(){
   		$app =& JFactory::getApplication();
   		if($_REQUEST['jform']['trip_id']){
	   		$app->redirect('index.php?option=com_trip&view=trip&layout=edit&trip_id='.$_REQUEST['jform']['trip_id']);
   		}
   }
}