<?php

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

 
jimport('joomla.application.component.controllerform');
 
class TripControllerTrip extends JControllerForm
{
	function __construct($default = array())
    {
        parent::__construct($default);
    
        $this->registerTask('add', 'edit');
        $this->registerTask('cancel', 'cancel');
        $this->registerTask('apply', 'apply');
        $this->registerTask('unpublish', 'publish');
        $this->registerTask('save2new', 'save2new');
        
    }
       
    function save2new(){
    	$server =& JURI::getInstance();
		$app =& JFactory::getApplication();
   		$model = $this->getModel();
   		if($model->save()){
   			$app->redirect('index.php?option=com_trip&layout=edit&trip_id=0&task=trip.edit', JText::_('SAVE_PRODUCT_SUCCESSFULLY'));
   		}
   		$app->redirect('index.php?option=com_trip&layout=edit&trip_id=0&task=trip.edit', JText::_('SAVE_PRODUCT_UNSUCCESSFULLY'));
    }
    
	function apply(){
		$server =& JURI::getInstance();
		$app =& JFactory::getApplication();
   		$model = $this->getModel();
   		if($result = $model->save()){

   			$app->redirect('index.php?option=com_trip&layout=edit&trip_id='.$result->trip_id.'&task=trip.edit', JText::_('SAVE_PRODUCT_SUCCESSFULLY'));
   		}
   		$app->redirect('index.php?option=com_trip&layout=edit&trip_id='.$result->trip_id.'&task=trip.edit', JText::_('SAVE_PRODUCT_UNSUCCESSFULLY'));
	}
   
   function save(){
   		$app =& JFactory::getApplication();
   		$model = $this->getModel();
   		if($model->save()){
   			$app->redirect('index.php?option=com_trip&view=trips', JText::_('SAVE_PRODUCT_SUCCESSFULLY'));
   		}
   		$app->redirect('index.php?option=com_trip&view=trips', JText::_('SAVE_PRODUCT_UNSUCCESSFULLY'));
   }
}