<?php

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

 
jimport('joomla.application.component.controllerform');
 
class TripControllerTripcategory extends JControllerForm
{
	function __construct($default = array())
    {
        parent::__construct($default);
    
        $this->registerTask('add', 'edit');
        $this->registerTask('cancel', 'cancel');
        $this->registerTask('apply', 'apply');
        $this->registerTask('unpublish', 'publish');
        $this->registerTask('save2new', 'save2new');    
    }
       
    function save2new(){
    	$server =& JURI::getInstance();
		$app =& JFactory::getApplication();
   		$model = $this->getModel();
   		if($model->save()){
   			$app->redirect('index.php?option=com_trip&layout=edit&id=0&task=tripcategory.edit', JText::_('SAVE_TRIPCATEGORY_SUCCESSFULLY'));
   		}
   		$app->redirect('index.php?option=com_trip&layout=edit&id=0&task=tripcategory.edit', JText::_('SAVE_TRIPCATEGORY_UNSUCCESSFULLY'));
    }
    
	function apply(){
		$server =& JURI::getInstance();
		$app =& JFactory::getApplication();
   		$model = $this->getModel();
   		if($result = $model->save()){
   			$app->redirect('index.php?option=com_trip&layout=edit&id='.$result->id.'&task=tripcategory.edit', JText::_('SAVE_TRIPCATEGORY_SUCCESSFULLY'));
   		}
   		$app->redirect('index.php?option=com_trip&layout=edit&id='.$result->id.'&task=tripcategory.edit', JText::_('SAVE_TRIPCATEGORY_UNSUCCESSFULLY'));
	}
   
   function save(){
   		$app =& JFactory::getApplication();
   		$model = $this->getModel();
   		if($model->save()){
   			$app->redirect('index.php?option=com_trip&view=tripcategories', JText::_('SAVE_TRIPCATEGORY_SUCCESSFULLY'));
   		}
   		$app->redirect('index.php?option=com_trip&view=tripcategories', JText::_('SAVE_TRIPCATEGORY_UNSUCCESSFULLY'));
   }
}