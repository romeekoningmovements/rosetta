<?php
/**
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');


/**
 * Weblinks list controller class.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_weblinks
 * @since		1.6
 */
class TripControllerDestinations extends JControllerAdmin
{
	function __construct($default = array())
    {
        parent::__construct($default);
        $this->registerTask('delete', 'delete');
    }
	
	public function publish(){
		// change the state of one or more items.
		$app =& JFactory::getApplication();
		$task = JRequest::getCmd('task');
		$model = $this->getModel('destination');
		$trips = JRequest::getVar('cid');
		$msg = JText::_('COM_TRIP_UNPUBLISH_SUCCESSFULL');

		if($trips){
			foreach($trips as $trip){
				switch($task){
					case 'publish':
						$msg = JText::_('COM_TRIP_PUBLISH_SUCCESSFULL');
						$model->changeState($trip, 1);
					break;
					case 'unpublish':
						$model->changeState($trip, 0);
					break;
				}
			}

			$this->setRedirect('index.php?option=com_trip&view=destinations&extention=com_trip', $msg);
		}
		
		return true;
	}
	
	public function delete(){
		$app =& JFactory::getApplication();
		$model = $this->getModel('destination');
		$destinations = JRequest::getVar('cid');
		foreach($destinations as $destination){
			$model->deleteProducts($destination);
		}
		$this->setRedirect('index.php?option=com_trip&view=destinations&extention=com_trip', JText::_('COM_TRIP_DELETE_SUCCESSFULL'));
	}
}
