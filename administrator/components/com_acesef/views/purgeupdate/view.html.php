<?php
/**
* @version		2.5.0
* @package		AceSEF
* @subpackage	AceSEF
* @copyright	2009-2012 JoomAce LLC, www.joomace.net
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/

// No Permission
defined('_JEXEC') or die('Restricted Access');

// View Class
class AcesefViewPurgeUpdate extends AcesefView {

	// Display purge
	function view($tpl = null) {
		// Get data from the model
		$this->urls = $this->get('CountUrls');
		$this->meta = $this->get('CountMeta');

		parent::display($tpl);
	}
}
