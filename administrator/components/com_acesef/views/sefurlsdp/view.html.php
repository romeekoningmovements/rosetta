<?php
/**
* @version		2.5.0
* @package		AceSEF
* @subpackage	AceSEF
* @copyright	2009-2012 JoomAce LLC, www.joomace.net
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/

// No Permission
defined('_JEXEC') or die('Restricted Access');

// View Class
class AcesefViewSefUrlsDp extends AcesefView {

	// View URLs
	function view($tpl = null) {
		// Get data from the model
		$this->lists = $this->get('Lists');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->toolbar = $this->get('ToolbarSelections');
		$this->sef = $this->get('SefUrl');
		
		parent::display($tpl);
	}
}
?>