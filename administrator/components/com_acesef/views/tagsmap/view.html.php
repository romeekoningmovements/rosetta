<?php
/**
* @version		2.5.0
* @package		AceSEF
* @subpackage	AceSEF
* @copyright	2009-2012 JoomAce LLC, www.joomace.net
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/

// No Permission
defined('_JEXEC') or die('Restricted Access');

// View Class
class AcesefViewTagsMap extends AcesefView {

	// View URLs
	function view($tpl = null) {
		// Get data from the model
		$this->items = $this->get('Items');
		$this->lists = $this->get('Lists');
		$this->pagination = $this->get('Pagination');
		$this->toolbar = $this->get('ToolbarSelections');

		parent::display($tpl);
	}
}
?>