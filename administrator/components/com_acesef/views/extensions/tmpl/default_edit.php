<?php
/**
* @version		2.5.0
* @package		AceSEF
* @subpackage	AceSEF
* @copyright	2009-2012 JoomAce LLC, www.joomace.net
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/

// No Permission
defined('_JEXEC') or die('Restricted access');

// Tmpl var
$tmpl = JRequest::getVar('tmpl');
?>

<script language="javascript">
	function submitbutton(pressbutton){
		// Check if is modal ivew
		<?php if ($tmpl == 'component') { ?>
		document.adminForm.modal.value = '1';
		<?php } ?>
		
		submitform(pressbutton);
	}
</script>

<form action="index.php?option=com_acesef&amp;controller=extensions&amp;task=edit&amp;cid[]=<?php echo $this->row->id; ?>&amp;tmpl=component" method="post" name="adminForm">
	<div>
		<fieldset class="adminform">
			<table class="toolbar1">
				<tr>
					<td class="desc" width="550px">
						<?php echo '<h3>'.$this->row->description.'</h3>'; ?>
					</td>
					<td>
						<a href="#" onclick="javascript: submitbutton('editSave'); window.top.setTimeout('SqueezeBox.close();', 1000);" class="toolbar1"><span class="icon-32-save1" title="<?php echo JText::_('ACESEF_COMMON_SAVE'); ?>"></span><?php echo JText::_('ACESEF_COMMON_SAVE'); ?></a>
					</td>
					<td>
						<a href="#" onclick="javascript: submitbutton('editApply'); " class="toolbar1"><span class="icon-32-apply1" title="<?php echo JText::_('ACESEF_COMMON_APPLY'); ?>"></span><?php echo JText::_('ACESEF_COMMON_APPLY'); ?></a>
					</td>
					<td>
						<a href="#" onclick="javascript: submitbutton('editCancel'); window.top.setTimeout('SqueezeBox.close();', 1000);" class="toolbar1"><span class="icon-32-cancel1" title="<?php echo JText::_('ACESEF_COMMON_CANCEL'); ?>"></span><?php echo JText::_('ACESEF_COMMON_CANCEL'); ?></a>
					</td>
				</tr>
			</table>
		</fieldset>
	</div>
	<div>
		<?php
            if ($fields = $this->ext_params->getFieldset('download_id')){
		?>
			<fieldset class="adminform">
				<legend><?php echo JText::_('ACESEF_CONFIG_MAIN_UPGRADE_ID'); ?></legend>
                <ul class="adminformlist">
                    <?php foreach($fields as $field) { ?>
                    <li><?php echo $field->label; echo $field->input; ?></li>
                    <?php } ?>
                </ul>
			</fieldset>
		<?php
			}
		?>
		<fieldset class="adminform">
			<legend><?php echo JText::_('Parameters'); ?></legend>

			<?php
				 echo JHtml::_('tabs.start', 'extension');
				// URL tab
				echo JHtml::_('tabs.panel', JText::_('ACESEF_COMMON_URL'), 'url');
				echo JHtml::_('sliders.start', 'url');
				echo JHtml::_('sliders.panel', JText::_('ACESEF_PARAMS_URL_EXTENSION'), 'extension');
				if ($fields = $this->ext_params->getFieldset('url')){
				?>
				<fieldset class="panelform">
                    <ul class="adminformlist">
						<?php foreach($fields as $field) { ?>
                                <li><?php echo $field->label; echo $field->input; ?></li>
                        <?php } ?>
					</ul>
				</fieldset>
				<?php
				}
				echo JHtml::_('sliders.panel', JText::_('ACESEF_PARAMS_URL_COMMON'), 'common');
				if ($fields = $this->common_params->getFieldset('default_url')){
				?>
				<fieldset class="panelform">
                    <ul class="adminformlist">
						<?php foreach($fields as $field) { ?>
                                <li><?php echo $field->label; echo $field->input; ?></li>
                        <?php } ?>
					</ul>
				</fieldset>
				<?php
				}
				echo JHtml::_('sliders.end');
				// Meta tab
				echo JHtml::_('tabs.panel', JText::_('ACESEF_COMMON_METADATA'), 'meta');
				if ($fields = $this->common_params->getFieldset('default_meta')){
				?>
				<fieldset class="panelform">
                    <ul class="adminformlist">
						<?php foreach($fields as $field) { ?>
                                <li><?php echo $field->label; echo $field->input; ?></li>
                        <?php } ?>
					</ul>
				</fieldset>
				<?php
				}
				if ($fields = $this->ext_params->getFieldset('meta')){
				?>
				<fieldset class="panelform">
                    <ul class="adminformlist">
						<?php foreach($fields as $field) { ?>
                                <li><?php echo $field->label; echo $field->input; ?></li>
                        <?php } ?>
					</ul>
				</fieldset>
				<?php
				}
				// Sitemap tab
				echo JHtml::_('tabs.panel', JText::_('ACESEF_COMMON_SITEMAP'), 'sitemap');
				if ($fields = $this->common_params->getFieldset('default_sitemap_auto_header')){
				?>
				<fieldset class="panelform">
                    <ul class="adminformlist">
						<?php foreach($fields as $field) { ?>
                                <li><?php echo $field->label; echo $field->input; ?></li>
                        <?php } ?>
					</ul>
				</fieldset>
				<?php
				}
				if ($fields = $this->common_params->getFieldset('default_sitemap_auto_cats')){
				?>
				<fieldset class="panelform">
                    <ul class="adminformlist">
						<?php foreach($fields as $field) { ?>
                                <li><?php echo $field->label; echo $field->input; ?></li>
                        <?php } ?>
					</ul>
				</fieldset>
				<?php
				}
				if ($fields = $this->common_params->getFieldset('default_sitemap_auto')){
				?>
				<fieldset class="panelform">
                    <ul class="adminformlist">
						<?php foreach($fields as $field) { ?>
                                <li><?php echo $field->label; echo $field->input; ?></li>
                        <?php } ?>
					</ul>
				</fieldset>
				<?php
				}
				// Tags tab
				echo JHtml::_('tabs.panel', JText::_('ACESEF_COMMON_TAGS'), 'tags');
				if ($fields = $this->common_params->getFieldset('default_tags')){
				?>
				<fieldset class="panelform">
                    <ul class="adminformlist">
						<?php foreach($fields as $field) { ?>
                                <li><?php echo $field->label; echo $field->input; ?></li>
                        <?php } ?>
					</ul>
				</fieldset>
				<?php
				}
				if ($fields = $this->common_params->getFieldset('default_tags_cats')){
				?>
				<fieldset class="panelform">
                    <ul class="adminformlist">
						<?php foreach($fields as $field) { ?>
                                <li><?php echo $field->label; echo $field->input; ?></li>
                        <?php } ?>
					</ul>
				</fieldset>
				<?php
				}
				if ($fields = $this->common_params->getFieldset('tags')){
				?>
				<fieldset class="panelform">
                    <ul class="adminformlist">
						<?php foreach($fields as $field) { ?>
                                <li><?php echo $field->label; echo $field->input; ?></li>
                        <?php } ?>
					</ul>
				</fieldset>
				<?php
				}
				
				// Internal Links tab
				echo JHtml::_('tabs.panel', JText::_('ACESEF_COMMON_ILINKS'), 'ilinks');
				if ($fields = $this->common_params->getFieldset('default_ilinks')){
				?>
				<fieldset class="panelform">
                    <ul class="adminformlist">
						<?php foreach($fields as $field) { ?>
                                <li><?php echo $field->label; echo $field->input; ?></li>
                        <?php } ?>
					</ul>
				</fieldset>
				<?php
				}
				if ($fields = $this->common_params->getFieldset('default_ilinks_cats')){
				?>
				<fieldset class="panelform">
                    <ul class="adminformlist">
						<?php foreach($fields as $field) { ?>
                                <li><?php echo $field->label; echo $field->input; ?></li>
                        <?php } ?>
					</ul>
				</fieldset>
				<?php
				}
				if ($fields = $this->common_params->getFieldset('ilinks')){
				?>
				<fieldset class="panelform">
                    <ul class="adminformlist">
						<?php foreach($fields as $field) { ?>
                                <li><?php echo $field->label; echo $field->input; ?></li>
                        <?php } ?>
					</ul>
				</fieldset>
				<?php
				}
				
				// Social Bookmarks tab
				echo JHtml::_('tabs.panel', JText::_('ACESEF_COMMON_ILINKS'), 'bookmarks');
				if ($fields = $this->common_params->getFieldset('default_bookmarks')){
				?>
				<fieldset class="panelform">
                    <ul class="adminformlist">
						<?php foreach($fields as $field) { ?>
                                <li><?php echo $field->label; echo $field->input; ?></li>
                        <?php } ?>
					</ul>
				</fieldset>
				<?php
				}
				if ($this->row->hasCats && ($fields = $this->common_params->getFieldset('default_bookmarks_cats'))){
				?>
				<fieldset class="panelform">
                    <ul class="adminformlist">
						<?php foreach($fields as $field) { ?>
                                <li><?php echo $field->label; echo $field->input; ?></li>
                        <?php } ?>
					</ul>
				</fieldset>
				<?php
				}
				if ($fields = $this->common_params->getFieldset('bookmarks')){
				?>
				<fieldset class="panelform">
                    <ul class="adminformlist">
						<?php foreach($fields as $field) { ?>
                                <li><?php echo $field->label; echo $field->input; ?></li>
                        <?php } ?>
					</ul>
				</fieldset>
				<?php
				}
                echo JHtml::_('tabs.end');
			?>
		</fieldset>
	</div>
	<div class="clr">
	</div>
	<input type="hidden" name="option" value="com_acesef" />
	<input type="hidden" name="controller" value="extensions" />
	<input type="hidden" name="task" value="edit" />
	<input type="hidden" name="modal" value="0" />
	<input type="hidden" name="id" value="<?php echo $this->row->id; ?>" />
	<?php echo JHTML::_('form.token'); ?>
</form>