<?php
/**
* @version		2.5.0
* @package		AceSEF
* @subpackage	AceSEF
* @copyright	2009-2012 JoomAce LLC, www.joomace.net
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/

// No Permission
defined('_JEXEC') or die('Restricted Access');

// Imports
AcesefUtility::import('library.elements.routerlist');
AcesefUtility::import('library.elements.categorylist');
JLoader::register('JHtmlSelect', JPATH_ACESEF_ADMIN.'/library/joomla/select.php');
JLoader::register('JElementRadio', JPATH_ACESEF_ADMIN.'/library/joomla/radio.php');
JLoader::register('JElementSpacer', JPATH_ACESEF_ADMIN.'/library/joomla/spacer.php');

// Edit Extension View Class
class AcesefViewExtensions extends AcesefView {

	// Edit extension
	function edit($tpl = NULL) {
		// Get row
		// $model = $this->getModel();
		// $row = $model->getEditData('AcesefExtensions');
		// $row->params = self::_getParams($row->extension, $row->params);
		
		// Get description from XML
		// $xml_file = JPATH_ACESEF_ADMIN.DS.'extensions'.DS.$row->extension.'.xml';
		// if (file_exists($xml_file)) {
			// $row->description = AcesefUtility::getXmlText($xml_file, 'description');
		// }
		$row = $this->getModel()->getEditData('AcesefExtensions');
		jimport('joomla.form.form');
        $ext_form = JForm::getInstance('extensionForm', JPATH_ACESEF_ADMIN.'/extensions/'.$row->extension.'.xml', array(), true, 'config');
        $ext_values = array('params' => json_decode($row->params));
        $ext_form->bind($ext_values);

        $common_form = JForm::getInstance('commonForm', JPATH_ACESEF_ADMIN.'/extensions/default_params.xml', array(), true, 'config');
        $common_values = array('params' => json_decode($row->params));
        $common_form->bind($common_values);

        $row->description = '';
        $row->hasCats = 0;

        $xml_file = JPATH_ACESEF_ADMIN.'/extensions/'.$row->extension.'.xml';
        if (file_exists($xml_file)) {
            $row->description = AcesefUtility::getXmlText($xml_file, 'description');
            $row->hasCats = (int) AcesefUtility::getXmlText($xml_file, 'hasCats');
        }
		// Get behaviors
		JHTML::_('behavior.combobox');
		JHTML::_('behavior.tooltip');
		
		// Import pane
		jimport('joomla.html.pane');
		$tabs = JPane::getInstance('Tabs');
		$sliders = JPane::getInstance('Sliders');
		
		// Assign data
		$this->assignRef('row', 			$row);
		$this->assignRef('tabs', 			$tabs);
		$this->assignRef('sliders', 		$sliders);
		$this->assignRef('ext_params', 		$ext_form);
        $this->assignRef('common_params', 	$common_form);

		parent::display($tpl);
	}
}
?>