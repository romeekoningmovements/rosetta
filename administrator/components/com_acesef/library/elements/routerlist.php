<?php
/**
* @version		2.5.0
* @package		AceSEF
* @subpackage	AceSEF
* @copyright	2009-2012 JoomAce LLC, www.joomace.net
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/

// No Permission
defined('_JEXEC') or die('Restricted Access');

jimport('joomla.form.formfield');

class JFormFieldRouterList extends JFormField {

	protected $type = 'RouterList';

	function getInput() {

		$attribs = 'class="inputbox"';
	
        $extension = AcesefUtility::getExtensionFromRequest();
		
		$options = AcesefUtility::getRouterList($extension);

		return JHTML::_('select.genericlist', $options, $this->name, $attribs, 'value', 'text', $this->value, $this->name);
	}
}