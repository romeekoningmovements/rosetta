<?php
/*
* @package		AceSEF
* @subpackage	Search
* @copyright	2009-2012 JoomAce LLC, www.joomace.net
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/

// No Permission
defined('_JEXEC') or die('Restricted access');

class AceSEF_com_search extends AcesefExtension {
	
	function beforeBuild(&$uri) {
        $ord = $uri->getVar('ordering', null);
        if ($ord == '') {
            $uri->delVar('ordering');
        }
		
		if (!is_null($uri->getVar('view')) && $uri->getVar('view') == 'search') {
			$uri->delVar('view');
		}

		if (is_null($uri->getVar('limitstart')) && !is_null($uri->getVar('limit'))){
			$uri->delVar('limit');
		}
        
        $phrase = $uri->getVar('searchphrase', null);
        if ($phrase == 'all') {
            $uri->delVar('searchphrase');
        }
    }
	
	function build(&$vars, &$segments, &$do_sef, &$metadata, &$item_limitstart) {
        extract($vars);
		
		if (isset($searchword)) {
            $segments[] = $searchword;
			$this->meta_desc = $this->meta_title[] = $searchword;
			unset($vars['searchword']);
		}
        
        if (isset($searchphrase)) {
            $segments[] = $searchphrase;
			unset($vars['searchphrase']);
		}
		
		if (isset($ordering)) {
            $segments[] = $ordering;
			unset($vars['ordering']);
		}
        
        if (isset($submit)) {
            $segments[] = $submit;
			unset($vars['submit']);
		}
		
		$metadata = parent::getMetaData($vars, $item_limitstart);
		
		unset($vars['limit']);
		unset($vars['limitstart']);
	}


	function _is16() {
		static $status;
		
		if (!isset($status)) {
			if (version_compare(JVERSION,'1.6.0','ge')) {
				$status = true;
			} else {
				$status = false;
			}
		}
		
		return $status;
	}
	
	function getCategoryList($query) {
        if (self::_is16()) {
            $rows = AceDatabase::loadObjectList("SELECT id, title AS name, parent_id AS parent FROM #__categories WHERE parent_id > 0 AND published = 1 AND extension = 'com_content' ORDER BY parent_id, lft");
        }
        else{
            $rows = AceDatabase::loadObjectList("SELECT c.id, CONCAT_WS(' / ', s.title, c.title) AS name FROM #__categories AS c, #__sections AS s WHERE s.scope = 'content' AND c.section = s.id ORDER BY s.title, c.title");
        }
        
        return $rows;
	}
}
?>