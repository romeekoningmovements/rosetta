<?php
	defined('_JEXEC') or die('Restricted access');
	// @loek hieronder nog de stijl weghalen en in less zetten bitte sehr, danke sch�n
?>
<div id="calculationbox">
	<div id="spinnertrigger"></div>
	<?php
		if(!empty($calculation)){
			?>
				<div id="calculation_wrapper">
					<h4><?php echo $calculation->title; ?></h4>
					<div class="calculation">
						<div class="group clearfix">
							<span>
								<?php
								if($calculation->nrpersons != 0){
								?>
								Voor 
								<?php
									if($calculation->nrpersons > 1){
										echo $calculation->nrpersons." personen";
									} 
									else{
										echo $calculation->nrpersons." persoon";
									}
								}
								?> 
							</span>
						</div>
						<div class="group clearfix">
							<span>Reissom</span>
							<span class="price">&euro; <?php echo number_format($calculation->reissom, 2,',','.'); ?></span>
						</div>
						<div class="group clearfix">
							<span>Boekingskosten</span>
							<span class="price">&euro; <?php echo number_format($calculation->boekingsbijdrage, 2,',','.'); ?></span>
						</div>
						<div class="group clearfix">
							<span>Calamiteitenfonds</span>
							<span class="price">&euro; <?php echo number_format($calculation->calamiteitenfonds, 2,',','.'); ?></span>
						</div>
						<div class="group clearfix">
							<span>Visumkosten</span>
							<span class="price">&euro; <?php echo number_format($calculation->visumkosten, 2,',','.'); ?></span>
						</div>
						<div class="group clearfix">
							<span>Annuleringsverzekering</span>
							<span class="price">&euro; <?php echo number_format($calculation->annuleringskosten, 2,',','.'); ?></span>
						</div>
						<div class="group clearfix">
							<span>Gescheiden facturering</span>
							<span class="price">
								<?php
									echo $calculation->gescheiden;
								?>
							</span>
						</div>
						<?php
							if(isset($calculation->extraoptions)){
								?>
								<div class="group clearfix">
									<span><strong>Extra opties</strong></span>
								</div>
								<?php
								foreach($calculation->extraoptions as $option){
									?>
										<div class="group clearfix">
											<span><?php echo $option->name." (".$option->desc; ?>)</span>
											<span class="price">&euro; <?php echo number_format($option->total, 2,',','.'); ?></span>
										</div>
									<?php
								}
							}
						?>
						<div class="group totalprice">
							<span>Totaal</span>
							<span class="price">&euro; <?php echo number_format($calculation->total, 2,',','.'); ?></span>
						</div>
	
					</div>
				</div>
			<?php
		}else{
			?>
			<center>U dient een selectie te maken</center>
			<?php
			
		}
	?>
</div>