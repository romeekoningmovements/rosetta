<?php
/**
 * @package		IBRIX.Trip
 * @subpackage	mod_trip_related
 * @author		Joep Tijssen
 */

// no direct access
defined('_JEXEC') or die;

class modTripCalculateHelper
{	
	static function getParams(&$params)
	{
		return $params;
	}
	
	public function getCalculation($triptype_id, $trip_id, $departure_id, $persons,$options, $extraoptions){
		$session =& JFactory::getSession();
		$db =& JFactory::getDBO();
		if(!$departure_id){
			return false;
		}
		$query  = "SELECT tt.triptype, t.title as title, ";
		if(is_numeric($departure_id)){
			$query .= "(d.reissom * ".(int)$persons.") as reissom, ";
			$query .= "d.vertrek as vertrekdatum, ";
		}
		else{
			$query .= "(t.reissom * ".(int)$persons.") as reissom, ";
			$query .= $db->Quote($departure_id)." as vertrekdatum, ";
		}
		$query .= "t.luchthaventax as luchthaventax, ";
		$query .= "t.boekingsbijdrage as boekingsbijdrage, t.calamiteitenfonds as calamiteitenfonds, ";
		$query .= "IF(t.visumnodig, (t.visumkosten * ".$persons."), 0) as visumkosten, ";
		$query .= "t.visumnodig as visumnodig, ".$persons." as nrpersons, ";
		
		
		if(isset($options[0])){
			if(is_numeric($departure_id)){
				$query .= "((IF(t.visumnodig, (t.visumkosten * ".$persons."), 0) + boekingsbijdrage + calamiteitenfonds + (d.reissom * ".(int)$persons.")) * 0.055) as annuleringskosten, ";
				$query .= "((IF(t.visumnodig, (t.visumkosten * ".$persons."), 0) + boekingsbijdrage + calamiteitenfonds + (d.reissom * ".(int)$persons.")) * 1.055) as total ";
			}
			else{
				$query .= "((IF(t.visumnodig, (t.visumkosten * ".$persons."), 0) + boekingsbijdrage + calamiteitenfonds + (t.reissom * ".(int)$persons.")) * 0.055) as annuleringskosten, ";
				$query .= "((IF(t.visumnodig, (t.visumkosten * ".$persons."), 0) + boekingsbijdrage + calamiteitenfonds + (t.reissom * ".(int)$persons.")) * 1.055) as total ";
			}
		}
		else{
			$query .= "0 as annuleringskosten, ";
			if(is_numeric($departure_id)){
				$query .= "(IF(t.visumnodig, (t.visumkosten * ".$persons."), 0) + boekingsbijdrage + calamiteitenfonds + (d.reissom * ".(int)$persons.")) as total ";
			}
			else{ 
				$query .= "(IF(t.visumnodig, (t.visumkosten * ".$persons."), 0) + boekingsbijdrage + calamiteitenfonds + (t.reissom * ".(int)$persons.")) as total ";
			}
		}
		$query .= "FROM jos_xtrip_trips t ";
		$query .= "INNER JOIN jos_xtrip_triptypes tt ON tt.id=t.triptype_id ";
		$query .= "LEFT JOIN jos_xtrip_departures d ON d.tripid=t.trip_id ";
		$query .= "LEFT JOIN jos_xtrip_options o ON o.trip_id=t.trip_id ";
		$query .= "WHERE t.trip_id=".$db->Quote($trip_id);
		if(is_numeric($departure_id)){
			$query .= " AND d.id=".$departure_id;
		}
		//print $query;
		$db->setQuery($query);
		if(!$calculation = $db->loadObject()){
			return false;
		}
		
		if(isset($options[1])){
			$calculation->gescheiden = 'Ja';
		}
		else{
			$calculation->gescheiden = 'Nee';
		}
		
		if(!empty($extraoptions)){
			$query = "SELECT * FROM jos_xtrip_options WHERE id IN (";
			foreach($extraoptions as $key=>$extraoption){
				if($extraoption == 'true'){
					$query .= $db->Quote($key).",";
				}
			}
			$query = substr($query, 0, -1).")";
			$db->setQuery($query);
			if($options = $db->loadObjectList()){
				foreach($options as $key=>$o){
					if($o->cpp){
						$calculation->total = ($calculation->total + ($o->cpp * (int)$persons));
						$calculation->extraoptions[$o->id]->total = ($o->cpp * (int)$persons);
						$calculation->extraoptions[$o->id]->desc = 'pp';
					}
					elseif($o->cpt){
						$calculation->total = ($calculation->total + ($o->cpt));
						$calculation->extraoptions[$o->id]->total = ($o->cpt);
						$calculation->extraoptions[$o->id]->desc = 'pr';
					}
					$calculation->extraoptions[$o->id]->name = $o->description;	
					if(isset($options[0])){
						$calculation->total += ($calculation->extraoptions[$o->id]->total * 0.055);
					} 
				}
			}
		}
		$calculation->trip_id = $trip_id;
		$calculation->triptype_id = $triptype_id;
		$calculation->departure = $departure_id;
		
		$session->set('booking', json_encode($calculation));
		return $calculation;
	}
}