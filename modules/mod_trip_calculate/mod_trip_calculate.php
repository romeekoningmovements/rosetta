<?php
/**
 * @package		IBRIX.Trip
 * @subpackage	mod_trip_related
 * @author		Joep Tijssen
 */

// no direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once dirname(__FILE__).'/helper.php';

$params = modTripCalculateHelper::getParams($params);
$calculation = modTripCalculateHelper::getCalculation(JRequest::getInt('triptype_id'), JRequest::getInt('trip_id'), JRequest::getVar('departure_id'), JRequest::getInt('person'), JRequest::getVar('defaultoptions'), JRequest::getVar('extraoptions'));

require JModuleHelper::getLayoutPath('mod_trip_calculate', $params->get('layout', 'default'));
