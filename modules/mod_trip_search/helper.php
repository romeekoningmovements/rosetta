<?php
/**
 * @package		IBRIX.Trip
 * @subpackage	mod_trip_related
 * @author		Joep Tijssen
 */

// no direct access
defined('_JEXEC') or die;

class modTripSearchHelper
{
	static function getParams(&$params)
	{
		return $params;
	}
	
	function getTripTypes(){
		$db =& JFactory::getDBO();
		$query = "SELECT * FROM #__xtrip_triptypes ORDER BY triptype ASC";
		$db->setQuery($query);
		if($triptypes = $db->loadObjectList()){
			$arr = array();
			$arr[] = JHTML::_('select.option', 0,JText::_('ALL_TRIPTYPES'));
			foreach($triptypes as $triptype){
				$arr[] = JHTML::_('select.option', $triptype->id,$triptype->triptype);
			}

			return $arr;
		}
		return false;
	}
	
	function getDestinations($triptype_id){
		$db =& JFactory::getDBO();
		$query  = " SELECT t.triptype_id, t.destination_id, count(t.destination_id) as total, d.title as destination FROM #__xtrip_trips t";
		$query .= " INNER JOIN #__xtrip_destinations d ON d.destination_id=t.destination_id ";
		if($triptype_id){
			$query .= " WHERE t.triptype_id=".$db->Quote($triptype_id);
		}
		if(stripos($query, 'WHERE') !== FALSE){
			$query .= " AND t.state=1 ";
		}
		else{
			$query .= " WHERE t.state=1 ";
		}
		$query .= " GROUP BY t.destination_id ORDER BY d.title ASC" ;
		
		$db->setQuery($query);
		if($trips = $db->loadObjectList()){
			$arr = array();
			$arr[] = JHTML::_('select.option', 0,JText::_('ALL_DESTINATIONS'));
			foreach($trips as $trip){
				$arr[] = JHTML::_('select.option', $trip->destination_id,$trip->destination." (".$trip->total.") ");
			}
			return $arr;
		}
		return false;
	}
}
