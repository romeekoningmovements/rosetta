<?php
/**
 * @package		IBRIX.Trip
 * @subpackage	mod_trip_search
 * @author		Joep Tijssen & Loek Dieben
 */
 
 
	defined('_JEXEC') or die('Restricted access');
	JHTML::_('behavior.framework');
	//JHTML::_('behavior.calendar');


?>
<script type="text/javascript">
	
	function getDestinations(triptype_id){
		var update = new Request.HTML({
			url: 'index.php',
			useSpinner: true,
			method:'get',
			spinnerTarget: 'search_wrapper',
			spinnerOptions:{
				message: 'ophalen lijst',
				containerPosition: {relativeTo: document.id('destination_id'), position: 'center'}
			},
			onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript){
				if(document.id('destination_wrapper')){
					document.id('destination_wrapper').innerHTML = responseElements.filter('#destination_wrapper')[0].innerHTML;
				}
			}
		}).send('triptype_id='+triptype_id);
	}
</script>
<!--  Module layout - search trip-->

<div class="row-fluid" id="search_wrapper">
	<?php

	?>
	<form method="get" id="search_trips" name="search_trips" action="<?php echo JRoute::_('index.php?option=com_trip&view=search&Itemid=143'); ?>">
		<input type="text" name="searchfield" id="searchfield" value="<?php echo JRequest::getVar('searchfield'); ?>" class="span12" placeholder="Zoeken..."/>
		<?php
			if($triptypes){
				echo JHTML::_('select.genericlist', $triptypes, 'triptype_id', "class='span12' onChange='getDestinations(this.value);'", 'value', 'text', JRequest::getInt('triptype_id'));
			}
		?>
		
		<div id="destination_wrapper">
		<?php
			if($destinations){
				echo JHTML::_('select.genericlist', $destinations, 'destination_id', "class='span12'", 'value', 'text', JRequest::getInt('destination_id'));
			}
		?>
		</div>
		<div class="date-input1 span6">
			<div class="input-prepend date" id="datestart" data-date="<?php echo date("d-m-Y") ?>" data-date-format="dd-mm-yyyy">
				<button class="btn" type="button"><i class="icon-calendar"></i></button>
				<input size="16" type="text" placeholder="Heen"  name="search_date_start" value="<?php echo str_replace(':','-',JRequest::getVar('search_date_start')); ?>" readonly="">
			</div>
		</div>
		<div class="date-input2 span6">
			<div class="input-prepend date" id="dateend" data-date="<?php echo date("d-m-Y") ?>" data-date-format="dd-mm-yyyy">
				<button class="btn" type="button"><i class="icon-calendar"></i></button>
				<input size="16" type="text" placeholder="Terug" name="search_date_end" value="<?php echo str_replace(':','-',JRequest::getVar('search_date_end')); ?>" readonly="">
			</div>
		</div>
		<button class="btn btn-large btn-block" type="submit"><i class="icon-search"></i> <?php echo JText::_('SEARCH');?></button>
	</form>
</div>



