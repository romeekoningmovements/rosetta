<?php
/**
 * @package		IBRIX.Trip
 * @subpackage	mod_trip_related
 * @author		Joep Tijssen
 */

// no direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once dirname(__FILE__).'/helper.php';

$params = modTripSearchHelper::getParams($params);
$triptypes 	  = modTripSearchHelper::getTripTypes();
$destinations = modTripSearchHelper::getDestinations(JRequest::getInt('triptype_id'));

require JModuleHelper::getLayoutPath('mod_trip_search', $params->get('layout', 'default'));
