<?php
/**
 * @package		IBRIX.Trip
 * @subpackage	mod_trip_header
 * @author		Loek Dieben
 */

defined('_JEXEC') or die('Restricted access');

require_once(JPATH_BASE.DS.'components'.DS.'com_trip'.DS.'helpers'.DS.'route.php');
$app = JFactory::getApplication();
$session =& JFactory::getSession();


if($images){
	$session->set('images', $images);
	foreach($images as $image){
		?>
			<img src="<?php echo $image->image ?>" alt="<?php echo $app->getCfg('sitename'); ?> - <?php echo $image->title; ?>" title="<?php echo $app->getCfg('sitename'); ?> - <?php echo $image->title; ?>" <?php if(count($images) > 1){ ?> style="display:none;" <?php } ?>/>
		<?php
	}
}
?>