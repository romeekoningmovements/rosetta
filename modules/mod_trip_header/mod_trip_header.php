<?php
/**
 * @package		IBRIX.Trip
 * @subpackage	mod_trip_related
 * @author		Joep Tijssen
 */

// no direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once dirname(__FILE__).'/helper.php';

$params = modTripHeaderHelper::getParams($params);
$images = modTripHeaderHelper::getTripHeaders($params->get('amount'));


require JModuleHelper::getLayoutPath('mod_trip_header', $params->get('layout', 'default'));
