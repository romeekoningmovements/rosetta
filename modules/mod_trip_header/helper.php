<?php
/**
 * @package		IBRIX.Trip
 * @subpackage	mod_trip_related
 * @author		Joep Tijssen
 */

// no direct access
defined('_JEXEC') or die;

class modTripHeaderHelper
{
	public function getTripHeaders($amount){
		$db =& JFactory::getDBO();
		//$query = "SELECT triptype AS title, image FROM jos_xtrip_triptypes tt WHERE image != '' 
		//		  UNION 
		$itemid = JRequest::getInt('Itemid');
		$destination_id = 0;
		if($itemid){
			$query = "SELECT params FROM #__menu WHERE id=".$db->Quote($itemid);
			$db->setQuery($query);
			if($result = $db->loadObject()){
				$destination_params = json_decode($result->params);
				$destination_id = $destination_params->destination_id;
			}
		}
		
//		$query = "SELECT DEST.`title` as headertitle ,TRIP.`title` as title ,trip_id ,triptype_id FROM jos_xtrip_trips TRIP JOIN jos_xtrip_destinations DEST ON DEST.destination_id = TRIP.`destination_id` WHERE image != ''";

                
		$query  = "SELECT DEST.title AS headertitle, TRIP.trip_id, TRIP.triptype_id, TRIP.title,TRIP.image FROM jos_xtrip_trips TRIP ";
                $query .= "JOIN jos_xtrip_destinations DEST ON DEST.destination_id = TRIP.`destination_id`";
                $query .= "WHERE TRIP.image != ''";
		$query .= " AND `TRIP`.`state` = 1 ";
		if($destination_id){
			$query .= " AND TRIP.destination_id=".$db->Quote($destination_id);
		}
		$query .= " ORDER BY rand() LIMIT ".$amount;

		$db->setQuery($query);
		$images = $db->loadObjectList();
//		exit("<pre>".print_r($query, 1) );
		if($images){
			return $images;
		}
		
		return false;
		
	}
	
	
	static function getParams(&$params)
	{
		return $params;
	}
}