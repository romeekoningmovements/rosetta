<?php
	defined('_JEXEC') or die('Restricted access');
	require_once(JPATH_SITE.DS.'components'.DS.'com_trip'.DS.'helpers'.DS.'route.php');
	$session =& JFactory::getSession();
	$images = $session->get('images');

?>

<!--  Module layout - featured trip-->
<div class="spotlight-trip visible-desktop">
	<h4><?php echo JText::_('SPOTLIGHT');?>:</h4>
	<div class="spotlighttrips">
	<?php

	if(isset($images)){
		foreach($images as $trip){
			$alias = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($trip->title, ENT_QUOTES, 'UTF-8'));
			$alias = strtolower($alias);
			$alias = str_replace("  "," ",$alias);
			$alias = str_replace(" - ","-",$alias);
			$alias = str_replace(" ","-",$alias);
			?>
				<span <?php if(count($spotlight) > 1){?> style="display:none;" <?php } ?>><a href="<?php echo JRoute::_('index.php?option=com_trip&view=trip&trip_id='.$trip->trip_id.':'.$alias.'&Itemid='.TripHelperRoute::getItemIdByTriptype($trip->triptype_id)); ?>" alt="<?php echo $trip->headertitle; ?>" name="<?php echo $trip->headertitle; ?>"> <?php echo $trip->headertitle; ?> </a></span>
			<?php
		}
	}
	?>
	</div>
</div>