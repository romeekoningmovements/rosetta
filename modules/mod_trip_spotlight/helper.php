<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_wrapper
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

class modTripSpotlightHelper
{
	public function getTripSpotlight($amount){
		$db =& JFactory::getDBO();
		$query = "SELECT title,trip_id,triptype_id FROM #__xtrip_trips WHERE `state` = 1 ORDER BY rand() LIMIT ".$amount;
		$db->setQuery($query);
		$spotlight = $db->loadObjectList();
		if($spotlight){
			return $spotlight;
		}
		return false;
		
	}

	static function getParams(&$params)
	{
		return $params;
	}
}
