<?php
/**
* @version		$Id: helper.php for Canonical URL module 2009-03-29 andreas berger $
* @package		Joomla 1.5, Canonical URL Module 1.1.0
* @copyright	Copyright (C) 2005 - 2009 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class modCanonicalHelper
{
	function getCode(&$params, &$module)
	{
	// just startup
	global $mainframe;
	$document =& JFactory::getDocument();
	$_before=$_SERVER['REQUEST_URI'];
	//1.1
	$isheader=$document->getHeadData();
	foreach($isheader['links'] as $blub=>$key){
		if(strpos($key, "canonical")!=false){
			if (preg_match('#href="(.*?)" #s', $key, $match)) {
				$parts=explode('"',$match[0]);
				//check for an absolute path and replace it
				if(strpos($parts[1],"//")!=false){
					$parts[1]=preg_replace ("#.*?\/\/#s","",$parts[1]);
					$parts[1]=preg_replace ("#.*\/#s","/",$parts[1]);
				}
				$_before=$parts[1];
			}
		}
	}
	//1.1
	$s_ar=array('?','.','+','*','$','|','[',']','(',')','{','}','#');
	$r_ar=array('\?','\.','\+','\*','\$','\|','\[','\]','\(','\)','\{','\}','\#');
	for ($i=0;$i<=9;$i++){
		//get searchstring and mask special characters
		$getter=str_replace ( $s_ar , $r_ar , trim($params->get("rulef".$i)));
		//get replacestring
		$setter=trim($params->get("rules".$i));
		if($getter!=""){ //replace parts only
			if(substr($getter, -1)=="="){ //everything to the end of a parameter
				$tmp_before=explode($getter,$_before); //explode it to check if there is more than one string to replace
				if(count($tmp_before)>=2){
					for($j=1;$j<=count($tmp_before)-1;$j++){
						if(strpos($tmp_before[$j],"&")){//yes there are i.e. we are the first or in the middle
							$_before = preg_replace( "#(&|&amp;)?".$getter."(.*?)(&|&amp;)#", $setter."$3" , $_before );
						}
						else{//no, we are end of the chain i.e. we are the only one or the last one parameter
							$_before = preg_replace( "#(\?|&|&amp;)?".$getter."(.*)#", $setter , $_before );
						}
					}
				}
			}
			else{//no parameter
				$_before = preg_replace( "#".$getter."#", $setter , $_before );
			}
		}
		elseif($getter==""&&$setter!=""){ //replace the whole request
			$_before=$setter;
		}
   }
	$_dom = ($_before!="")?(substr(JURI :: base(), 0, -1)):(JURI :: base());
	//1.1
	$mytrigger=0;
	foreach($isheader['links'] as $blub=>$key){
		if(strpos($key, "canonical")!=false){
			$isheader['links'][$blub]='<link href="'.$_dom.$_before.'" rel="canonical"';
			$document->setHeadData($isheader);
			$mytrigger++;
		}
	}
	if($mytrigger<=0){
		$document->addHeadLink( $_dom.$_before, 'canonical', 'rel', '' );
	}
	//1.1
	}
}