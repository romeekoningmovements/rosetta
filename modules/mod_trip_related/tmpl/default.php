<?php
/**
 * @package		IBRIX.Trip
 * @subpackage	mod_trip_related
 * @author		Joep Tijssen & Loek Dieben
 */
	defined('_JEXEC') or die('Restricted access');
?>
<?php
	if($relatedtrips){
		?>
		<ul class="nav nav-tabs nav-stacked">
			<?php 
			foreach($relatedtrips as $relatedtrip){
				$alias = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($relatedtrip->title, ENT_QUOTES, 'UTF-8'));
				$alias = strtolower($alias);
				$alias = str_replace("  "," ",$alias);
				$alias = str_replace(" ","-",$alias);
				?>
				<li>
					<?php
						//JRoute::_('index.php?option=com_trip&view=trip&triptype_id='.$trip->triptype_id.':'.TripHelperRoute::getTripTypeAlias($trip->triptype).'&trip_id='.$trip->trip_id.':'.TripHelperRoute::getTripTypeAlias($trip->triptitle).'&Itemid='.TripHelperRoute::getItemIdByTriptype($trip->triptype_id)); 
					?>
					<a href="<?php echo JRoute::_('index.php?option=com_trip&view=trip&trip_id='.$relatedtrip->trip_id.":".$alias.'&Itemid='.$relatedtrip->itemid); ?>" alt="<?php echo $relatedtrip->title; ?>" title="<?php echo $relatedtrip->title; ?>"> 
						<?php echo $relatedtrip->title; ?> <i class="icon-chevron-right pull-right"></i>
					</a>
				</li>
				<?php
			}
			?>
		</ul>
		<?php
	}
?>
				