<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_related_items
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

require_once JPATH_SITE.'/components/com_content/helpers/route.php';

abstract class modTripRelatedHelper
{
	public function getRelatedTrips($trip_id, $triptype_id){
		$db =& JFactory::getDBO();
		$query = "SELECT * FROM #__xtrip_trips WHERE trip_id=".$db->Quote($trip_id);
		$db->setQuery($query);
		if($currenttrip = $db->loadObject()){
			$matchingcriteria = explode(' ', $currenttrip->title);
			if(isset($matchingcriteria[0])){
				$matchingcriteria = str_replace('-','',$matchingcriteria[0]);
				$query  = " SELECT t.*, m.id as itemid FROM #__xtrip_trips t";
				$query .= " INNER JOIN #__xtrip_triptypes tt ON tt.id=t.triptype_id ";
				$query .= " INNER JOIN #__menu m ON m.title=tt.triptype ";
				$query .= " WHERE t.title LIKE '%".$matchingcriteria."%' AND t.trip_id != ".$db->Quote($trip_id)." AND `state` = 1 ORDER BY triptype_id ASC LIMIT 6";
				$db->setQuery($query);
				if($related = $db->loadObjectList()){
					return $related;
				}
				else{
					$query  = " SELECT t.*, m.id as itemid FROM #__xtrip_trips t ";
					$query .= " INNER JOIN #__xtrip_triptypes tt ON tt.id=t.triptype_id ";
					$query .= " INNER JOIN #__menu m ON m.title=tt.triptype ";
					$query .= " WHERE t.triptype_id=".$db->Quote($triptype_id);
					$query .= "  AND `state` = 1 ORDER BY rand() LIMIT 6";
					$db->setQuery($query);
					if($related = $db->loadObjectList()){
						return $related;
					}
				}
			}
		}
		return false;
	}
	
	static function getParams(&$params)
	{
		return $params;
	}

}
