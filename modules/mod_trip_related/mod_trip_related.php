<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_wrapper
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once dirname(__FILE__).'/helper.php';

$params = modTripRelatedHelper::getParams($params);
$relatedtrips = modTripRelatedHelper::getRelatedTrips(JRequest::getInt('trip_id'), JRequest::getInt('triptype_id'));

require JModuleHelper::getLayoutPath('mod_trip_related', $params->get('layout', 'default'));
